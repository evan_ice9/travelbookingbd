//
//  SignUpViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()

@end


typedef struct
{
    int month;
    int day;
    int weekDay;
    int year;
}dateObj;

static const CGFloat PORTRAIT_KEYBOARD_HEIGHT = 216;

@implementation SignUpViewController
{

    UIView *containerView;
    UIView *maskView;
    PDTSimpleCalendarViewController *calendarViewController;
    dateObj dateofBirth;
    NSDate *d;
    UIView *v;
    MBProgressHUD *hud;
    NSArray *citydata;
    NSString *cityCode;
    UITextField *activeField;
}



@synthesize scrollView,nameTitle,firstname,lastname,address,zip,city,passport,DOB,pictureView,email,pass,phone;
@synthesize cityTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap1=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    
    
    UITapGestureRecognizer *tap2=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tap1.numberOfTapsRequired = 1;
    tap1.numberOfTouchesRequired = 1;
    
    tap2.numberOfTapsRequired = 1;
    tap2.numberOfTouchesRequired = 1;
    
    [DOB addGestureRecognizer:tap1];
    [pictureView addGestureRecognizer:tap2];
    DOB.userInteractionEnabled = YES;
    pictureView.userInteractionEnabled =YES;
    pictureView.layer.cornerRadius = 4.0f;
    dateofBirth =[self componentsFromDate:[NSDate date]];
    
    nameTitle.delegate =self;
    firstname.delegate = self;
    lastname.delegate = self;
    address.delegate = self;
    zip.delegate = self;
    passport.delegate = self;
    email.delegate = self;
    pass.delegate = self;
    phone.delegate = self;
    city.delegate = self;
    
    city.clearButtonMode = UITextFieldViewModeWhileEditing;
    scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
    if (self.fbUserInfo)
    {
        [self prefillWithFbData:self.fbUserInfo];
    }
    // Do any additional setup after loading the view.
}



-(void)handleTap:(UITapGestureRecognizer *)tap
{

    int tag = tap.view.tag;
    NSDate *selectedDate;
    
    switch (tag)
    {
        case 90:
        {
            NSLog(@"hello");
            
            
    
            
            calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
            [calendarViewController setDelegate:self];
            NSDateComponents *offset=[[NSDateComponents alloc]init];
            offset.year=-60;
            NSDate *firstDate=[calendarViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0];
            
            offset.year = -16;
            selectedDate =[calendarViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0];
            
            calendarViewController.firstDate=firstDate;
            calendarViewController.lastDate=[NSDate date];
            calendarViewController.selectedDate = d?d:selectedDate;

            containerView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 280, 400)];
            UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];
            [self dimIn];
            
            [self addChildViewController:calendarViewController];
            [containerView addSubview:calendarViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [containerView addSubview:doneBtn];
            containerView.layer.cornerRadius = 8.0f;
            containerView.clipsToBounds = YES;
            [self.view addSubview:containerView];
            [self.view bringSubviewToFront:containerView];
            [calendarViewController.collectionView reloadData];
            [calendarViewController scrollToSelectedDate:NO];
        }
            break;
        case 92:
        {
        
            [self dimIn];
            
            v = [[UIView alloc]initWithFrame:CGRectMake(40, 100, 240, 250)];
            v.backgroundColor=[Utils colorFromHex:@"#dedede"];
            UIButton *btn1 =[[UIButton alloc]initWithFrame:CGRectMake(30, 160, 180, 30)];
            UIButton *btn2 =[[UIButton alloc]initWithFrame:CGRectMake(30, 200, 180, 30)];
            
            v.layer.cornerRadius = 4.0f;
            //btn1.backgroundColor =[UIColor blueColor];
            //btn2.backgroundColor =[UIColor grayColor];
            
            [btn1 setBackgroundImage:[Utils imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
            [btn2 setBackgroundImage:[Utils imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
           
            [btn1 setTitle:@"Camera" forState:UIControlStateNormal];
            [btn2 setTitle:@"Album" forState:UIControlStateNormal];
            
            [btn1 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [btn2 addTarget:self action:@selector(btnPressed:) forControlEvents:UIControlEventTouchUpInside];
            
            btn2.layer.cornerRadius = 4.0f;
            btn1.layer.cornerRadius = 4.0f;
            
            
            btn1.clipsToBounds = YES;
            btn2.clipsToBounds = YES;
            
            v.clipsToBounds = YES;
            
            [v addSubview:btn1];
            [v addSubview:btn2];
            
            btn1.tag = 12;
            btn2.tag = 14;
            
            
            
            [self.view addSubview:v];
        }
            
            break;
            
        default:
            break;
    }
    


}


-(IBAction)btnPressed:(id)sender
{

    UIButton *btn =(UIButton *)sender;
    
    int tag = btn.tag;
    
    
    switch (tag) {
        case 12:
            
            NSLog(@"camera");
            
            [self showImagepickerfromCamera];
            break;
            
        case 14:
            NSLog(@"album");
            [self showImagepickerfromLibrary];
            break;
            
        default:
            break;
    }
    

    NSLog(@"pressed");

}


-(IBAction)dateConfirmBtn:(id)sender
{
    NSLog(@"date confirm");
    [containerView removeFromSuperview];
    [calendarViewController removeFromParentViewController];
    [self dimOut];
    
    NSString *dateStr = [NSString stringWithFormat:@"%d-%d-%d",dateofBirth.day,dateofBirth.month,dateofBirth.year];
    [DOB setText:dateStr];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signUp:(id)sender
{
    
    NSLog(@"signed up");
    
/*    nameTitle.delegate =self;
    firstname.delegate = self;
    lastname.delegate = self;
    address.delegate = self;
    zip.delegate = self;
    passport.delegate = self;
    email.delegate = self;
    pass.delegate = self;
    phone.delegate = self;
    city.delegate = self;*/
    if ([nameTitle.text isEqualToString:@""] || [firstname.text isEqualToString:@""] || [lastname.text isEqualToString:@""] || [address.text isEqualToString:@""] || [zip.text isEqualToString:@""] || [passport.text isEqualToString:@""] || [email.text isEqualToString:@""] || [pass.text isEqualToString:@""] || [phone.text isEqualToString:@""] || [city.text isEqualToString:@""])
    {
        [self.view makeToast:@"Some fields are missing"];
    }
    else
    {
    
        NSDictionary *dict = @{@"title":nameTitle.text,@"firstName":firstname.text,@"lastName":lastname.text,@"dob":DOB.text,@"email":email.text,@"phone":phone.text,@"address":address.text,@"address2":address.text,@"zip":zip.text,@"passport":passport.text,@"city":cityCode,@"image":@"http://www.clker.com/cliparts/5/7/4/8/13099629981030824019profile.svg.med.png",@"photoId":@"12121",@"nationality":@"BD",@"password":pass.text,@"type":@"test",@"fbid":@"",@"gplusid":@""};
        
        
        
        hud = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:hud];
        [hud show:YES];
        
        [[Network shared_instance]SignUpwithParameters:dict completionBlock:^(int status, id response) {
            
            switch (status) {
                case 0:
                    
                    NSLog(@"%@",response);
                    [self loginSuccess:response];
                    break;
                case 1:
                    NSLog(@"%@",response);
                    [self loginFailed];
                    break;
                    
                default:
                    break;
            }
            
            
            
            
        }];

        
        
    }
    
    
    
    
    
    
    
    
    
}

- (IBAction)backToSignIn:(id)sender
{
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    
    if ([self.presentingViewController isKindOfClass:[InitSlidingViewController class]])
    {
        

        SignInViewController *signIn = (SignInViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signIn"];
        [self dismissViewControllerAnimated:NO completion:nil];
        [self.presentingViewController presentViewController:signIn animated:YES completion:nil];
    }
    else
    {
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
    
    
}

- (IBAction)back:(id)sender
{
    
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    
   [self dismissViewControllerAnimated:YES completion:nil];
    
}


#pragma mark Facebook Login methods

-(void)prefillWithFbData:(NSDictionary *)fbData
{
    NSLog(@"%@",fbData);
    
    firstname.text = fbData[@"first_name"];
    lastname.text = fbData[@"last_name"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    NSDate *birthDay =  [formatter dateFromString:fbData[@"birthday"]];
    [formatter setDateFormat:@"dd-MM-yyyy"];
    DOB.text = [formatter stringFromDate:birthDay];
    [pictureView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=250&height=250",fbData[@"id"]]]];
    pictureView.clipsToBounds = YES;
    email.text = fbData[@"email"];
    address.text = fbData[@"location"][@"name"];
    if ([fbData[@"gender"] isEqualToString:@"male"])
    {
        nameTitle.text = @"Mr";
    }
    else
    {
        nameTitle.text = @"Mrs";
    }
}

#pragma mark ImagePicker Delegates


-(void)showImagepickerfromLibrary
{

    UIImagePickerController *picker =[[UIImagePickerController alloc]init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType =UIImagePickerControllerSourceTypePhotoLibrary;
    
    /*[self addChildViewController:picker];
    [self.view addSubview:picker.view];*/
    
    [self presentViewController:picker animated:YES completion:nil];
    
    

}



-(void)showImagepickerfromCamera
{
    
    UIImagePickerController *picker =[[UIImagePickerController alloc]init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType =UIImagePickerControllerSourceTypeCamera;
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Device has no camera" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    
        [alert show];
    }
    
    
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    UIImage *img = info[UIImagePickerControllerEditedImage];
    [pictureView setImage:img];
    [pictureView setContentMode:UIViewContentModeScaleToFill];
    pictureView.layer.cornerRadius = 4.0f;
    pictureView.clipsToBounds = YES;
    
    [self dimOut];
    [v removeFromSuperview];
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}



-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{


    NSLog(@"image pick cancelled");
    
    [v removeFromSuperview];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self dimOut];

}



#pragma mark CalanderView Delegates


-(void)dimIn
{
    
    maskView=[[UIView alloc]initWithFrame:self.view.bounds];
    maskView.backgroundColor=[UIColor blackColor];
    maskView.alpha=0;
    [self.view addSubview:maskView];
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=.8;
    }];
    
    
    
}

-(void)dimOut
{
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=0;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [maskView removeFromSuperview];
            maskView=nil;
            
        }
    }];
    
}


-(dateObj)componentsFromDate:(NSDate *)date
{
    
    NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned units = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit;
    NSDateComponents *components =[calender components:units fromDate:date];
    
    dateObj obj;
    
    obj.day = (int)components.day;
    obj.month = (int)components.month;
    obj.weekDay = (int)components.weekday;
    obj.year = (int)components.year;
    
    return obj;
}

-(NSString *)getMonthOfYear:(int)month
{
    
    NSArray *months=@[@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December"];
    
    return months[month-1];
    
}

- (void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    NSLog(@"Date Selected : %@",date);
    
    d = date;
    dateofBirth = [self componentsFromDate:date];
}

- (BOOL)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller shouldUseCustomColorsForDate:(NSDate *)date
{
    /*if ([self.customDates containsObject:date]) {
     return YES;
     }*/
    
    return NO;
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller circleColorForDate:(NSDate *)date
{
    return [UIColor whiteColor];
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller textColorForDate:(NSDate *)date
{
    return [UIColor orangeColor];
}


#pragma mark Keyboard Delegates

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    activeField = textField;
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}


-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{

    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    scrollView.contentInset = contentInsets;
    scrollView.scrollIndicatorInsets = contentInsets;

    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, PORTRAIT_KEYBOARD_HEIGHT, 0);
    scrollView.contentInset = edgeInsets;
    scrollView.scrollIndicatorInsets = edgeInsets;
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= PORTRAIT_KEYBOARD_HEIGHT;

    
    if (activeField==city)
    {
        [self.scrollView setContentOffset:CGPointMake(0, activeField.frame.origin.y+167) animated:YES];
        
    }
    else if (!CGRectContainsPoint(aRect, activeField.frame.origin) )
    {
      [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }

    

}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    if (textField == city)
    {
        
        NSString *tempStr = textField.text;
        tempStr = [tempStr stringByReplacingCharactersInRange:range withString:string];
        
        if (tempStr.length > 2)
        {
            [[Network shared_instance]getCities:tempStr completionBlock:^(int status, id response)
             {
                 switch (status) {
                     case 0:
                     {
                         
                         //NSLog(@"%@",response);
                         
                         if ([response[@"cities"] count]>0)
                         {
                             citydata = response[@"cities"];
                             cityTable.hidden = NO;
                             [cityTable reloadData];
                             [cityTable initTableViewDataSourceAndDelegate:^NSInteger(UITableView *tableView, NSInteger section)
                              {
                                  return citydata.count;
                              }
                                                  setCellForIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath)
                              {
                                  
                                  SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
                                  [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                                  @try
                                  {
                                      [cell.lb setText:[NSString stringWithFormat:@"%@,%@",citydata[indexPath.row][@"name"],citydata[indexPath.row][@"countryName"]]];
                                  }
                                  @catch (NSException *exception)
                                  {
                                      NSLog(@"%@",exception.description);
                                  }
                                  
                                  
                                  return cell;
                              }
                                                      setDidSelectRowBlock:^(UITableView *tableView, NSIndexPath *indexPath)
                              {
                                  SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
                                  textField.text=cell.lb.text;
                                  cityCode = citydata[indexPath.row][@"cityCode"];
                                  cityTable.hidden = YES;
                              }];
                             
                         }
                         
                         
                     }
                         break;
                         
                     default:
                         break;
                 }
                 
                 
             }];
        }
        else
        {
        
            cityTable.hidden = YES;
        
        }
  
        
        
        
        
    }

    return YES;

}

#pragma mark SignUp Methods

-(void)loginSuccess:(NSDictionary *)info
{
    
    
    NSLog(@"user Logged in");
    info = [info objectForKey:@"user"];
    [[UserObject sharedInstance]setUserLoggedIn:YES];
    [[UserObject sharedInstance]SetAccountType:emailAccount];
    
    
    NSString *fullName=[NSString stringWithFormat:@"%@.%@ %@",info[@"title"],info[@"firstName"],info[@"lastName"]];
    NSString *userID=[NSString stringWithFormat:@"%@",info[@"id"]];
    NSString *email_no=[NSString stringWithFormat:@"%@",info[@"email"]];
    NSString *birthday =[NSString stringWithFormat:@"%@",info[@"dob"]];
    NSString *location =[NSString stringWithFormat:@"%@ %@",info[@"address"],info[@"address2"]];
    NSString *photoURL = info[@"image"];
    NSString *zip_no = info[@"zip"];
    NSString *nationality = info[@"nationality"];
    
    NSLog(@"%@ %@ %@ %@ %@",fullName,userID,email,birthday,location);
    
    UserObject *u=[UserObject sharedInstance];
    [u setEmail:email_no];
    [u setUserName:fullName];
    [u setBirthday:birthday];
    [u setLocation:location];
    [u setProfileURL:photoURL];
    [u setUserID:userID];
    [u setZip:zip_no];
    [u setNationality:nationality];
    
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];
    [hud show:NO];
    [hud removeFromSuperview];
    
    /*NSDictionary *temp_dict=[[NSDictionary alloc]initWithObjectsAndKeys:@"Sign In",@"value",@"no",@"doAdjust",nil];
     [[NSNotificationCenter defaultCenter]postNotificationName:SideMenu_Action object:nil userInfo:temp_dict];*/
    [self dismissViewControllerAnimated:YES completion:nil];
}



-(void)loginFailed
{
    [self.view makeToast:@"Sign Up failed"];
    [hud show:NO];
    [hud removeFromSuperview];
}


@end
