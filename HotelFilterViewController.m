//
//  HotelFilterViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 6/23/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HotelFilterViewController.h"

@implementation HotelFilterViewController
{

    UITextField *tf,*areatf;
    NSString *searchString,*sortType;
    RadioButton *r1,*r2;
    TableViewWithBlock *areaTable;
    NSArray *areaData;
    NMRangeSlider *nm1,*nm2;
    NSString *ratingUpperValue,*ratingLowerValue;
	float priceRatio;
}


@synthesize parameters,priceLowerValue,priceUpperValue;
@synthesize slider1LeadingSpace,slider1TraiingSpace;
@synthesize slider2TrailingSpace,slider2LeadingSpace;


-(void)viewDidLoad
{
    
    UIImageView *hotelIcon = (UIImageView *)[self.view viewWithTag:700];
    hotelIcon.image = [hotelIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [hotelIcon setTintColor:[Utils colorFromHex:@"#999999"]];
    
    tf = (UITextField *)[self.view viewWithTag:710];
    tf.background = [Utils imageNamed:@"UnderLine" imagetintColor:[Utils colorFromHex:@"#6D63B0"]];
    tf.delegate = self;
    tf.text = parameters[@"filter"][@"hotelName"]?parameters[@"filter"][@"hotelName"]:@"";
    
    areatf = (UITextField *)[self.view viewWithTag:720];
    areatf.delegate = self;
    
    r1 = (RadioButton *)[self.view viewWithTag:800];
    r2 = (RadioButton *)[self.view viewWithTag:810];
    
    r1.imageView.tintColor = [Utils colorFromHex:@"#F57C00"];
    
    
    r1.groupButtons = @[r1,r2];
    
    if (parameters[@"filter"][@"sortType"])
    {
        if ([sortType isEqualToString:@"price"])
        {
            [r1 setSelectedWithTag:800];
        }
        else if([sortType isEqualToString:@"rating"])
        {
            [r1 setSelectedWithTag:810];
        }
    }
    
    
    areaTable = (TableViewWithBlock *)[self.view viewWithTag:730];
    [areaTable initTableViewDataSourceAndDelegate:^NSInteger(UITableView *tableView, NSInteger section)
     {
         
         return areaData.count;
         
     } setCellForIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath)
     {
         
         
         SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
         [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
         
         @try
         {
             [cell.lb setText:[NSString stringWithFormat:@"%@",areaData[indexPath.row]]];
         }
         @catch (NSException *exception)
         {
             NSLog(@"%@",exception.description);
         }
         
         return cell;
         
     }setDidSelectRowBlock:^(UITableView *tableView, NSIndexPath *indexPath)
     {
         [areatf setText:areaData[indexPath.row]];
         areaTable.hidden = YES;
     }];
    
    
    UIButton *applyBtn = (UIButton *)[self.view viewWithTag:820];
    [applyBtn addTarget:self action:@selector(filterDone:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *resetBtn = (UIButton *)[self.view viewWithTag:830];
    [resetBtn addTarget:self action:@selector(filterReset:) forControlEvents:UIControlEventTouchUpInside];
	
	
	if (parameters[@"filter"][@"rating"])
    {
		NSArray *ratingArr = parameters[@"filter"][@"rating"];
		ratingLowerValue = [ratingArr firstObject];
		ratingUpperValue = [ratingArr lastObject];
	}
	else
	{
		ratingLowerValue = @"1";
		ratingUpperValue = @"5";
	
	}
	
    nm1 = (NMRangeSlider *)[self.view viewWithTag:440];
    [nm1 addTarget:self action:@selector(slider1ValueChange:) forControlEvents:UIControlEventValueChanged];
    [nm1 setTintColor:[Utils colorFromHex:@"#F57C00"]];
    
    nm2 = (NMRangeSlider *)[self.view viewWithTag:450];
    [nm2 addTarget:self action:@selector(slider2ValueChange:) forControlEvents:UIControlEventValueChanged];
    [nm2 setTintColor:[Utils colorFromHex:@"#F57C00"]];
	
	priceRatio = ([priceUpperValue intValue] - [priceLowerValue intValue])/100;
    
    nm1.minimumValue = 0;
    nm1.maximumValue = 100;
	
	if (parameters[@"filter"][@"priceGt"] && parameters[@"filter"][@"priceLt"])
    {
		nm1.upperValue = ([parameters[@"filter"][@"priceLt"] intValue]-[priceLowerValue intValue])/priceRatio;
		
		nm1.lowerValue = ([parameters[@"filter"][@"priceGt"] intValue]-[priceLowerValue intValue])/priceRatio;
	}
	else
	{
		nm1.upperValue = 100;
		nm1.lowerValue = 0;
	}
	
   
    
    nm2.minimumValue = 1;
    nm2.maximumValue = 5;
    
    nm2.upperValue = [ratingUpperValue intValue];
    nm2.lowerValue = [ratingLowerValue intValue];
    
    [self updateSliderValues1];
    [self updateSliderValues2];
	
	[self initSliderPositions1];
	[self initSliderPositions2];

}

#pragma mark Filter Delegates

-(IBAction)filterDone:(id)sender
{

    NSString *priceLv = [NSString stringWithFormat:@"%f",nm1.lowerValue*priceRatio + [priceLowerValue intValue]];
    NSString *priceUv = [NSString stringWithFormat:@"%f",nm1.upperValue *priceRatio + [priceLowerValue intValue]];
    
    ratingLowerValue = [NSString stringWithFormat:@"%d",(int)nm2.lowerValue];
    ratingUpperValue = [NSString stringWithFormat:@"%d",(int)nm2.upperValue];
	
    searchString = tf.text;
    sortType = r1.selectedButton.tag == 800 ?@"price":@"rating";
    
    NSDictionary *filters = @{@"hotelName":tf.text,@"priceGt":priceLv,@"priceLt":priceUv,@"rating":[self getRatingStrwithIndex:[ratingLowerValue intValue] ending:[ratingUpperValue intValue]],@"sortType":sortType};
    [self.delegate applyFilter:filters];

}


-(NSArray *)getRatingStrwithIndex:(int)start ending:(int)end
{

	NSMutableArray *ratingArr = [[NSMutableArray alloc]init];
	for (int i = start; i <= end; i++)
	{
		[ratingArr addObject:[NSString stringWithFormat:@"%d",i]];
	}
	//[ratingStr deleteCharactersInRange:NSMakeRange(ratingStr.length-1, 1)];
	//[ratingStr appendFormat:@"]"];
	
	return ratingArr;
}

-(IBAction)filterReset:(id)sender
{
    NSDictionary *filters = @{};
    [self.delegate applyFilter:filters];
}


-(IBAction)slider1ValueChange:(id)sender
{
    [self updateSliderValues1];
    
}

-(IBAction)slider2ValueChange:(id)sender
{
    [self updateSliderValues2];
}



-(void)updateSliderValues1
{
    
    CGPoint lowerCenter;
    lowerCenter.x = (nm1.lowerCenter.x + nm1.frame.origin.x);
    lowerCenter.y = (nm1.center.y - 30.0f);
    UILabel *lowerLabel = (UILabel *)[self.view viewWithTag:420];
    lowerLabel.center = lowerCenter;
    lowerLabel.text = [NSString stringWithFormat:@"%d", (int)nm1.lowerValue*(int)priceRatio + [priceLowerValue intValue]];
    
    CGPoint upperCenter;
    upperCenter.x = (nm1.upperCenter.x + nm1.frame.origin.x);
    upperCenter.y = (nm1.center.y - 30.0f);
    UILabel *upperLabel = (UILabel *)[self.view viewWithTag:421];
    upperLabel.center = upperCenter;
    upperLabel.text = [NSString stringWithFormat:@"%d", (int)nm1.upperValue * (int)priceRatio +[priceLowerValue intValue]];
    
    UILabel *priceRangeLabel = (UILabel *)[self.view viewWithTag:600];
    NSString *currencyLabel = [Utils getCurrencySymBol:parameters[@"currency"]];
    [priceRangeLabel setText:[NSString stringWithFormat:@"%@%@-%@%@",lowerLabel.text,currencyLabel,upperLabel.text,currencyLabel]];
}


-(void)updateSliderValues2
{
    
    CGPoint lowerCenter;
    lowerCenter.x = (nm2.lowerCenter.x + nm2.frame.origin.x);
    lowerCenter.y = (nm2.center.y - 30.0f);
    UILabel *lowerLabel = (UILabel *)[self.view viewWithTag:430];
    lowerLabel.center = lowerCenter;
    lowerLabel.text = [NSString stringWithFormat:@"%d", (int)nm2.lowerValue];
	
	
	NSLog(@"%f",lowerLabel.frame.origin.x);
	
    CGPoint upperCenter;
    upperCenter.x = (nm2.upperCenter.x + nm2.frame.origin.x);
    upperCenter.y = (nm2.center.y - 30.0f);
    UILabel *upperLabel = (UILabel *)[self.view viewWithTag:431];
    upperLabel.center = upperCenter;
    upperLabel.text = [NSString stringWithFormat:@"%d", (int)nm2.upperValue];
    UILabel *ratingRange = (UILabel *)[self.view viewWithTag:605];
    [ratingRange setText:[NSString stringWithFormat:@"%@ star - %@ star",lowerLabel.text,upperLabel.text]];
	
	NSLog(@"%f",upperLabel.frame.origin.x);
}


-(void)initSliderPositions1
{
	float fullWidth1 = 207-0;
	float fullWidth2 = 210-3.5;
	
	UILabel *priceLowerLabel = (UILabel *)[self.view viewWithTag:420];
	priceLowerLabel.translatesAutoresizingMaskIntoConstraints = NO;
	
	float ratio1 = fullWidth1/nm1.maximumValue;
	float ratio2 = fullWidth2/nm1.maximumValue;
	
	slider1LeadingSpace.constant = ratio1 * nm1.lowerValue;
	
	UILabel *priceUpperLabel = (UILabel *)[self.view viewWithTag:421];
	priceUpperLabel.translatesAutoresizingMaskIntoConstraints = NO;
	
	slider1TraiingSpace.constant = ratio2 * (nm1.maximumValue-nm1.upperValue) + 4;
}

-(void)initSliderPositions2
{

	float fullWidth = 229-22;
	
	UILabel *ratingLowerLabel = (UILabel *)[self.view viewWithTag:430];
	ratingLowerLabel.translatesAutoresizingMaskIntoConstraints = NO;
	
	float ratio = fullWidth/(nm2.maximumValue - nm2.minimumValue);
	slider2LeadingSpace.constant = 22 + ratio * (nm2.lowerValue - nm2.minimumValue);
	
	UILabel *ratingUpperLabel = (UILabel *)[self.view viewWithTag:431];
	ratingUpperLabel.translatesAutoresizingMaskIntoConstraints = NO;
	
	slider2TrailingSpace.constant = ratio * (nm2.maximumValue - nm2.upperValue) + 23;
	
}

#pragma mark TextField delegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == areatf)
    {
        NSString *searchText = areatf.text;
        searchText = [searchText stringByReplacingCharactersInRange:range withString:string ];
        if (searchText.length >3)
        {
            
            [[Network shared_instance]getCityLocations:searchText completionBlock:^(int status, id response)
             {
                 switch (status) {
                     case 0:
                     {
                         
                         
                         NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                         for (GMSAutocompletePrediction *value in response)
                         {
                             [tempArr addObject:value.attributedFullText.string];
                         }
                         areaData = tempArr;
                         
                         if (areaData.count>0)
                         {
                             [areaTable reloadData];
                             areaTable.hidden = NO;
                         }
                         else
                         {
                             
                             areaTable.hidden = YES;
                             
                         }
                         
                         
                         
                     }
                         break;
                         
                     default:
                         break;
                 }
                 
                 
             }];
            
            
        }
        
    }
    
    
    
    return YES;
    
}



@end
