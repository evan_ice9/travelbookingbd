//
//  Network.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/4/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import <GoogleMaps/GoogleMaps.h>
#import <UIImageView+AFNetworking.h>

@interface Network : AFHTTPSessionManager



+(Network *)shared_instance;



-(void)getTourFilterOptionsWithCompletionBlock:(void(^)(int status,id response))cmpBlock;
-(void)getCurrenciesWithCompletionBlock:(void (^)(int status,id response))compBlck;
-(void)getHotelswithParameters:(NSDictionary *)data completionBlock:(void(^)(int status,id response))compblock;
-(void)getFeaturedHotelsandToursWithCompletion:(void(^)(int status,id response))compblock;
-(void)getOnlyFeaturedToursCompletionBlock:(void(^)(int status,id response))cmpBlock;
-(void)loginwithParameters:(NSDictionary *)parameters completionBlock:(void(^)(int status,id response))compblock;
-(void)fbLoginWithParameters:(NSDictionary *)parameters completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)SignUpwithParameters:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)recentSearcheswithUserID:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlck;
-(void)userReservations:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlck;
-(void)userTourReservations:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)bookHotel:(NSDictionary *)params completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)getReservedHotel:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)getReservedRoom:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)cancelHotel:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)bookTour:(NSDictionary *)params completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)filterTours:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpBlock;

-(void)getHotelDetails:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpblock;
-(void)getCities:(NSString *)cityName completionBlock:(void(^)(int status,id response))cmpblock;
-(void)getRooms:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpblock;
-(void)getTour:(NSDictionary *)data completionBlock:(void(^)(int status,id response))compblock;
-(void)checkInterHotel:(NSDictionary *)data compBlock:(void(^)(int status,id response))cmpBlock;
-(void)getTourPlaces:(NSString *)place completionBlock:(void(^)(int status,id response))cmpBlock;
-(void)getCityLocations:(NSString *)location completionBlock:(void(^)(int status,id response))cmpblock;

-(void)cancelAllConnections;



@end
