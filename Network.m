//
//  Network.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/4/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "Network.h"

@implementation Network




+(Network *)shared_instance
{

    static Network *_sharedInstance=nil;
    
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
        
        _sharedInstance=[[self alloc]initWithBaseURL:[NSURL URLWithString:base_url]];
        
    });

    return _sharedInstance;
}

-(instancetype)initWithBaseURL:(NSURL *)url
{

    self = [super initWithBaseURL:url];
    
    if (self)
    {
        self.requestSerializer=[AFJSONRequestSerializer serializer];
        self.responseSerializer=[AFJSONResponseSerializer serializer];
        [self.requestSerializer setTimeoutInterval:50];
		[self.requestSerializer setValue:[Utils hashValue:IOS_APP_API_PRIVATE_KEY publicKey:IOS_APP_API_PUBLIC_KEY] forHTTPHeaderField:@"TBBD-Signature"];
		[self.requestSerializer setValue:IOS_APP_API_PUBLIC_KEY forHTTPHeaderField:@"TBBD-Key"];
        [self.operationQueue setMaxConcurrentOperationCount:1];
    }

    return self;
}


-(void)getCurrenciesWithCompletionBlock:(void (^)(int status,id response))compBlck
{
    [self GET:@"get-currencies" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
       
        compBlck(0,responseObject);
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        
        NSLog(@"%@",error.description);
        compBlck(1,error.description);
    }];

}


-(void)getFeaturedHotelsandToursWithCompletion:(void(^)(int status,id response))compblock
{

    [self GET:@"featured-hotels-tours" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        compblock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        compblock(1,error.description);
        
    }];


}

-(void)loginwithParameters:(NSDictionary *)parameters completionBlock:(void(^)(int status,id response))compblock
{

    NSLog(@"%@",parameters);
    [self POST:@"device-login-by-email" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
    {
        if ([[responseObject objectForKey:@"error"] boolValue])
        {
            compblock(1,responseObject);
        }
        else
        {
        
            compblock(0,responseObject);
        
        }
        
        
       
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
    
                    compblock(1,error.description);
    }];



}


-(void)fbLoginWithParameters:(NSDictionary *)parameters completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"device-login-by-fb" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject)
    {
       
        if ([[responseObject objectForKey:@"error"] boolValue])
        {
            cmpBlock(1,responseObject);
        }
        else
        {
            
            cmpBlock(0,responseObject);
            
        }
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
    
            cmpBlock(1,error.description);
        
    }];
    

}


-(void)SignUpwithParameters:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpBlock
{
    
    [self POST:@"device-signup" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
    {
        
        
        
        if ([[responseObject objectForKey:@"error"] boolValue])
        {
         
            cmpBlock(1,responseObject);
        }
        else
        {
        
            cmpBlock(0,responseObject);
        }
        
       
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
       
        cmpBlock(1,error.description);
        
    }];



} 



-(void)getHotelDetails:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpblock
{
    
    [self POST:@"hotel" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         
         
         if ([[responseObject objectForKey:@"error"]boolValue])
         {
             
             cmpblock(1,responseObject);
         }
         else
         {
             
             cmpblock(0,responseObject);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         cmpblock(1,error.description);
         
     }];


}


-(void)getRooms:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpblock
{
    
    [self POST:@"rooms" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         
         
         if ([[responseObject objectForKey:@"error"]boolValue])
         {
             
             cmpblock(1,responseObject);
         }
         else
         {
             
             cmpblock(0,responseObject);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         cmpblock(1,error.description);
         
     }];
    
    
}




-(void)getCities:(NSString *)cityName completionBlock:(void(^)(int status,id response))cmpblock
{

    [self POST:@"get-cities" parameters:@{@"cityName":cityName} success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"error"]boolValue])
         {
             
             cmpblock(1,responseObject);
         }
         else
         {
             
             cmpblock(0,responseObject);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         
         cmpblock(1,error.description);
         
     }];

}

-(void)getCityLocations:(NSString *)location completionBlock:(void(^)(int status,id response))cmpblock
{
        

    GMSPlacesClient *placeClient = [[GMSPlacesClient alloc]init];
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc]init];
    filter.type = kGMSPlacesAutocompleteTypeFilterAddress;

    [placeClient autocompleteQuery:location bounds:nil filter:filter callback:^(NSArray *results, NSError *error)
    {
        if (error == nil)
        {
            cmpblock(0,results);
        }
        else
        {
            cmpblock(1,error.description);
        }
    
    }];


}

-(void)getCoordinates:(GMSPlace *)place completionBlock:(void(^)(int status,id response))cmpblock
{


}

-(void)getHotelswithParameters:(NSDictionary *)data completionBlock:(void(^)(int status,id response))compblock
{
    
    [self POST:@"hotels" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
     {
		 NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
		 NSLog( @"success: %d", [r statusCode] );
         if ([[responseObject objectForKey:@"error"] boolValue])
         {
             compblock(1,responseObject);
         }
         else
         {
             compblock(0,responseObject);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         compblock(1,error.description);
		 NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
		 NSLog( @"failed: %d", [r statusCode] );
     }];
    
}

-(void)getTour:(NSDictionary *)data completionBlock:(void(^)(int status,id response))compblock
{

    
    [self POST:@"tour" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
     {
         
         if ([[responseObject objectForKey:@"error"] boolValue])
         {
             compblock(1,responseObject);
         }
         else
         {
             compblock(0,responseObject);
         }
         
         
     } failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         compblock(1,error.description);
     }];


}


-(void)getOnlyFeaturedHotelsCompletionBlock:(void(^)(int status,id response))cmpBlock
{

    [self GET:@"featured-hotels" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
    }];

}

-(void)getOnlyFeaturedToursCompletionBlock:(void(^)(int status,id response))cmpBlock
{
    
    [self GET:@"featured-hotels" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
     {
         cmpBlock(0,responseObject);
     }
      failure:^(NSURLSessionDataTask *task, NSError *error)
     {
         cmpBlock(1,error.description);
     }];
    
}


-(void)checkInterHotel:(NSDictionary *)data compBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"internal-hotel-availability" parameters:data success:^(NSURLSessionDataTask *task, id responseObject) {
        NSLog(@"%@",responseObject);
        
        cmpBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        cmpBlock(1,error.description);
        NSLog(@"%@",error.description);
    }];
}

-(void)recentSearcheswithUserID:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlck
{

    [self POST:@"recent-searches" parameters:@{@"userId":id_user} success:^(NSURLSessionDataTask *task, id responseObject)
    {
                    cmpBlck(0,responseObject[@"recentSearches"]);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
       
        cmpBlck(1,error.description);
    }];
}


-(void)userReservations:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlck
{


    [self POST:@"reservations" parameters:@{@"userId":id_user} success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlck(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlck(1,error.description);
        
    }];
}


-(void)userTourReservations:(NSString *)id_user completionBlock:(void(^)(int status,id response))cmpBlock
{
    [self POST:@"reserved-tours" parameters:@{@"userId":id_user} success:^(NSURLSessionDataTask *task, id responseObject)
    {
       
        
        cmpBlock(0,responseObject);
        
    }
       failure:^(NSURLSessionDataTask *task, NSError *error)
    {
     
        cmpBlock(1,error.description);
        
    }];


}




-(void)bookHotel:(NSDictionary *)params completionBlock:(void(^)(int status,id response))cmpBlock
{


    [self POST:@"book-hotel" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
       cmpBlock(0,error.description);
    }];
}


-(void)getReservedHotel:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"reserved-hotel" parameters:@{@"bookingId":bookingID} success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
       
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
    }];

}

-(void)getReservedRoom:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"reserved-room" parameters:@{@"bookingId":bookingID} success:^(NSURLSessionDataTask *task, id responseObject)
    {
        
        cmpBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
        
    }];


}

-(void)cancelHotel:(NSString *)bookingID completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"cancel-hotel" parameters:@{@"bookingId":bookingID} success:^(NSURLSessionDataTask *task, id responseObject)
    {
    
        cmpBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
    
        cmpBlock(1,error.description);

    }];

}



-(void)bookTour:(NSDictionary *)params completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"book-tour" parameters:params success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
        
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
    }];

}



-(void)getTourFilterOptionsWithCompletionBlock:(void(^)(int status,id response))cmpBlock
{

    [self GET:@"get-tours-filter-options" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
        
    } failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
        
    }];
}

-(void)filterTours:(NSDictionary *)data completionBlock:(void(^)(int status,id response))cmpBlock
{
    [self POST:@"tours" parameters:data success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
    }
       failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error.description);
    }];

}


-(void)getTourPlaces:(NSString *)place completionBlock:(void(^)(int status,id response))cmpBlock
{

    [self POST:@"tour-places" parameters:@{@"placeName":place} success:^(NSURLSessionDataTask *task, id responseObject)
    {
        cmpBlock(0,responseObject);
    }
    failure:^(NSURLSessionDataTask *task, NSError *error)
    {
        cmpBlock(1,error);
    }];


}


-(void)cancelAllConnections
{
    for (NSURLSessionTask *t in self.tasks)
    {
        [t cancel];
    }
}

@end
