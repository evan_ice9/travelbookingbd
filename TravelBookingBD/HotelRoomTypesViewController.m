//
//  HotelRoomTypesViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/2/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HotelRoomTypesViewController.h"

@interface HotelRoomTypesViewController ()

@end

@implementation HotelRoomTypesViewController
{

    int expand_amount;
    int height_margin;
    int view_height;
    
    NSMutableDictionary *expandAmountData;
    NSArray *dataRoom;
    hotel *dataHotel;
    MBProgressHUD *hud;
    BOOL isReserved;
    double minimumPrice;

}

@synthesize nameHolder,scrollView,discount_view;
@synthesize Hoteltitle,ratingView,price,currency,cityName;
@synthesize img1,img2,img3,container,containerHeight,spinner,parameters;

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self setupRoomTypes];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    CAShapeLayer *shape=[[CAShapeLayer alloc]init];
    [shape setPath:[Utils createPathwithframe:discount_view.bounds].CGPath];
    [discount_view.layer setMask:shape];

}


-(void)setRoomData:(NSArray *)rooms
{
    [spinner stopAnimating];
    dataRoom = rooms;
    [self setupRoomTypes:rooms];
}


-(void)setAttributes:(hotel *)mainData
{
    dataHotel = mainData;
    
    [Hoteltitle setText:[mainData getName]];
    [cityName setText:[mainData getCityName]];
    
    if (roundf([mainData getLowestPrice])==[mainData getLowestPrice])
    {
        [price setText:[NSString stringWithFormat:@"%.f",[mainData getLowestPrice]]];
    }
    else
    {
        [price setText:[NSString stringWithFormat:@"%.2f",[mainData getLowestPrice]]];
    }
    
    minimumPrice = [mainData getLowestPrice];
    
    ratingView.rating = [mainData getRating];
    ratingView.canEdit = NO;
    
    [currency setText:[mainData getCurrencySymBol]];
    
    if ([mainData images].count>0)
    {
        [img1 setImageWithURL:[NSURL URLWithString:[mainData images][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
        
        if ([mainData images].count>1)
        {
            
            [img2 setImageWithURL:[NSURL URLWithString:[mainData images][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            
            if ([mainData images].count>2)
            {
                [img3 setImageWithURL:[NSURL URLWithString:[mainData images][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            }
        }
        
    }


}


-(void)setBookingData:(NSDictionary *)data
{

    isReserved = YES;
    [Hoteltitle setText:data[@"hotelName"]];
    [cityName setText:data[@"city"][@"name"]];
    [price setText:[NSString stringWithFormat:@"%@",data[@"amount"]]];
    ratingView.rating = [data[@"rating"] intValue];
    ratingView.canEdit = NO;
    
    [currency setText:[Utils getCurrencySymBol:data[@"currency"]]];
    
    if ([data[@"images"] count]>0)
    {
        [img1 setImageWithURL:[NSURL URLWithString:data[@"images"][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
        
        if ([data[@"images"] count]>1)
        {
            
            [img2 setImageWithURL:[NSURL URLWithString:data[@"images"][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            
            if ([data[@"images"] count]>2)
            {
                [img3 setImageWithURL:[NSURL URLWithString:data[@"images"][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            }
        }
        
    }

}

-(void)setupRoomTypes:(NSArray *)data
{
    img1.tag = 900;
    UITapGestureRecognizer *tapper1 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    [img1 addGestureRecognizer:tapper1];
    img1.userInteractionEnabled = YES;
    
    img2.tag = 901;
    UITapGestureRecognizer *tapper2 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    [img2 addGestureRecognizer:tapper2];
    img2.userInteractionEnabled = YES;
    
    img3.tag = 902;
    UITapGestureRecognizer *tapper3 =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    [img3 addGestureRecognizer:tapper3];
    img3.userInteractionEnabled = YES;
    
    height_margin = 8;
    int start_position = nameHolder.frame.origin.y+nameHolder.frame.size.height+height_margin;
    view_height=40;
    view_count = data.count;
    //expand_amount=100;
    expandData = [[NSMutableArray alloc]init];
    bookingData = [[NSMutableArray alloc]init];
    expandAmountData = [[NSMutableDictionary alloc]init];
    scrollView.delaysContentTouches=NO;
    
    for (int i=0; i<view_count; i++)
    {
        
        UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapOnView:)];
        UIView *roomtype_view=[[UIView alloc]initWithFrame:CGRectMake(10, start_position+i*(view_height+height_margin), 300, view_height)];

        //add lable
        UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 200, 20)];
        [l setText:data[i][@"name"]];
        [l setFont:[UIFont fontWithName:@"Helvetica-Bold" size:14]];
        [l setTextColor:[Utils colorFromHex:@"#999999"]];
        [roomtype_view addSubview:l];
        
        
        //add price
        double priceValue = [data[i][@"price"] doubleValue];
        minimumPrice = priceValue < minimumPrice?priceValue:minimumPrice;
        UILabel *pricelbl = [[UILabel alloc]initWithFrame:CGRectMake(230, 10, 80, 20)];
        [pricelbl setText:[NSString stringWithFormat:@"%@%.2f",[Utils getCurrencySymBol:data[i][@"currency"]],priceValue]];
        [pricelbl setFont:[UIFont fontWithName:@"helvetica" size:14]];
        [pricelbl setTextColor:[Utils colorFromHex:@"#f57c00"]];
        [roomtype_view addSubview:pricelbl];
        
        
        //add facilities
        UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(0, 40, 300, 1)];
        [seperator setBackgroundColor:[Utils colorFromHex:@"#ededed"]];
        [roomtype_view addSubview:seperator];
        
        
        int startingY = 50;
        expand_amount = startingY;
        
        NSString *smokingStats = [[[data objectAtIndex:i] objectForKey:@"smoking"] boolValue]?@"smoking allowed":@"smoking not allowed";
        NSString *bedType = [NSString stringWithFormat:@"bed type %@",data[i][@"bedType"]];
        NSString *description = data[i][@"description"];
        NSArray *stats =  @[smokingStats,bedType,description];
        
        for (int k = 0; k<3; k++)
        {
            //UILabel *f = [[UILabel alloc]initWithFrame:CGRectMake(20, startingY + 20*k , 200, 15)];
            UILabel *f = [[UILabel alloc]init];
            [f setText:[NSString stringWithFormat:@"+ %@",stats[k]]];
            [f setFont:[UIFont fontWithName:@"Helvetica" size:12]];
            [f setTextColor:[Utils colorFromHex:@"#999999"]];
            int height = [f.text sizeWithFont:[UIFont fontWithName:@"Helvetica" size:12] forWidth:260 lineBreakMode:NSLineBreakByWordWrapping].height;
            [f setFrame:CGRectMake(20, startingY + height*k , 260, height)];
            f.lineBreakMode = NSLineBreakByWordWrapping;
            f.numberOfLines = 0;
            [f sizeToFit];
            height = f.frame.size.height;
            expand_amount +=height;
            [roomtype_view addSubview:f];
        }
        
        
        //add button
        UIButton *btn=[[UIButton alloc]initWithFrame:CGRectMake(0, view_height+expand_amount-30, 300, 30)];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#f57c00"]] forState:UIControlStateNormal];
        [btn setTitle:@"BOOK IT" forState:UIControlStateNormal];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:13]];
        btn.tag = i;
        [btn addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        btn.exclusiveTouch=YES;
        [roomtype_view addSubview:btn];
        
        //add gesture and color
        roomtype_view.backgroundColor=[UIColor whiteColor];
        [roomtype_view addGestureRecognizer:tap];
        roomtype_view.tag=40+i;
        roomtype_view.clipsToBounds=YES;
        [expandData insertObject:[NSNumber numberWithInt:0] atIndex:i];
        [expandAmountData setObject:[NSNumber numberWithInt:expand_amount] forKey:[NSString stringWithFormat:@"%d",i]];

        NSLog(@"%d %d",start_position+i*view_height+height_margin,height_margin);
        [scrollView addSubview:roomtype_view];
        
    }
    
    NSLog(@"%@",expandData);
    
    [price setText:[NSString stringWithFormat:@"%.2f",minimumPrice]];
    
    if ([Utils rounded2decimal:minimumPrice] < [Utils rounded2decimal:[dataHotel getLowestPrice]])
    {
        NSDictionary *d = @{@"price":[NSString stringWithFormat:@"%.2f",minimumPrice]};
        [[NSNotificationCenter defaultCenter]postNotificationName:updateMinimumPrice object:nil userInfo:d];
    }
    
    containerHeight.constant =start_position + view_count*(height_margin+view_height);
    
    CGRect f = container.frame;
    f.size.height = start_position+view_count*(height_margin+view_height);
    container.frame = f;

}


-(void)handleTapOnView:(UITapGestureRecognizer *)tap
{

    NSLog(@"tapped %d",tap.view.tag);
    
    [self adjustView:tap.view.tag];
    

}


-(void)adjustView:(int)tag
{

    UIView *v=(UIView *)[self.view viewWithTag:tag];
    BOOL isBooked;
    int ViewExpandHeight = [[expandAmountData objectForKey:[NSString stringWithFormat:@"%d",tag-40]] intValue];
    
    if (isReserved)
    {
        isBooked = YES;
    }
    else
    {
        isBooked = [bookingData containsObject:[NSNumber numberWithInt:tag-40]]?YES:NO;
    }
    
    NSLog(@"%d",isBooked);
    
    if ([[expandData objectAtIndex:tag-40] intValue]==0)
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame=v.frame;
            frame.size.height+=isBooked?ViewExpandHeight-30:ViewExpandHeight;
            v.frame=frame;
            
            
            for (int i=tag+1; i<tag+view_count;i++)
            {
                UIView *v=(UIView *)[self.view viewWithTag:i];
                CGRect frame=v.frame;
                frame.origin.y+=isBooked?ViewExpandHeight-30:ViewExpandHeight;
                v.frame=frame;
            }
            
        } completion:^(BOOL finished) {
            if (finished)
            {
                [expandData replaceObjectAtIndex:tag-40 withObject:[NSNumber numberWithInt:1]];
                [self adjustScrollView];
            }
        }];

    }
    else if([[expandData objectAtIndex:tag-40] intValue]==1)
    {
    
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect frame=v.frame;
            frame.size.height-=isBooked?ViewExpandHeight-30:ViewExpandHeight;
            v.frame=frame;
            
            
            for (int i=tag+1; i<tag+view_count;i++)
            {
                UIView *v=(UIView *)[self.view viewWithTag:i];
                CGRect frame=v.frame;
                frame.origin.y-=isBooked?ViewExpandHeight-30:ViewExpandHeight;
                v.frame=frame;
            }
            
        } completion:^(BOOL finished) {
            if (finished)
            {
                [expandData replaceObjectAtIndex:tag-40 withObject:[NSNumber numberWithInt:0]];
                [self adjustScrollView];
            }
        }];

    
    
    }


}


-(void)adjustScrollView
{

    UIView *v=[self.view viewWithTag:(view_count-1)+40];
    int estimated_height = v.frame.origin.y+v.frame.size.height;
    int margin=8;
    
    containerHeight.constant = estimated_height+margin;
    CGRect f = container.frame;
    f.size.height = estimated_height+margin;
    container.frame = f;


}


-(void)BookRoom:(NSDictionary *)data withTag:(int)tag
{
    
    hud = [[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:hud];
    [hud show:YES];

    

    [[Network shared_instance]bookHotel:data completionBlock:^(int status, id response)
    {
     
        [hud show:NO];
        [hud removeFromSuperview];
        
        switch (status) {
            case 0:
            {
                NSLog(@"%@",response);
                
                [self.view makeToast:[NSString stringWithFormat:@"Room Booked"]];
                [self adjustView:tag+40];
                [bookingData addObject:[NSNumber numberWithInt:tag]];
                NSDictionary *data = @{@"bookingId":response[@"bookingId"]};
                [[NSNotificationCenter defaultCenter]postNotificationName:setCancelBtn object:nil userInfo:data];
            
            }
                break;
            case 1:
            {
                
                [self.view makeToast:[NSString stringWithFormat:@"Failed"]];
            }
                break;
                
            default:
                break;
        }
        
        
        
    }];


}


-(IBAction)onBtnClick:(UIButton *)sender
{
    
    NSDictionary *temp =[dataRoom objectAtIndex:sender.tag];
    
    if ([[UserObject sharedInstance]IsUserLoggedIn])
    {
     NSDictionary *d=@{@"userId":[[UserObject sharedInstance] getUserID],@"totalAmount":temp[@"price"],@"currency":temp[@"currency"],@"bookingDate":parameters[@"checkInDate"],@"hotelId":[dataHotel getID],@"roomId":temp[@"id"],@"source":temp[@"source"],@"searchId":temp[@"searchId"]};
        
        [self BookRoom:d withTag:sender.tag];
        
        NSLog(@"btn clicked");

    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SideMenuSignIn object:nil];
       
    }
    
}

-(void)imageOnClick:(UITapGestureRecognizer *)tap
{
    [self.delegate didOpenImageSlider:YES index:tap.view.tag-900];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
