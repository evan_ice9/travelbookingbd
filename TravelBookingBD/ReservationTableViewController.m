//
//  ReservationViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "ReservationTableViewController.h"

@interface ReservationTableViewController ()

@end

@implementation ReservationTableViewController
{

    NSArray *mainData;
    BOOL isLoading;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    [self.tableView.tableFooterView setBackgroundColor:[UIColor clearColor]];
    [self showLoadingView];
    
    if ([[UserObject sharedInstance]IsUserLoggedIn])
    {
        [self loadTableData:self.type];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



-(void)loadTableData:(ReservationType)r
{


    if (r==hotelRerservations)
    {
        [[Network shared_instance]userReservations:[[UserObject sharedInstance]getUserID] completionBlock:^(int status, id response)
        {
        
            switch (status) {
                case 0:
                {
                    [self hideLoadingView];
                    NSLog(@"%@",response);
                    mainData = response[@"reservations"];
                    [self.tableView reloadData];
                    isLoading = NO;
                    
                }
                    break;
                case 1:
                {
                    NSLog(@"failed");
                    isLoading = NO;
                }
                    break;
                    
                default:
                    break;
            }
            
        }];
    }
    else if (r == tourReservations)
    {
    
    
        NSLog(@"tour reservations");
        
        
        [[Network shared_instance]userTourReservations:[[UserObject sharedInstance]getUserID] completionBlock:^(int status, id response)
        {
            
            
            switch (status)
            {
                case 0:
                {
                
                    
                    mainData = response[@"reserved"];
                    [self hideLoadingView];
                    [self.tableView reloadData];
                    isLoading = NO;
                
                }
                    break;
                    
                case 1:
                {
                 
                    NSLog(@"failed");
                    isLoading = NO;
                    
                }
                    break;
                    
                default:
                    break;
            }
            
            
            
           
        }];
    
    }


}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return mainData.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *cell_Identifier= self.type==tourReservations?@"cell_reserve_tour":@"cell_reserve";
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cell_Identifier forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    /*UIView *container=(UIView *)[cell viewWithTag:420];
    
    [Utils roundedLayer:container.layer radius:1.0f shadow:YES bounds:container.bounds];
    
    NSDictionary *data = [mainData objectAtIndex:indexPath.row];
    
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:400];
    UILabel *hotelName = (UILabel *)[cell viewWithTag:401];
    UILabel *cityName = (UILabel *)[cell viewWithTag:402];
    UILabel *price = (UILabel *)[cell viewWithTag:403];
    UILabel *currencySymbol = (UILabel *)[cell viewWithTag:404];
    
    [imgView setImageWithURL:[NSURL URLWithString:data[@"images"][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
    [hotelName setText:data[@"hotelName"]];
    [cityName setText:data[@"city"][@"name"]];
    [price setText:[NSString stringWithFormat:@"%@",data[@"amount"]]];
    [currencySymbol setText:[Utils getCurrencySymBol:data[@"currency"]]];
    
    
    UIView *checkInView = [cell viewWithTag:50];
    UIView *checkOutView = [cell viewWithTag:51];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *inDate = [formatter dateFromString:data[@"checkInDate"]];
    NSDate *outDate = [formatter dateFromString:data[@"checkOutDate"]];
    
    
    [self setTextDate:checkInView date:inDate];
    [self setTextDate:checkOutView date:outDate];
    
    
    UILabel *checkInTime = (UILabel *)[cell viewWithTag:70];
    UILabel *checkOutTime = (UILabel *)[cell viewWithTag:71];
    
    [checkInTime setText:[NSString stringWithFormat:@"Check in %@",data[@"checkInTime"]]];
    [checkOutTime setText:[NSString stringWithFormat:@"Check out %@",data[@"checkOutTime"]]];*/
    
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    switch (self.type)
    {
        case hotelRerservations:
        {
            [[NSNotificationCenter defaultCenter]postNotificationName:bookingDataShow object:nil userInfo:mainData[indexPath.row]];
            
        }
            break;
        case tourReservations:
            NSLog(@"tour");
            break;
            
        default:
            break;
    }


}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (scrollView.contentOffset.y<-40 && !scrollView.isDecelerating)
    {
        if (!isLoading)
        {
            NSLog(@"reload");
            isLoading = YES;
            [self showLoadingView];
            
            if ([[UserObject sharedInstance]IsUserLoggedIn])
            {
                [self loadTableData:self.type];
            }

            
        }
        
        
    }
    

}

-(void)setTextDate:(UIView *)view date:(NSDate *)d
{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *c = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:d];
    
    UILabel *month = (UILabel *)[view viewWithTag:60];
    UILabel *weekDay = (UILabel *)[view viewWithTag:61];
    UILabel *day = (UILabel *)[view viewWithTag:62];
    
    [month setText:[NSString stringWithFormat:@"%@",[Utils getMonthOfYear:c.month]]];
    [weekDay setText:[NSString stringWithFormat:@"%@",[Utils getDayOfWeek:c.weekday]]];
    [day setText:[NSString stringWithFormat:@"%d",c.day]];
    
    
}


-(void)showLoadingView
{

    UIView *activity = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    UIActivityIndicatorView *aI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [aI startAnimating];
    [aI setColor:[Utils colorFromHex:@"#8BC34A"]];
    [activity addSubview:aI];
    aI.center = CGPointMake(160, 30);
    [activity setBackgroundColor:[UIColor clearColor]];
    
    self.tableView.tableHeaderView = activity;

}



-(void)hideLoadingView
{
    self.tableView.tableHeaderView = nil;
}




@end
