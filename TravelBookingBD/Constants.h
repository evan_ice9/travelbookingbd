//
//  Header.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

@interface Constants




//urls local 192.168.1.111
// server 23.253.89.236
//server 2 104.130.155.191

//afeef@ice9interactive.com
//access123**


#define base_url @"http://104.130.155.191/travelbooking/backend/web/api/1.0/"
#define GPlus_ClientID @"782688046035-hgb6oraotq7s7ti6g0o5bignajvpodvr.apps.googleusercontent.com"

#define IOS_APP_API_PRIVATE_KEY @"uAVe-PxZFtakGJw_zdN3uZC7u0o7X8sZ"
#define IOS_APP_API_PUBLIC_KEY @"ios-app"


#define Side_Menu_Did_Scroll_To_Position @"scrolled"
#define tabview_did_scroll @"tab scrolled"
#define Handle_mask_layer @"mask_layer"
#define Side_Menu_Visibility @"visible"


#define UserLoggedIn @"loggedIn"
#define UserLoginType @"loginType"
#define UserEmail @"email"
#define UserphotoURL @"photoUrl"
#define UserName @"userName"
#define UserFbID @"fbID"
#define UserDOB @"DOB"
#define UserLocation @"location"
#define UserID @"user_ID"
#define UserZip @"userZip"
#define UserNationality @"nationality"
#define UserPassport @"passport"
#define UserCity @"user_city"
#define UserSearchData @"userSearchData"

#define ObjHotels @"hotels"
#define ObjTours @"tours"

#define SideMenu_Action @"action"
#define Reload_Table @"reload"
#define hotelDetailView @"hotelDetail"
#define searchResult @"searchResult"
#define ToHotelRooms @"hotelRooms"
#define ToTourAccomodations @"accomodations"
#define setCancelBtn @"cancelBtn"


#define bookingDataShow @"bookingData"
#define SideMenuSignIn @"signIn"
#define SideMenuSignUp @"signUp"
#define showFeatureHotel @"featureHotel"
#define showFeatureTour @"featureTour"
#define offlineSearchArray @"offlineArray"


#define updateMinimumPrice @"updatePrice"
@end