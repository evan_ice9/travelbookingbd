//
//  ReservationViewTableController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignInViewController.h"
#import "HotelOverViewController.h"


typedef enum
{
hotelRerservations,
tourReservations

}ReservationType;


@interface ReservationTableViewController : UITableViewController

@property ReservationType type;

@end
