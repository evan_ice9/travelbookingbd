//
//  AboutViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *table;

@end
