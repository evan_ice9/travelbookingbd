//
//  SideMenuControllerTableViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+AFNetworking.h>

@protocol scrolingDelegate<NSObject>

-(void)scrollPosition:(float)x;

@end


@interface SideMenuController : UIViewController<UITableViewDataSource,UITableViewDelegate>
{

    NSArray *_dataSource;
    NSArray *_iconArray;
    NSIndexPath *_indexPath;
}

@property (strong, nonatomic) IBOutlet UITableView *table;




@property CGFloat topOffset;
@property CGFloat slideAmount;
@property CGFloat peekAmount;
@property CGFloat firstX;
@property CGFloat firstY;
@property BOOL sideMenuOpen;
@property UIPanGestureRecognizer *panGesture;
@property (nonatomic,weak) id<scrolingDelegate> delegate;



@end
