//
//  ProfileViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK.h>
#import "SearchViewController.h"

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *propic;
@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *SignOut;
@property (weak,nonatomic) SearchViewController *topController;

- (IBAction)signOut:(id)sender;
- (IBAction)editBtn:(id)sender;
- (IBAction)back:(id)sender;



@end
