//
//  FeaturedHotelViewController.m
//  TravelBookingBD
//
//  Created by Amit Kumar Saha on 2/15/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "FeaturedHotelViewController.h"

@interface FeaturedHotelViewController ()

@end

@implementation FeaturedHotelViewController
{

    NSArray *Maindata;
    UIView *maskView;
    UIView *v;
    UIPickerView *pickerView;
    UIView *containerView;
    UIView *calendarContainerView;
    NSArray *pickerData;
    PDTSimpleCalendarViewController *calendarViewController;
    
    UILabel *roomsCount,*nightsCount,*adultsCount,*childrenCount,*currency;
    int pickerViewSelectedIndex;
    NSDate *checkInDate;
    NSDate *checkOutDate;
    NSDate *selectedDate;
    UITapGestureRecognizer *tap;
    MBProgressHUD *hud;
	
}

@synthesize table,spinner;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    table.delegate=self;
    table.dataSource=self;
    
 
    checkInDate = [[NSDate date] dateByAddingTimeInterval:4*24*60*60];
    checkOutDate = [checkInDate dateByAddingTimeInterval:24*60*60];
    
    
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 70)];
    [self loadHotelData];
    
    // Do any additional setup after loading the view.
}




-(void)loadHotelData
{
    [spinner startAnimating];
    Maindata=nil;
    [self.table reloadData];
    
    
    [[Network shared_instance] getOnlyFeaturedToursCompletionBlock:^(int status, id response) {
        switch (status) {
            case 0:
            {
                [spinner stopAnimating];
                Maindata = response[@"hotels"];
                [self.table reloadData];
            }
                break;
            case 1:
            {
                NSLog(@"featured hotel load failed");
                [spinner stopAnimating];
            }
                
            default:
                break;
        }
    }];



}

-(void)dimIn
{
    
    maskView=[[UIView alloc]initWithFrame:self.view.bounds];
    maskView.backgroundColor=[UIColor blackColor];
    maskView.alpha=0;
    [self.view addSubview:maskView];
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=.8;
    }];
    
    
    
}

-(void)dimOut
{
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=0;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [maskView removeFromSuperview];
            maskView=nil;
            
        }
    }];
    
}



#pragma mark TableView Delegates

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *cellIdentifier=@"cell";
    
    UITableViewCell *cell=[table dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *Name = (UILabel *)[cell viewWithTag:50];
    UILabel *cityName = (UILabel *)[cell viewWithTag:51];
    UILabel *price = (UILabel *)[cell viewWithTag:52];
    UIImageView *hotelImageView = (UIImageView *)[cell viewWithTag:53];
    UILabel *currencySymbol = (UILabel *)[cell viewWithTag:80];
    
    UIView *container = [cell viewWithTag:60];
    
    hotel *h= [[hotel alloc]initWithData:Maindata[indexPath.section]];
    
    
    [Name setText:[h getName]];
    [cityName setText:[h getCityName]];
    if (roundf([h getLowestPrice])==[h getLowestPrice] )
    {
        [price setText:[NSString stringWithFormat:@"%.f",[h getLowestPrice]]];
    }
    else
    {
        [price setText:[NSString stringWithFormat:@"%.2f",[h getLowestPrice]]];
    }
    
    [currencySymbol setText:[h getCurrencySymBol]];
    
    if ([h images].count>0)
    {
        NSString *imageUrl = [h images][0][@"src"];
        [hotelImageView setImageWithURL:[NSURL URLWithString:imageUrl]];
    }
    
    ASStarRatingView *star = (ASStarRatingView *)[cell viewWithTag:58];
    star.rating = [h getRating];
    star.maxRating = 5;
    star.canEdit= NO;
    
    price.minimumScaleFactor=.5;
    price.adjustsFontSizeToFitWidth=YES;

    Name.minimumScaleFactor=.9;
    Name.adjustsFontSizeToFitWidth=YES;
    
    [Utils roundedLayer:container.layer radius:1.0 shadow:YES bounds:container.bounds];

    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    //tableView.layer.masksToBounds=NO;
    //container.layer.masksToBounds=NO;
    
    //[cell.contentView.superview setClipsToBounds:NO];

    return cell;
    
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return Maindata.count;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    NSDictionary *dict=@{@"index":[NSString stringWithFormat:@"%d",indexPath.section]};
    
    [self showModalView:indexPath.section];
    
       //[[NSNotificationCenter defaultCenter]postNotificationName:hotelDetailView object:nil userInfo:dict];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView *header=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 10)];
    header.backgroundColor=[Utils colorFromHex:@"#eeeeee"];
    
    return header;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    return 10;
}


#pragma mark availability check methods


-(void)showModalView:(NSInteger)index
{
    
    [self dimIn];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(NO)}];
    
    v =[[[NSBundle mainBundle]loadNibNamed:@"modalView" owner:self options:nil] firstObject];
    v.layer.cornerRadius = 4.0;
    v.clipsToBounds = YES;
    tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelModalView)];
    [maskView addGestureRecognizer:tap];
    
    [self.view addSubview:v];
    [v setFrame:CGRectMake(30, 40, 260, 262)];
    UIButton *dnBtn =(UIButton *)[v viewWithTag:95];
    dnBtn.tag = index;
    [dnBtn addTarget:self action:@selector(doneBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
    for (int k=90; k<95; k++)
    {
        UIButton *selectBtn= (UIButton *)[v viewWithTag:k];
        [selectBtn addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    roomsCount = (UILabel *)[v viewWithTag:190];
    nightsCount = (UILabel *)[v viewWithTag:191];
    adultsCount = (UILabel *)[v viewWithTag:192];
    childrenCount = (UILabel *)[v viewWithTag:193];
    currency = (UILabel *)[v viewWithTag:194];
    
    UIButton *checkInBtn = (UIButton*)[v viewWithTag:53];
    UIButton *checkOutBtn = (UIButton *)[v viewWithTag:54];
    
    UIView *checkInView = [v viewWithTag:50];
    UIView *checkOutView = [v viewWithTag:51];
    
    [self setTextDate:checkInView date:[[NSDate date] dateByAddingTimeInterval:4*24*60*60]];
    [self setTextDate:checkOutView date:[[NSDate date] dateByAddingTimeInterval:5*24*60*60]];
	
    [checkInBtn addTarget:self action:@selector(popCalenderView:) forControlEvents:UIControlEventTouchUpInside];
    [checkOutBtn addTarget:self action:@selector(popCalenderView:) forControlEvents:UIControlEventTouchUpInside];

}

-(IBAction)doneBtn:(UIButton *)sender
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSString *startDate = [formatter stringFromDate:checkInDate];
    NSString *endDate = [formatter stringFromDate:checkOutDate];
    
    hotel *h= [[hotel alloc]initWithData:Maindata[sender.tag]];

    NSDictionary *params;
    
    if ([[UserObject sharedInstance]IsUserLoggedIn])
    {
       
        //params = @{@"userId":[[UserObject sharedInstance] getUserID],@"currency":currency.text,@"hotelId":[h getID],@"city":[h getCityCode],@"checkInDate":startDate,@"checkOutDate":endDate,@"nationality":@"IN",@"rooms":@[@{@"adults":adults.text,@"children":children.text}]};
        
        params = @{@"userId":[[UserObject sharedInstance] getUserID],@"currency":currency.text,@"hotelId":[h getID],@"city":[h getCityCode],@"checkInDate":startDate,@"checkOutDate":endDate,@"nationality":@"IN",@"rooms":@[@{@"adults":adultsCount.text,@"children":childrenCount.text}]};
    }
    else
    {
        //params = @{@"currency":currency.text,@"hotelId":[h getID],@"city":[h getCityCode],@"checkInDate":startDate,@"checkOutDate":endDate,@"nationality":@"IN",@"rooms":@[@{@"adults":adults.text,@"children":children.text}]};
        
        params = @{@"currency":currency.text,@"hotelId":[h getID],@"city":[h getCityCode],@"checkInDate":startDate,@"checkOutDate":endDate,@"nationality":@"IN",@"rooms":@[@{@"adults":adultsCount.text,@"children":childrenCount.text}]};
    }
    
    hud =[[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:hud];
    [hud show:YES];
    [[Network shared_instance]checkInterHotel:params
                                        compBlock:^(int status, id response)
    {
        [hud show:NO];
        [hud removeFromSuperview];
        
        switch (status) {
            case 0:
            {
                NSLog(@"%@",response);
                
                [self showHotelView:response parameters:params];
                
                checkInDate = [[NSDate date] dateByAddingTimeInterval:24*60*60*4];
                checkOutDate = [checkInDate dateByAddingTimeInterval:24*60*60];
            
            }
                break;
            case 1:
                NSLog(@"failure");
                break;
                
            default:
                break;
        }
        
                                            
    }];
    
    [self dimOut];
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(YES)}];
    [v removeFromSuperview];

}



-(void)showHotelView:(NSDictionary *)data parameters:(NSDictionary *)params
{


    hotel *h=[[hotel alloc]initWithData:data];
    
    HotelOverViewController *hc=(HotelOverViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"hotelOverView"];
    hc.mainData = h;
    hc.parameters = params;
    [self presentViewController:hc animated:YES completion:nil];

}


-(void)cancelModalView
{

    [self dimOut];
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(YES)}];
    
    [containerView removeFromSuperview];
    [calendarContainerView removeFromSuperview];
    [v removeFromSuperview];
	
	checkInDate = [[NSDate date] dateByAddingTimeInterval:4*24*60*60];
	checkOutDate = [checkInDate dateByAddingTimeInterval:24*60*60];


}

#pragma mark FilterPickerView delegates

-(IBAction)showPickerView:(UIButton *)sender
{
    int tag = (int)sender.tag;
	switch (tag)
	{
		case 90:
		{
			pickerViewSelectedIndex = [pickerData indexOfObject:roomsCount.text];
			
			NSMutableArray *arr = [[NSMutableArray alloc]init];
			for (int i = 0; i<5; i++)
			{
				[arr addObject:[NSString stringWithFormat:@"%d",i]];
			}
			
			pickerData = arr;

		}
			break;
			
		case 91:
		{
			NSMutableArray *arr = [[NSMutableArray alloc]init];
			for (int i = 0; i<30; i++)
			{
				[arr addObject:[NSString stringWithFormat:@"%d",i]];
			}
			
			pickerData = arr;

         	pickerViewSelectedIndex = [pickerData indexOfObject:nightsCount.text];
		}
			break;
			
		case 92:
		{
			NSMutableArray *arr = [[NSMutableArray alloc]init];
			for (int i = 0; i<17; i++)
			{
				[arr addObject:[NSString stringWithFormat:@"%d",i]];
			}
			
			pickerData = arr;

			pickerViewSelectedIndex = [pickerData indexOfObject:adultsCount.text];
		}
			break;
			
		case 93:
		{
			NSMutableArray *arr = [[NSMutableArray alloc]init];
			for (int i = 0; i<31; i++)
			{
				[arr addObject:[NSString stringWithFormat:@"%d",i]];
			}
			
			pickerData = arr;

			
			pickerViewSelectedIndex = [pickerData indexOfObject:childrenCount.text];
		}			break;
			
		case 94:
		{
			pickerViewSelectedIndex = [pickerData indexOfObject:currency.text];
			pickerData = @[@"USD",@"INR",@"GBP",@"EUR",@"YEN",@"BDT"];
		}
			break;
		default:
			break;
			
	}
	

	
    pickerView=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 240, 270)];
    pickerView.dataSource=self;
    pickerView.delegate=self;
	[pickerView selectRow:pickerViewSelectedIndex inComponent:0 animated:NO];
	
    containerView=[[UIView alloc]initWithFrame:CGRectMake(40, 10, 240, 300)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 270, 240, 30)];
    containerView.backgroundColor=[UIColor whiteColor];
    containerView.layer.cornerRadius=5.0;
    
    [containerView addSubview:pickerView];
    doneBtn.tag=tag;
    doneBtn.backgroundColor=[UIColor grayColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(pickerSelectDone:) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    [self.view addSubview:containerView];
    [self.view bringSubviewToFront:containerView];
    containerView.clipsToBounds = YES;
    
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{

    return 1;

}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;

}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    return   pickerData[row];

}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    pickerViewSelectedIndex = (int)row;

}
-(IBAction)pickerSelectDone:(id)sender
{
 
    UIButton *btn=(UIButton *)sender;
    
    switch (btn.tag) {
        case 90:
		{
            NSLog(@"rooms %@",pickerData[pickerViewSelectedIndex]);
            [roomsCount setText:pickerData[pickerViewSelectedIndex]];
			[adultsCount setText:[NSString stringWithFormat:@"%d",[self adjustAdults:[roomsCount.text intValue]]]];
		}
			
            break;
            
        case 91:
		{
            NSLog(@"nights %@",pickerData[pickerViewSelectedIndex]);
            [nightsCount setText:pickerData[pickerViewSelectedIndex]];
			checkOutDate = [checkInDate dateByAddingTimeInterval:24*60*60*[nightsCount.text intValue]];
			UIView *checkOutView = [v viewWithTag:51];
			[self setTextDate:checkOutView date:checkOutDate];
		}
            break;
            
        case 92:
		{
            NSLog(@"adults %@",pickerData[pickerViewSelectedIndex]);
            [adultsCount setText:pickerData[pickerViewSelectedIndex]];
			[roomsCount setText:[NSString stringWithFormat:@"%d",[self adjustRooms:[adultsCount.text intValue]]]];
		}
            break;
			
            
        case 93:
		{
            NSLog(@"children %@",pickerData[pickerViewSelectedIndex]);
            [childrenCount setText:pickerData[pickerViewSelectedIndex]];
		}
            break;
            
        case 94:
		{
            NSLog(@"currency %@",pickerData[pickerViewSelectedIndex]);
            [currency setText:pickerData[pickerViewSelectedIndex]];
		}
            break;
        default:
            break;
            
    }

    [containerView removeFromSuperview];
    
}




#pragma mark calender Methods

-(IBAction)popCalenderView:(UIButton *)sender
{
    NSLog(@"tapped %d",(int)sender.tag);
    
    int tag = (int)sender.tag;
    
    calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
    NSDateComponents *offset=[[NSDateComponents alloc]init];
    offset.month = -1;
    NSDate *firstDate=(tag==53)?checkInDate:[NSDate dateWithTimeInterval:60*60*24 sinceDate:checkInDate];
    calendarViewController.firstDate = firstDate;
    offset.month=(tag == 53)?12:1;
    calendarViewController.lastDate=(tag == 53)?[calendarViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0]:[calendarViewController.calendar dateByAddingComponents:offset toDate:checkInDate options:0];
    
    
    if (tag==53)
    {
        
        selectedDate = checkInDate;
        
    }
    else if(tag == 54)
    {
        
        selectedDate = checkOutDate;
        
        
    }
    [calendarViewController setSelectedDate:selectedDate];
    [calendarViewController scrollToSelectedDate:NO];
    [calendarViewController.collectionView reloadData];
    [calendarViewController setDelegate:self];
    
    
    
    calendarContainerView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 280, 400)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];

    switch (tag)
    {
            
            
        case 53:
            
            [self addChildViewController:calendarViewController];
            [calendarContainerView addSubview:calendarViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            doneBtn.tag = tag;
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [calendarContainerView addSubview:doneBtn];
            [self.view addSubview:calendarContainerView];
            [self.view bringSubviewToFront:calendarContainerView];
            break;
            
            
            
        case 54:
            
            [self addChildViewController:calendarViewController];
            [calendarContainerView addSubview:calendarViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            doneBtn.tag = tag;
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [calendarContainerView addSubview:doneBtn];
            [self.view addSubview:calendarContainerView];
            [self.view bringSubviewToFront:calendarContainerView];
            
            break;
            
        default:
            break;
    }
    
}

-(void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    selectedDate = date;
	NSLog(@"%@",date);
}


-(void)setTextDate:(UIView *)view date:(NSDate *)d
{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *c = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:d];
    
    UILabel *month = (UILabel *)[view viewWithTag:60];
    UILabel *weekDay = (UILabel *)[view viewWithTag:61];
    UILabel *day = (UILabel *)[view viewWithTag:62];
    
    [month setText:[NSString stringWithFormat:@"%@",[Utils getMonthOfYear:c.month]]];
    [weekDay setText:[NSString stringWithFormat:@"%@",[Utils getDayOfWeek:c.weekday]]];
    [day setText:[NSString stringWithFormat:@"%d",c.day]];


}

-(IBAction)dateConfirmBtn:(UIButton *)sender
{
    NSLog(@"%d",(int)sender.tag);
    
    
    switch ((int)sender.tag)
    {
        case 53:
        {
            
            UIView *checkInView = [v viewWithTag:50];
            UIView *checkOutView = [v viewWithTag:51];
            [self setTextDate:checkInView date:selectedDate];
            [self setTextDate:checkOutView date:[NSDate dateWithTimeInterval:60*60*24 sinceDate:selectedDate]];
            checkInDate = selectedDate;
            checkOutDate = [NSDate dateWithTimeInterval:60*60*24 sinceDate:selectedDate];
            
        
        }
            break;
            
            case 54:
        {
            UIView *checkOutView = [v viewWithTag:51];
            [self setTextDate:checkOutView date:selectedDate];
            checkOutDate = selectedDate;
            
        }
            break;
        default:
            break;
    }
    [self adjustNights];
    [calendarContainerView removeFromSuperview];
}

#pragma mark adjustFilterDelegates

-(int)adjustRooms:(int)adults
{
	
	int rooms = [roomsCount.text intValue];
	
	if (adults>rooms*4)
		rooms = adults%4==0?adults/4:adults/4 + 1 ;
	else if (adults<rooms)
		rooms = adults;
	
	
	return rooms;
	
}


-(int)adjustAdults:(int)rooms
{
	int adults = [adultsCount.text intValue];
	
	if (adults< rooms)
	{
		adults = rooms;
	}
	else if (adults > rooms*4)
	{
		adults = rooms*4;
	}
	
	return adults;
}


-(void)adjustNights
{
	
	NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *c1 = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:checkInDate];
	NSDateComponents *c2 = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:checkOutDate];
	
	NSLog(@"%d %d",c1.day,c2.day);
	
	NSDateComponents *c=[calendar components:(NSCalendarUnitDay) fromDateComponents:c1 toDateComponents:c2 options:0];
	
	[nightsCount setText:[NSString stringWithFormat:@"%d", c.day]];
	
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
