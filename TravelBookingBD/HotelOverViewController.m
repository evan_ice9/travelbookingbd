//
//  HotelOverViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/28/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HotelOverViewController.h"

@interface HotelOverViewController ()

@end

@implementation HotelOverViewController
{

    imageScrollerViewController *isvc;

}

@synthesize holder,header,parameters,bookingData;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showSignInView) name:SideMenuSignIn object:nil];
    
    vc1=(HotelDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"hotel_detail"];
    vc1.delegate = self;
    vc2=(HotelRoomTypesViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"room_types"];
    vc2.delegate = self;
    
    
    tab =[[MGSwipeTabBarController alloc]initWithViewControllers:@[vc1,vc2]];
    tab.view.frame=holder.bounds;
    [holder addSubview:tab.view];
    
    
    for (int i=100; i<102; i++)
    {
    
        UIButton *btn=(UIButton *)[self.view viewWithTag:i];
        [btn addTarget:self action:@selector(tabBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#8bc34a"]] forState:UIControlStateSelected];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#8bc34a"]] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        if (btn.tag==100)
        {
            btn.selected=YES;
        }
        
    }
    
    
    /*UIView *buttonHolder=(UIView *)[self.view viewWithTag:50];
    YIInnerShadowView *shadow=[[YIInnerShadowView alloc]initWithFrame:buttonHolder.frame];
    shadow.shadowRadius=5.0;
    shadow.shadowMask=YIInnerShadowMaskTop;
    shadow.shadowOpacity=.4;
    [buttonHolder addSubview:shadow];
    [buttonHolder.layer setMasksToBounds:YES];*/
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(header.frame.origin.x+2, header.frame.origin.y, header.frame.size.width-2, header.frame.size.height)];
    header.layer.masksToBounds = NO;
    header.layer.shadowColor = [UIColor blackColor].CGColor;
    header.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    header.layer.shadowOpacity = 0.3f;
    header.layer.shadowPath = shadowPath.CGPath;
    header.clipsToBounds = NO;
    

    
    if (bookingData)
    {
        [vc1 setBookingData:bookingData];
        [vc2 setBookingData:bookingData];
        [self fetchBookingData];
        
    }else if (self.mainData)
    {
        [self setData:self.mainData];
        [self fetchHotelDetails];
    }
    
    
    
    // Do any additional setup after loading the view.
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setData:(hotel *)h
{
    vc1.parameters = parameters;
    vc2.parameters = parameters;
    [vc1 setHotel:h];
    [vc2 setAttributes:self.mainData];

}


-(void)fetchHotelDetails
{

    NSDictionary *dict = @{@"hotelId":[self.mainData getID],@"searchId":[self.mainData getSearchID]};
    NSDictionary *dict2 = @{@"hotelId":[self.mainData getID],@"searchId":[self.mainData getSearchID],@"source":[self.mainData getSource]};
    
    
    [[Network shared_instance]getHotelDetails:dict completionBlock:^(int status, id response) {
        
        
        switch (status) {
            case 0:
            {
                NSLog(@"%@",response);
                self.mainData = [[hotel alloc]initWithData:response];
                [vc1 setAttributesExtended:self.mainData withCancellation:NO];
                
                [[Network shared_instance]getRooms:dict2 completionBlock:^(int status, id response)
                 {
                     
                     NSLog(@"%@",response);
                     [vc2 setRoomData:response[@"rooms"]];
                     
                 }];

            }
                
                break;
            case 1:
                NSLog(@"failed");
                break;
                
            default:
                break;
        }
        
        
    }];
    
}

-(void)fetchBookingData
{

    [[Network shared_instance]getReservedHotel:[NSString stringWithFormat:@"%@",bookingData[@"bookingId"]] completionBlock:^(int status, id response)
    {
        switch (status)
        {
            case 0:
            {
                NSLog(@"%@",response);
                hotel *h= [[hotel alloc]initWithData:response];
                [vc1 setAttributesExtended:h withCancellation:YES];
                
                [[Network shared_instance]getReservedRoom:[NSString stringWithFormat:@"%@",bookingData[@"bookingId"]] completionBlock:^(int status, id response)
                {
                    
                    switch (status) {
                        case 0:
                        {
                        
                            NSLog(@"%@",response);
                            [vc2 setRoomData:response[@"rooms"]];
                        }
                            break;
                            
                        case 1:
                        {
                        
                        
                        }
                            break;
                    }
                   
                }];
                
            }
                break;
                
            case 1:
            {
                NSLog(@"%@",response);
                        
            }
                break;
        }
        
    }];


}


-(IBAction)tabBtnPressed:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    if (btn.selected)
    {
        return;
    }
    
    
    btn.selected=YES;
    [tab setSelectedIndex:btn.tag%100 animated:YES];
    
    for (int i=100; i<103; i++)
    {
        if (i!=btn.tag)
        {
            UIButton *temp=(UIButton *)[self.view viewWithTag:i];
            temp.selected=NO;
        }
    }


}


-(void)viewDidAppear:(BOOL)animated
{
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(adjustTabBtn) name:@"tabview did scroll" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(toHotelRooms) name:ToHotelRooms object:nil];
}



-(void)toHotelRooms
{
    [tab setSelectedIndex:1 animated:YES];
}

-(void)showSignInView
{
    SignInViewController *signIn = (SignInViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signIn"];
    [self presentViewController:signIn animated:YES completion:nil];
}

-(void)adjustTabBtn
{
    
    
    int index=[tab selectedIndex];
    
    UIButton *btn=(UIButton *)[self.view viewWithTag:index+100];
    btn.selected=YES;
    
    for (int i=100; i<103; i++)
    {
        if (i!=btn.tag)
        {
            UIButton *temp=(UIButton *)[self.view viewWithTag:i];
            temp.selected=NO;
        }
    }
    
    
    
    
}

#pragma mark imageSliderDelegates

-(void)didOpenImageSlider:(BOOL)value index:(int)indexValue
{

    NSLog(@"%d",value);
    if (value)
    {
        [tab enablePanGesture:NO];
        NSLog(@"clicked");
        isvc = [self.storyboard instantiateViewControllerWithIdentifier:@"image_scroller"];
        isvc.hoteldelegate = self;
        isvc.imageArray = [_mainData images];
        isvc.t =[_mainData getName];
        isvc.selectedIndex = indexValue;
        [self addChildViewController:isvc];
        [self.holder addSubview:isvc.view];
        [isvc didMoveToParentViewController:self];
    }
    else
    {
        [isvc didMoveToParentViewController:nil];
        [isvc removeFromParentViewController];
        [isvc.view removeFromSuperview];
        [tab enablePanGesture:YES];

    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
