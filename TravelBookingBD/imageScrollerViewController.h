//
//  imageScrollerViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 5/31/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HotelDetailViewController.h"
#import "TourDetailViewController.h"
@interface imageScrollerViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *holder;
@property NSArray *imageArray;
@property (strong, nonatomic) IBOutlet UILabel *titleText;
@property (strong, nonatomic) IBOutlet UILabel *paginationText;
@property (strong,nonatomic) NSString *t;
@property int selectedIndex;

@property id<hotelDetailDelegate> hoteldelegate;
@property id<tourDetailDelegate> tourdelegate;

- (IBAction)btnClicked:(id)sender;

@end
