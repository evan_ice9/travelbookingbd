//
//  ReservationTabViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/26/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "ReservationTabViewController.h"

@interface ReservationTabViewController ()

@end

@implementation ReservationTabViewController
{

    YIInnerShadowView *shadow;

}

@synthesize holder,tabController;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUp];
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(adjustTabBtn) name:@"tabview did scroll" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showBookingDetail:) name:bookingDataShow object:nil];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(void)setUp
{
    for (int i=100; i<102; i++)
    {
        UIButton *btn=(UIButton *)[self.view viewWithTag:i];
        [btn addTarget:self action:@selector(onBtnPress:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#8bc34a"]] forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        if (i==100)
        {
            btn.selected = YES;
        }
        
        
        
    }
    
    if ([[UserObject sharedInstance]IsUserLoggedIn])
    {
        uv1 = (ReservationTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"reservation_table_view"];
        uv1.type = hotelRerservations;
        
        uv2 = (ReservationTableViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"reservation_table_view"];
        uv2.type = tourReservations;
        
        
        
        tabController=[[MGSwipeTabBarController alloc]initWithViewControllers:@[uv1,uv2]];
        tabController.view.frame=holder.bounds;
        [holder addSubview:tabController.view];
    }
    else
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:SideMenuSignIn object:nil];
    }
    
    UIView *buttonHolder=(UIView *)[self.view viewWithTag:50];
    if (!shadow)
    {
        shadow=[[YIInnerShadowView alloc]initWithFrame:buttonHolder.frame];
        shadow.shadowRadius=5.0;
        shadow.shadowMask=YIInnerShadowMaskTop;
        shadow.shadowOpacity=.4;
        [buttonHolder addSubview:shadow];
        [buttonHolder.layer setMasksToBounds:YES];
    }
    



}


-(void)adjustTabBtn
{
    
    
    int index=[tabController selectedIndex];
    
    UIButton *btn=(UIButton *)[self.view viewWithTag:index+100];
    btn.selected=YES;
    
    for (int i=100; i<102; i++)
    {
        if (i!=btn.tag)
        {
            UIButton *temp=(UIButton *)[self.view viewWithTag:i];
            temp.selected=NO;
        }
    }
    
    


}

-(void)showBookingDetail:(NSNotification *)not
{

   NSDictionary *bookingInfo = [not userInfo];

    
    HotelOverViewController *hc=(HotelOverViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"hotelOverView"];
    hc.bookingData = bookingInfo;
    [self presentViewController:hc animated:YES completion:nil];
    
}

-(IBAction)onBtnPress:(id)sender
{
    UIButton *btn=(UIButton *)sender;
    if (btn.selected)
    {
        return;
    }
    
    
    btn.selected=YES;
    [tabController setSelectedIndex:btn.tag%100 animated:YES];
    
    for (int i=100; i<102; i++)
    {
        if (i!=btn.tag)
        {
           UIButton *temp=(UIButton *)[self.view viewWithTag:i];
           temp.selected=NO;
        }
    }


}

-(void)promptLogin
{

    if (![[UserObject sharedInstance]IsUserLoggedIn])
    {
        [[NSNotificationCenter defaultCenter]postNotificationName:SideMenuSignIn object:nil];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
