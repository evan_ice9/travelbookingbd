//
//  hotel.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface hotel : NSObject
{

    NSString *hotelID;
    NSString *code;
    NSString *name;
    int rating;
    float lat;
    float lon;
    NSString *location;
    
    
    NSString *cityCode;
    NSString *cityName;
    
    float lowestPrice;
    NSString *lowestPriceCurrency;
    int discount;
    NSString *discountCurrency;
    
    NSArray *images;
    
    NSString *hotelDescription;
    NSArray *facilities;
    
    NSString *searchID;
    
    NSString *source;
    
    NSArray *cancellationPolicy;
    NSString *bookingID;
    BOOL featured;
}


-(id)initWithData:(NSDictionary *)data;

-(NSString *)getID;

-(NSString *)getCode;
-(NSString *)getName;

-(int)getRating;

-(float)getLatitude;

-(float)getLongitude;

-(NSString *)getLocation;

-(NSString *)getCityCode;

-(NSString *)getCityName;

-(float)getLowestPrice;

-(NSString *)getLowestPriceCurrency;

-(int)getDiscount;

-(NSString *)getDiscountCurrency;

-(NSArray *)images;

-(NSString *)getCurrencySymBol;


-(NSString *)getDescription;

-(NSArray *)getFacilities;

-(NSString *)getSearchID;

-(NSString *)getSource;

-(NSArray *)cancellationPolicyArray;

-(NSString *)getBookingID;

-(BOOL)isFeatured;

@end
