//
//  FeaturedHotelViewController.h
//  TravelBookingBD
//
//  Created by Amit Kumar Saha on 2/15/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ASStarRatingView.h>
#import <PDTSimpleCalendar.h>
#import "hotel.h"
#import "HotelOverViewController.h"


@interface FeaturedHotelViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIPickerViewDataSource,UIPickerViewDelegate,PDTSimpleCalendarViewDelegate,PDTSimpleCalendarViewCellDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@end
