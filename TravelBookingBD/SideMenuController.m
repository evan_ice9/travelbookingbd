//
//  SideMenuControllerTableViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "SideMenuController.h"

@interface SideMenuController ()

@end

@implementation SideMenuController

@synthesize panGesture;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.table.dataSource=self;
    self.table.delegate=self;
    
    self.sideMenuOpen=NO;

    panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanGestures:)];
    //panGesture.cancelsTouchesInView = YES;
    panGesture.delaysTouchesBegan = YES;
    self.view.opaque=NO;
    //[self.view addGestureRecognizer:panGesture];
    [panGesture setMinimumNumberOfTouches:1];
    [panGesture setMaximumNumberOfTouches:1];
    
    
    NSDictionary *dict=[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Strings" ofType:@"plist"]];
    _dataSource = [dict objectForKey:@"Sidemenu"];

    _iconArray =[dict objectForKey:@"Sidemenu_icons"];

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reload) name:Reload_Table object:nil];
    
    if ([self.table respondsToSelector:@selector(setSeparatorStyle:)])
    {
        [self.table setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated
{
    if (_indexPath)
    {
        [self.table selectRowAtIndexPath:_indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
    }

}

-(void)viewDidDisappear:(BOOL)animated
{
    //[[NSNotificationCenter defaultCenter]removeObserver:self];
}

-(void)reload
{

   [self.table reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return _dataSource.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *profile_cell=@"profile";
    NSString *normal_cells=@"cell";
    NSString *profile_cell_no_user = @"profile_no_user";
    
    
    UITableViewCell *cell =(indexPath.row==0)?[[UserObject sharedInstance]IsUserLoggedIn]?[tableView dequeueReusableCellWithIdentifier:profile_cell forIndexPath:indexPath]:[tableView dequeueReusableCellWithIdentifier:profile_cell_no_user forIndexPath:indexPath]:[tableView dequeueReusableCellWithIdentifier:normal_cells forIndexPath:indexPath];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    if (indexPath.row==0)
    {
        UserObject *u=[UserObject sharedInstance];
        UILabel *name=(UILabel *)[cell viewWithTag:16];
        UILabel *subTitle = (UILabel *)[cell viewWithTag:17];
        UIImageView *profilePic = (UIImageView *)[cell viewWithTag:15];
        
        if ([u IsUserLoggedIn])
        {
           
            NSString *imageURL;
            if ([u getUserType]==fbAccount)
            {
                imageURL=[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=small",[u userObjectForKey:UserFbID]];
            }
            else if ([u getUserType]==emailAccount)
            {
                imageURL = [u userObjectForKey:UserphotoURL];
            }

            
            [name setText:[u userObjectForKey:UserName]];
            [subTitle setText:@"How are you today"];
            
            [profilePic setImageWithURL:[NSURL URLWithString:imageURL]];
            profilePic.layer.cornerRadius=profilePic.frame.size.height/2;
            profilePic.clipsToBounds=YES;
            
            
        }
        
        
    }
    else
    {
        UILabel *title=(UILabel *)[cell viewWithTag:50];
        [title setText:[_dataSource objectAtIndex:indexPath.row-1]];
        [title sizeToFit];
        
        UIImageView *imageView=(UIImageView *)[cell viewWithTag:60];
        [imageView setImage:[UIImage imageNamed:[_iconArray objectAtIndex:indexPath.row-1]]];
        [imageView setContentMode:UIViewContentModeCenter];
    }
    
    // Configure the cell...
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        NSDictionary *temp_dict=[[NSDictionary alloc]initWithObjectsAndKeys:@"Sign In",@"value",nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:SideMenu_Action object:nil userInfo:temp_dict];
        
        NSLog(@"sign in");
        
    }
    else
    {
    _indexPath=indexPath;
    UITableViewCell *cell=(UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[Utils colorFromHex:@"#29b6f6"]];
    
    UILabel *title=(UILabel *)[cell viewWithTag:50];
    [title setTextColor:[UIColor whiteColor]];
    
    UIImageView *image_label=(UIImageView *)[cell viewWithTag:55];
    image_label.image = [image_label.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [image_label setTintColor:[UIColor whiteColor]];
    
    UIImageView *icon=(UIImageView *)[cell viewWithTag:60];
    icon.image = [icon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [icon setTintColor:[UIColor whiteColor]];

    NSDictionary *temp_dict=[[NSDictionary alloc]initWithObjectsAndKeys:title.text,@"value",nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:SideMenu_Action object:nil userInfo:temp_dict];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==0)
    {
        return;
    }
    
    UITableViewCell *cell=(UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    [cell setBackgroundColor:[UIColor whiteColor]];
    UILabel *title=(UILabel *)[cell viewWithTag:50];
    [title setTextColor:[Utils colorFromHex:@"#b0b0b0"]];
    
    
    UIImageView *image_label=(UIImageView *)[cell viewWithTag:55];
    
    [image_label setImage:[UIImage imageNamed:@"SideBarArrow"]];
    
    UIImageView *icon=(UIImageView *)[cell viewWithTag:60];
    [icon setImage:[UIImage imageNamed:[_iconArray objectAtIndex:indexPath.row-1]]];

}


- (void) handlePanGestures : (UIPanGestureRecognizer *) sender
{
    //[self.view bringSubviewToFront:[(UIPanGestureRecognizer*)sender view]];
    [self.view bringSubviewToFront:self.view];
    CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:sender.view];
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateBegan)
    {
        //self.firstX = 0.0f;//[[sender view] center].x;
        //self.firstY = [[sender view] center].y+self.topOffset;
        self.firstX=self.view.center.x;
        self.firstY=self.view.center.y;
    }
    
    translatedPoint = CGPointMake(translatedPoint.x+self.firstX, self.firstY);
    [self.delegate scrollPosition:self.view.frame.origin.x];
    
    //NSLog(@"%f %f",translatedPoint.x,self.firstX);

    if (translatedPoint.x<self.view.frame.size.width/2+self.slideAmount)
    {
        [[self view] setCenter:translatedPoint];
        //return;
        
    }
    //[[sender view] setCenter:translatedPoint];
    
    
    if ([(UIPanGestureRecognizer*)sender state] == UIGestureRecognizerStateEnded || [sender state] == UIGestureRecognizerStateCancelled || [sender state] == UIGestureRecognizerStateFailed) {
        
        CGFloat velocityX = (0.2*[(UIPanGestureRecognizer*)sender velocityInView:self.view].x);
        CGFloat finalX = translatedPoint.x + velocityX;
        CGFloat finalY = self.firstY;
        
        
        if (finalY < 0) {
            finalY = 0;
        } else if (finalY > 1024) {
            finalY = 1024;
        }
        
        /*if (UIDeviceOrientationIsPortrait([[UIDevice currentDevice] orientation])) {
            if (finalX < 0) {
                //finalX = 0;
            } else if (finalX > 768) {
                //finalX = 768;
            }
            
            if (finalY < 0) {
                finalY = 0;
            } else if (finalY > 1024) {
                finalY = 1024;
            }
        } else {
            if (finalX < 0) {
                //finalX = 0;
            } else if (finalX > 1024) {
                //finalX = 768;
            }
            
            if (finalY < 0) {
                finalY = 0;
            } else if (finalY > 768) {
                finalY = 1024;
            }
        }*/
        
        CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;
        
        [self animateToPoint:finalX yPos:finalY withAnimationDuration:animationDuration];
    }
    
    
}


- (void) animateToPoint : (CGFloat) finalX yPos : (CGFloat) finalY withAnimationDuration : (CGFloat) animationDuration {
    
    if (self.view.frame.origin.x < -160) {
        [self handleCloseSlidingMenuViewControllerwithAnimationDuration:animationDuration];
        
    } else {
        [self handleShowSlidingMenuView : finalX yPos:finalY withAnimationDuration:animationDuration];
    }}


-(void)handleCloseSlidingMenuViewControllerwithAnimationDuration:(CGFloat)animationDuration
{
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidFinish)];
    [self.delegate scrollPosition:self.view.frame.origin.x];
    self.view.frame = CGRectMake(-self.view.frame.size.width, self.topOffset, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    self.sideMenuOpen=NO;
    
    
    //NSDictionary *userInfo=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithBool:self.sideMenuOpen],@"status", nil];
    //[[NSNotificationCenter defaultCenter]postNotificationName:Handle_mask_layer object:nil userInfo:userInfo];
    
}
- (void) handleShowSlidingMenuView : (CGFloat) finalX
                              yPos : (CGFloat) finalY
             withAnimationDuration : (CGFloat) animationDuration{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(animationDidFinish)];
    [self.delegate scrollPosition:self.view.frame.origin.x];
    self.view.frame = CGRectMake(self.slideAmount, self.topOffset, self.view.frame.size.width, self.view.frame.size.height);
    [UIView commitAnimations];
    self.sideMenuOpen=YES;

    
    //NSDictionary *userInfo=[[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithBool:self.sideMenuOpen],@"status", nil];
    //[[NSNotificationCenter defaultCenter]postNotificationName:Handle_mask_layer object:nil userInfo:userInfo];
    
}


-(void)animationDidFinish
{
    
    //NSLog(@"finished");
    
    //NSLog(@"%f",self.view.frame.size.width);
    
   [self.delegate scrollPosition:self.view.frame.origin.x];
    
}



@end
