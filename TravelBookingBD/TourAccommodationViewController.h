//
//  TourAccommodationViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PDTSimpleCalendar.h>
#import <MBProgressHUD.h>
#import "tour.h"
#import "UserObject.h"
#import "TourDetailViewController.h"

@protocol modalMethods<NSObject>

-(void)isModalShow:(BOOL)value;
-(void)isShowToast:(NSString *)msg;

@end

@interface TourAccommodationViewController : UIViewController<PDTSimpleCalendarViewDelegate,PDTSimpleCalendarViewCellDelegate>
@property (strong, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollview;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;


@property (strong, nonatomic) IBOutlet UILabel *tourTitle;
@property (strong, nonatomic) IBOutlet UILabel *price;
@property (strong, nonatomic) IBOutlet UILabel *tourType;
@property (strong, nonatomic) IBOutlet UILabel *currencySymbol;

@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;

@property (strong, nonatomic) IBOutlet UIView *tag1;
@property (strong, nonatomic) IBOutlet UIView *tag2;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIImageView *calendarIcon;

@property (strong, nonatomic) IBOutlet UIView *bookingCard;


@property (weak,nonatomic) id<modalMethods>delegate;
@property (weak,nonatomic) id<tourDetailDelegate>tourDelegate;

-(void)setAttributes:(tour *)t;
-(void)setup:(tour *)t;
- (IBAction)bookTour:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *checkInView;


@end
