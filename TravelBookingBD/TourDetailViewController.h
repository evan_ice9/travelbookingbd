//
//  TourDetailViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tour.h"

@protocol tourDetailDelegate <NSObject>
-(void)didClickImage:(BOOL)value index:(int)index;
@end

@interface TourDetailViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *container;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *height;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UILabel *tourTitle;
@property (strong, nonatomic) IBOutlet UILabel *tourType;
@property (strong, nonatomic) IBOutlet UILabel *priceTag;
@property (strong, nonatomic) IBOutlet UILabel *currencySymbol;
@property (strong, nonatomic) IBOutlet UIView *tag1;
@property (strong, nonatomic) IBOutlet UIView *tag2;
@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;


@property (strong, nonatomic) IBOutlet UILabel *duration;
@property (strong, nonatomic) IBOutlet UILabel *startPlace;
@property (strong, nonatomic) IBOutlet UILabel *endPlace;
@property (strong, nonatomic) IBOutlet UILabel *destinations;
@property (strong, nonatomic) IBOutlet UILabel *offerStart;
@property (strong, nonatomic) IBOutlet UILabel *offerExpires;
@property (strong, nonatomic) IBOutlet UIButton *bookingBtn;

@property id<tourDetailDelegate> delegate;


-(void)setAttributes:(tour *)t;
-(void)setup:(tour *)t;

@end
