//
//  SearchViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/24/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

typedef struct
{
    int month;
    int day;
    int weekDay;
}dateObj;

@implementation SearchViewController
{

    UIView *containerView;
    UIView *maskView;
    
    
    UILabel *checkIn_date;
    UILabel *checkIn_month;
    UILabel *checkIn_day;
    
    UILabel *checkOut_date;
    UILabel *checkOut_month;
    UILabel *checkOut_day;
    
    
    NSDate *checkInDate;
    NSDate *checkOutDate;
    
    
    
    UILabel *roomsCount;
    UILabel *nightsCount;
    UILabel *adultsCount;
    UILabel *childrenCount;
    UILabel *currencyLabel;
    NSString *cityCode;
    NSString *cityName;
    
    
    
    int selection;
    int start;
    NSDate *selectedDate;
    
    
    NSArray *pickerData;
    NSArray *citydata;
    
    int pickerViewSelectedIndex;
    
    NSMutableArray *imageArrayHotel;
    NSMutableArray *imageArrayTour;
    
    PDTSimpleCalendarViewController *calendarViewController;

}


@synthesize formContainer,hotelsContainer,offersContainer,searchText,checkin,checkout,pickerView,errorMessage,inputErrorIcon;
@synthesize autoTable;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)setupView
{
    [self getImages];
    
    

    for (int i=100; i<103; i++)
    {
        __weak UIImageView *tempImage=(UIImageView*)[self.view viewWithTag:i];
        tempImage.layer.cornerRadius=4.0f;
        tempImage.layer.borderColor=[[Utils colorFromHex:@"#c26100"] CGColor];
        tempImage.layer.borderWidth=1.0f;
        tempImage.clipsToBounds=YES;
        
        if (i%100<imageArrayHotel.count)
        {
            NSURLRequest *req = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:imageArrayHotel[i%100]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
            NSCachedURLResponse *res = [[NSURLCache sharedURLCache]cachedResponseForRequest:req];
            if (res)
            {
                NSLog(@"cached %d",i);
            }
            
        [tempImage setImageWithURLRequest:req placeholderImage:[UIImage imageNamed:@"placeholder findHotels"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
        {
            [tempImage setImage:image];
        }
        failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
        {
         NSLog(@"failed %@",error.description);
        }];
    
        }

        
    }
    
    for (int i=200; i<203; i++)
    {
       __weak UIImageView *tempImage=(UIImageView*)[self.view viewWithTag:i];
        tempImage.layer.cornerRadius=4.0f;
        tempImage.layer.borderColor=[[Utils colorFromHex:@"#4c457b"] CGColor];
        tempImage.layer.borderWidth=1.0f;
        tempImage.clipsToBounds=YES;
        
        if (i%200<imageArrayTour.count)
        {
            NSURLRequest *req = [[NSURLRequest alloc]initWithURL:[NSURL URLWithString:imageArrayTour[i%200]] cachePolicy:NSURLRequestReturnCacheDataElseLoad timeoutInterval:30.0];
            NSCachedURLResponse *res = [[NSURLCache sharedURLCache]cachedResponseForRequest:req];
            if (res)
            {
                NSLog(@"cached %d",i);
            }
            
        [tempImage setImageWithURLRequest:req placeholderImage:[UIImage imageNamed:@"placeholder findHotels"] success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             [tempImage setImage:image];
         }
                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error)
         {
             NSLog(@"failed %@",error.description);
         }];
        }
        
    }
    
    
    for (int i = 120; i<123; i++)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.tag = i;
        [btn addTarget:self action:@selector(clickImgBtn:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    for (int i = 220; i<223; i++)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.tag = i;
        [btn addTarget:self action:@selector(clickImgBtn:) forControlEvents:UIControlEventTouchUpInside];

    }
    
    
    //apply shadow to subviews
    [Utils roundedLayer:formContainer.layer radius:2.0f shadow:YES bounds:formContainer.bounds];
    [Utils roundedLayer:hotelsContainer.layer radius:2.0f shadow:YES bounds:hotelsContainer.bounds];
    [Utils roundedLayer:offersContainer.layer radius:2.0f shadow:YES bounds:offersContainer.bounds];
    
    searchText.delegate=self;
    
    
    checkIn_date=(UILabel *)[checkin viewWithTag:62];
    checkIn_month=(UILabel *)[checkin viewWithTag:60];
    checkIn_day=(UILabel *)[checkin viewWithTag:61];
    
    checkOut_date=(UILabel *)[checkout viewWithTag:72];
    checkOut_month=(UILabel *)[checkout viewWithTag:70];
    checkOut_day=(UILabel *)[checkout viewWithTag:71];
    
    roomsCount=(UILabel *)[self.view viewWithTag:190];
    nightsCount=(UILabel *)[self.view viewWithTag:191];
    adultsCount=(UILabel *)[self.view viewWithTag:192];
    childrenCount=(UILabel *)[self.view viewWithTag:193];
    currencyLabel=(UILabel *)[self.view viewWithTag:194];
    searchText.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    
    
    handle_Tap(checkin, self, @selector(popCalenderView:));
    handle_Tap(checkout, self, @selector(popCalenderView:));
    
    pickerData = [MasterDataModel shared_instance].currencies;
    
    checkInDate = [[NSDate date] dateByAddingTimeInterval:4*24*60*60];
    NSDateComponents *components=[[NSDateComponents alloc]init];
    components.day = 1;
    NSCalendar *c =[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    checkOutDate = [c dateByAddingComponents:components toDate:checkInDate options:0];
    
    [self setDate:1];
    
    
    [autoTable setFrame:CGRectMake(20, 60, 260, 137)];
    [autoTable.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [autoTable.layer setBorderWidth:.2];
    autoTable.clipsToBounds = NO;
    [Utils roundedLayer:autoTable.layer radius:2.0 shadow:YES bounds:autoTable.bounds];
    autoTable.layer.masksToBounds = YES;
    start = 0;

}


-(void)popCalenderView:(UITapGestureRecognizer *)gestureRecognizer
{
    
    NSLog(@"tapped %d",gestureRecognizer.view.tag);
    
    int tag = gestureRecognizer.view.tag;
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(NO)}];
    
    calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
    NSDateComponents *offset=[[NSDateComponents alloc]init];
    NSDate *firstDate=(tag==50)?[[NSDate date] dateByAddingTimeInterval:4*24*60*60]:[NSDate dateWithTimeInterval:60*60*24 sinceDate:checkInDate];
    calendarViewController.firstDate = firstDate;
    offset.month=(tag == 50)?12:1;
    calendarViewController.lastDate=(tag == 50)?[calendarViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0]:[calendarViewController.calendar dateByAddingComponents:offset toDate:checkInDate options:0];
    
    
     if (tag==50)
     {
         
       selectedDate = checkInDate;
         
     }
     else if(tag == 51)
     {
         
       selectedDate = checkOutDate;
         
         
     }
    [calendarViewController setSelectedDate:selectedDate];
    [calendarViewController scrollToSelectedDate:NO];
    [calendarViewController.collectionView reloadData];
    [calendarViewController setDelegate:self];
    
    

    containerView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 280, 400)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];
    
    [self dimIn];

    
    switch (tag)
    {
            
            
        case 50:
            
            [self addChildViewController:calendarViewController];
            [containerView addSubview:calendarViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [containerView addSubview:doneBtn];
            [self.view addSubview:containerView];
            [self.view bringSubviewToFront:containerView];
            selection=1;
            break;
            
            
            
        case 51:
            
            [self addChildViewController:calendarViewController];
            [containerView addSubview:calendarViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [containerView addSubview:doneBtn];
            [self.view addSubview:containerView];
            [self.view bringSubviewToFront:containerView];
            selection=2;
            
            break;
            
        default:
            break;
    }

}


-(void)getImages
{

    imageArrayHotel = [[NSMutableArray alloc]init];
    imageArrayTour = [[NSMutableArray alloc]init];
    
    for (hotel *h in [MasterDataModel shared_instance].featuredHotels)
    {
        if ([h images].count>0)
        {
            NSString *topImgUrl=[h images][0][@"src"];
            [imageArrayHotel addObject:topImgUrl];
        }
    }
    
    for (tour *t in [MasterDataModel shared_instance].featuredTours)
    {
        if ([t images].count>0)
        {
            NSString *ImgUrl=[t images][0][@"src"];
            
            [imageArrayTour addObject:ImgUrl];
        }
    }
    


}


#pragma mark TextFieldDelegates

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{

    autoTable.hidden = YES;
    
    return YES;

}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    
    NSString *tempStr = textField.text;
    tempStr = [tempStr stringByReplacingCharactersInRange:range withString:string];
    
    
    if (tempStr.length>0)
    {
        errorMessage.hidden = YES;
        inputErrorIcon.hidden = YES;
    }
    
    
    
    if (tempStr.length > 2)
    {
        [[Network shared_instance]getCities:tempStr completionBlock:^(int status, id response)
        {
            switch (status) {
                case 0:
                {
                   
                    //NSLog(@"%@",response);

                    if ([response[@"cities"] count]>0)
                    {
                        citydata = response[@"cities"];
                         autoTable.hidden = NO;
                        [autoTable reloadData];
                        [autoTable initTableViewDataSourceAndDelegate:^NSInteger(UITableView *tableView, NSInteger section)
                         {
                             return citydata.count;
                         }
                                             setCellForIndexPathBlock:^UITableViewCell *(UITableView *tableView, NSIndexPath *indexPath)
                         {
                             
                             SelectionCell *cell=[tableView dequeueReusableCellWithIdentifier:@"SelectionCell"];
                             [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
                             @try
                             {
                               [cell.lb setText:[NSString stringWithFormat:@"%@,%@",citydata[indexPath.row][@"name"],citydata[indexPath.row][@"countryName"]]];
                             }
                             @catch (NSException *exception)
                             {
                                 NSLog(@"%@",exception.description);
                             }
                             
                             
                             return cell;
                         }
                                                 setDidSelectRowBlock:^(UITableView *tableView, NSIndexPath *indexPath)
                         {
                              SelectionCell *cell=(SelectionCell*)[tableView cellForRowAtIndexPath:indexPath];
                              textField.text=cell.lb.text;
                             cityCode = citydata[indexPath.row][@"cityCode"];
                             cityName = citydata[indexPath.row][@"name"];
                              autoTable.hidden = YES;
                             [textField resignFirstResponder];
                         }];
                        
                    }
                    
                    
                }
                    break;
                    
                default:
                    break;
            }

            
        }];
    }
    else
    {
    
        autoTable.hidden = YES;
    
    }
    
    

    return YES;

}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    [self.view endEditing:YES];
    
}


#pragma mark - PDTSimpleCalendarViewDelegate

//Set the edgesForExtendedLayout to UIRectEdgeNone

-(IBAction)dateConfirmBtn:(id)sender
{
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(YES)}];
    [self dimOut];
    [containerView removeFromSuperview];
    [self setDate:selection];
    [self adjustNights];
    selection=0;

}
- (void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    NSLog(@"Date Selected : %@",date);
    
    selectedDate = date;
    
    switch (selection) {
        case 1:
            {
                checkInDate=date;
                NSDateComponents *c = [[NSDateComponents alloc]init];
                c.day = 1;
                checkOutDate = [calendarViewController.calendar dateByAddingComponents:c toDate:checkInDate options:0];
            }
            break;
        case 2:
            checkOutDate=date;
            break;
            
        default:
            break;
    }
    
    
}

-(void)setDate:(int)select{

     dateObj object;
    switch (select) {
        case 1:
            object = [self componentsFromDate:checkInDate];
            
            checkIn_date.text=[NSString stringWithFormat:@"%d",object.day];
            [checkIn_month setText:[Utils getMonthOfYear:object.month]];
            [checkIn_day setText:[Utils getDayOfWeek:object.weekDay]];
            
            object = [self componentsFromDate:checkOutDate];
            
            checkOut_date.text=[NSString stringWithFormat:@"%d",object.day];
            [checkOut_month setText:[Utils getMonthOfYear:object.month]];
            [checkOut_day setText:[Utils getDayOfWeek:object.weekDay]];
            
            break;
        case 2:
            object = [self componentsFromDate:checkOutDate];
            
            checkOut_date.text=[NSString stringWithFormat:@"%d",object.day];
            [checkOut_month setText:[Utils getMonthOfYear:object.month]];
            [checkOut_day setText:[Utils getDayOfWeek:object.weekDay]];
            
            break;
            
        default:
            break;
    }
    

}


-(dateObj)componentsFromDate:(NSDate *)date
{

    NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned units = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit;
    NSDateComponents *components =[calender components:units fromDate:date];

    dateObj obj;
    
    obj.day = (int)components.day;
    obj.month = (int)components.month;
    obj.weekDay = (int)components.weekday;
    
    return obj;
}

- (BOOL)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller shouldUseCustomColorsForDate:(NSDate *)date
{
    /*if ([self.customDates containsObject:date]) {
        return YES;
    }*/
    
    return NO;
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller circleColorForDate:(NSDate *)date
{
    return [UIColor whiteColor];
}

- (UIColor *)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller textColorForDate:(NSDate *)date
{
    return [UIColor orangeColor];
}


-(void)dimIn
{

    maskView=[[UIView alloc]initWithFrame:self.view.bounds];
    maskView.backgroundColor=[UIColor blackColor];
    maskView.alpha=0;
    [self.view addSubview:maskView];
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=.8;
    }];

    
    
}

-(void)dimOut
{
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=0;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [maskView removeFromSuperview];
            maskView=nil;

        }
    }];

}

#pragma mark FindHotelMethods



-(IBAction)clickImgBtn:(UIButton *)sender
{

    int tag = sender.tag/100;
    
    switch (tag) {
        case 1:
        {
        
            [[NSNotificationCenter defaultCenter]postNotificationName:showFeatureHotel object:nil];
        
        }
            break;

        case 2:
        {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:showFeatureTour object:nil];
            
        }
            break;

        default:
            break;
    }


}



- (IBAction)onBtnPress:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    
    NSMutableArray *data=[[NSMutableArray alloc]init];
    
    switch (btn.tag) {
        case 90:
            NSLog(@"rooms");
            for (int i=0; i<5; i++)
            {
                data[i]=[NSString stringWithFormat:@"%d",start + i];
            }
            pickerData=data;
            pickerViewSelectedIndex = [pickerData indexOfObject:roomsCount.text];
            [self showPickerView:(int)btn.tag];
            break;

        case 91:
            NSLog(@"nights");
            for (int i=0; i<30; i++)
            {
                data[i]=[NSString stringWithFormat:@"%d",i + start];
            }
            pickerData=data;
            pickerViewSelectedIndex = [pickerData indexOfObject:nightsCount.text];
            [self showPickerView:(int)btn.tag];

            break;

        case 92:
            NSLog(@"adults");
            for (int i=0; i<17; i++)
            {
                data[i]=[NSString stringWithFormat:@"%d",i + start];
            }
            pickerData=data;
            pickerViewSelectedIndex = [pickerData indexOfObject:adultsCount.text];
            [self showPickerView:(int)btn.tag];

            break;

        case 93:
            NSLog(@"children");
            for (int i=0; i<10; i++)
            {
                data[i]=[NSString stringWithFormat:@"%d",i + start];
            }
            pickerData=data;
            pickerViewSelectedIndex = [pickerData indexOfObject:childrenCount.text];
            [self showPickerView:(int)btn.tag];

            break;

        case 94:
            {
                
                NSLog(@"currency");
            
                NSMutableArray *arr = [[NSMutableArray alloc]init];

                for (NSDictionary *d in [MasterDataModel shared_instance].currencies)
                {
                    [arr addObject:d[@"currencyCode"]];
                }
            
                pickerData=arr;
                pickerViewSelectedIndex = [pickerData indexOfObject:currencyLabel.text];
                [self showPickerView:(int)btn.tag];
                
            }
            break;

        case 95:
            NSLog(@"find hotels");
            [self findHotels];
            break;

        default:
            break;
    }
    
}


-(void)findHotels
{
    if ([searchText.text isEqualToString:@""])
    {
        errorMessage.hidden = NO;
        inputErrorIcon.hidden = NO;
    }
    else
    {
    
    
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        
        cityCode = (cityCode.length == 0)?@"":cityCode;
        
        NSDictionary *dict;
        if ([[UserObject sharedInstance]IsUserLoggedIn])
        {
            
            dict = @{@"userId":[[UserObject sharedInstance] getUserID],@"currency":currencyLabel.text,@"city":cityCode,@"cityName":cityName,@"checkInDate":[formatter stringFromDate:checkInDate],@"checkOutDate":[formatter stringFromDate:checkOutDate],@"nationality":@"BD",@"rooms":@[@{@"adults":adultsCount.text,@"children":childrenCount.text,@"nights":nightsCount.text,@"rooms":roomsCount.text}],@"offset":@"0",@"limit":@"10"};

        }
        else
        {
            dict = @{@"currency":currencyLabel.text,@"city":cityCode,@"cityName":cityName,@"cityName":[[searchText.text componentsSeparatedByString:@","] objectAtIndex:0],@"checkInDate":[formatter stringFromDate:checkInDate],@"checkOutDate":[formatter stringFromDate:checkOutDate],@"nationality":@"BD",@"rooms":@[@{@"adults":adultsCount.text,@"children":childrenCount.text,@"nights":nightsCount.text,@"rooms":roomsCount.text}],@"offset":@"0",@"limit":@"10"};
            
            [self saveForOffline:dict];
        }
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:searchResult object:nil userInfo:dict];
        
        [self.RSdelegate searchDataDidUpdate];
    }
    
}

-(void)saveForOffline:(NSDictionary *)dict
{
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:offlineSearchArray])
    {
        NSMutableArray *d = [[NSMutableArray alloc]init];
        [d addObject:dict];
        [d addObjectsFromArray:[[[NSUserDefaults standardUserDefaults]objectForKey:offlineSearchArray] mutableCopy]];
        [[NSUserDefaults standardUserDefaults]setObject:d forKey:offlineSearchArray];
    }
    else
    {
        NSMutableArray *d = [[NSMutableArray alloc]initWithObjects:dict,nil];
        [[NSUserDefaults standardUserDefaults]setObject:d forKey:offlineSearchArray];
    }

}


#pragma mark pickerView Delegates


-(IBAction)pickerSelectDone:(id)sender
{
    [containerView removeFromSuperview];
    containerView=nil;
    UIButton *btn=(UIButton *)sender;
    
    switch (btn.tag) {
        case 90:
            NSLog(@"rooms %d",pickerViewSelectedIndex + start);
            [roomsCount setText:[NSString stringWithFormat:@"%d",pickerViewSelectedIndex + start]];
            [adultsCount setText:[NSString stringWithFormat:@"%d",[self adjustAdults:pickerViewSelectedIndex + start]]];
            break;
            
        case 91:
            NSLog(@"nights %d",pickerViewSelectedIndex + start);
            [nightsCount setText:[NSString stringWithFormat:@"%d",pickerViewSelectedIndex + start]];
            checkOutDate = [self adjustCheckOutDate:pickerViewSelectedIndex + start];
            [self setDate:2];
            break;
            
        case 92:
            NSLog(@"adults %d",pickerViewSelectedIndex + start);
           [adultsCount setText:[NSString stringWithFormat:@"%d",pickerViewSelectedIndex + start]];
            [roomsCount setText:[NSString stringWithFormat:@"%d",[self adjustRooms:pickerViewSelectedIndex + start]]];
            break;
            
        case 93:
            NSLog(@"children %d",pickerViewSelectedIndex + start);
           [childrenCount setText:[NSString stringWithFormat:@"%d",pickerViewSelectedIndex + start]];
            break;
            
        case 94:
            NSLog(@"currency");
            [currencyLabel setText:pickerData[pickerViewSelectedIndex]];
            break;
            
        case 95:
            NSLog(@"find hotels");
            break;
            
        default:
            break;

    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(YES)}];
    [self dimOut];
}

-(void)showPickerView:(int)tag
{
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(NO)}];
    pickerView=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 240, 270)];
    pickerView.dataSource=self;
    pickerView.delegate=self;
    [pickerView selectRow:pickerViewSelectedIndex inComponent:0 animated:NO];
    
    
    containerView=[[UIView alloc]initWithFrame:CGRectMake(40, 10, 240, 300)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 270, 240, 30)];
    containerView.backgroundColor=[UIColor whiteColor];
    containerView.layer.cornerRadius=5.0;

    
    [self dimIn];
    [containerView addSubview:pickerView];
    doneBtn.tag=tag;
    doneBtn.backgroundColor=[UIColor grayColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(pickerSelectDone:) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    [self.view addSubview:containerView];
    [self.view bringSubviewToFront:containerView];
    containerView.clipsToBounds = YES;
    
}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
  
    return pickerData[row];
    
    
}


-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    pickerViewSelectedIndex=(int)row;
}



#pragma mark room-adult adjustment Methods


-(int)adjustRooms:(int)adults
{

    int rooms = [roomsCount.text intValue];

    if (adults>rooms*4)
            rooms = adults%4==0?adults/4:adults/4 + 1 ;
    else if (adults<rooms)
        rooms = adults;
   
    
    return rooms;

}


-(int)adjustAdults:(int)rooms
{
    int adults = [adultsCount.text intValue];
    
    if (adults< rooms)
    {
        adults = rooms;
    }
    else if (adults > rooms*4)
    {
        adults = rooms*4;
    }
    
    return adults;
}


-(void)adjustNights
{

	NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
	NSDateComponents *c1 = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:checkInDate];
	NSDateComponents *c2 = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:checkOutDate];
	
	NSLog(@"%d %d",c1.day,c2.day);
	
	NSDateComponents *c=[calendar components:(NSCalendarUnitDay) fromDateComponents:c1 toDateComponents:c2 options:0];
	
	[nightsCount setText:[NSString stringWithFormat:@"%d", c.day]];

}

-(NSDate *)adjustCheckOutDate:(int)nights
{
    NSDate *t = [checkInDate dateByAddingTimeInterval:(nights -1)*24*60*60];
    return t;
}

@end
