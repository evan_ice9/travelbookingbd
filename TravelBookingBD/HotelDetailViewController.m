//
//  HotelDetailViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/28/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HotelDetailViewController.h"

@interface HotelDetailViewController ()

@end

@implementation HotelDetailViewController
{

    hotel *mainData;
}

@synthesize holderScroll,checkin,checkout,cityName,hotelTitle,price,img1,img2,img3,description;

@synthesize currencySymbol,ratingView;

@synthesize isbooking,containerHeight,container,checkInView,checkOutView;

@synthesize parameters,spinner;

-(void)viewDidAppear:(BOOL)animated
{
     UIView *discount_view=(UIView *)[self.view viewWithTag:510];
    
    CAShapeLayer *shape=[[CAShapeLayer alloc]init];
    [shape setPath:[Utils createPathwithframe:discount_view.bounds].CGPath];
    [discount_view.layer setMask:shape];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(instantiateCancelBtn:) name:setCancelBtn object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePrice:) name:updateMinimumPrice object:nil];
}

#pragma mark hotelView methods
-(void)setHotel:(hotel *)h
{
    mainData = h;
    [self setAttributes];
}



-(void)setAttributes
{
    //[addressImageView setImage:[Utils imageNamed:@"address" imagetintColor:[UIColor whiteColor]]];
    checkin.image=[Utils imageNamed:@"icon_calander" imagetintColor:[UIColor whiteColor]];
    checkout.image=[Utils imageNamed:@"icon_calander" imagetintColor:[UIColor whiteColor]];
    
    [hotelTitle setText:[mainData getName]];
    [cityName setText:[mainData getCityName]];
    if (roundf([mainData getLowestPrice])==[mainData getLowestPrice])
    {
      [price setText:[NSString stringWithFormat:@"%.f",[mainData getLowestPrice]]];
    }
    else
    {
      [price setText:[NSString stringWithFormat:@"%.2f",[mainData getLowestPrice]]];
    }
    ratingView.rating = [mainData getRating];
    ratingView.canEdit = NO;


    
    [currencySymbol setText:[mainData getCurrencySymBol]];
    
    
    
    if ([mainData images].count>0)
    {
        [img1 setImageWithURL:[NSURL URLWithString:[mainData images][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
        
        if ([mainData images].count>1)
        {
        
            [img2 setImageWithURL:[NSURL URLWithString:[mainData images][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            
            if ([mainData images].count>2)
            {
                [img3 setImageWithURL:[NSURL URLWithString:[mainData images][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            }
        }
        
    }
    
    
    
    UILabel *chkInDate = (UILabel *)[checkInView viewWithTag:600];
    UILabel *chkInMonth = (UILabel *)[checkInView viewWithTag:601];
    UILabel *chkInDay = (UILabel *)[checkInView viewWithTag:602];
    
    UILabel *chkOutDate = (UILabel *)[checkOutView viewWithTag:700];
    UILabel *chkOutMonth = (UILabel *)[checkOutView viewWithTag:701];
    UILabel *chkOutDay = (UILabel *)[checkOutView viewWithTag:702];
    
    
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *inDate = [f dateFromString:parameters[@"checkInDate"]];
    NSDate *outDate = [f dateFromString:parameters[@"checkOutDate"]];
    

    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *c = [calender components:(NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit) fromDate:inDate];
    
    [chkInDate setText:[NSString stringWithFormat:@"%d",c.day]];
    [chkInMonth setText:[Utils getMonthOfYear:c.month]];
    [chkInDay setText:[Utils getDayOfWeek:c.weekday]];
    
    c = [calender components:(NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit) fromDate:outDate];
    
    [chkOutDate setText:[NSString stringWithFormat:@"%d",c.day]];
    [chkOutMonth setText:[Utils getMonthOfYear:c.month]];
    [chkOutDay setText:[Utils getDayOfWeek:c.weekday]];
    
}

-(void)setAttributesExtended:(hotel *)h withCancellation:(BOOL)cancel
{
    
    UITapGestureRecognizer *tapper1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    UITapGestureRecognizer *tapper2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    UITapGestureRecognizer *tapper3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(imageOnClick:)];
    img1.tag = 900;
    [img1 addGestureRecognizer:tapper1];
    img1.userInteractionEnabled = YES;
    
    img2.tag = 901;
    [img2 addGestureRecognizer:tapper2];
    img2.userInteractionEnabled = YES;
    
    img3.tag = 902;
    [img3 addGestureRecognizer:tapper3];
    img3.userInteractionEnabled = YES;
    
    mainData = h;
    
    [spinner stopAnimating];
    
    int startX,startY,height,margin;
    
    startY = 382;
    startX = 15;
    height = 30;
    margin = 8;
    
    //set booking btn
    if (cancel)
    {
        int cancelViewHeight = [h cancellationPolicyArray].count*30 + 50;
        UIView *cancelView = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, cancelViewHeight)];
        [cancelView setBackgroundColor:[Utils colorFromHex:@"#8bc34a"]];
        
        
        
        for (int i = 0; i<[h cancellationPolicyArray].count; i++)
        {
            UILabel *l  = [[UILabel alloc]initWithFrame:CGRectMake(startX,15+i*30, 290, 20)];
            NSDictionary *d =[h cancellationPolicyArray][i];
            [l setText:[NSString stringWithFormat:@"# %@ %@ %@",d[@"description"],d[@"value"],d[@"currency"]]];
            [l setTextColor:[UIColor whiteColor]];
            [l setFont:[UIFont systemFontOfSize:12]];
            [cancelView addSubview:l];
        }
        
        //startY += startY +[h cancellationPolicyArray].count * 30;

        UIButton *cancelButton = [[UIButton alloc]initWithFrame:CGRectMake(0, cancelViewHeight-height, 320, height)];
        [cancelButton setTitle:@"Cancel Reservation" forState:UIControlStateNormal];
        [cancelButton setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#689f38"]] forState:UIControlStateNormal];
        cancelButton.tag = [[h getBookingID] intValue];
        [cancelButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        cancelButton.exclusiveTouch = YES;
        [cancelButton addTarget:self action:@selector(cancelBooking:) forControlEvents:UIControlEventTouchUpInside];
        [cancelView addSubview:cancelButton];
        cancelView.tag = 1004;
        
        [container addSubview:cancelView];
        
        startY +=cancelViewHeight + margin;
        
        
    }
    else
    {
    
        UIButton *BookItButton = [[UIButton alloc]initWithFrame:CGRectMake(0, startY, 320, height)];
        [BookItButton setTitle:@"Book It" forState:UIControlStateNormal];
        [BookItButton setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#f57c00"]] forState:UIControlStateNormal];
        [BookItButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [BookItButton addTarget:self action:@selector(onBookingBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        BookItButton.exclusiveTouch = YES;
        BookItButton.tag = 1003;
        [container addSubview:BookItButton];
        
        startY +=height + margin;
    
    }
    
    //initialize height

    
    //set description text
    UILabel *descriptionText = [[UILabel alloc]init];
    descriptionText.lineBreakMode = NSLineBreakByWordWrapping;
    descriptionText.numberOfLines = 0;
    NSString *placerText = [h getDescription];
    [descriptionText setFont:[UIFont systemFontOfSize:10]];
    CGSize s = [placerText sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(290, 600) lineBreakMode:NSLineBreakByWordWrapping];
    [descriptionText setFrame:CGRectMake(startX, 25, 290, s.height+20)];
    [descriptionText setText:placerText];
    [descriptionText setTextColor:[Utils colorFromHex:@"#999999"]];
    
    //setup description view
    UIView *descriptionView = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, s.height + 50)];
    [descriptionView addSubview:descriptionText];
    [descriptionView setBackgroundColor:[UIColor whiteColor]];
    descriptionView.tag = 1000;
    
    //set description icon
    UIImageView *iconview = [[UIImageView alloc]initWithFrame:CGRectMake(startX, 10, 20, 20)];
    [iconview setImage:[UIImage imageNamed:@"icon_description"]];
    [iconview setContentMode:UIViewContentModeCenter];
    [descriptionView addSubview:iconview];
    
    //set description label
    UILabel *viewTitle = [[UILabel alloc]initWithFrame:CGRectMake(startX + 25 , 10 , 100, 20)];
    [viewTitle setText:@"Description"];
    [viewTitle setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    [viewTitle setTextColor:[Utils colorFromHex:@"#f57c00"]];
    [descriptionView addSubview:viewTitle];
    
    [container addSubview:descriptionView];
    
    startY +=descriptionView.frame.size.height + margin;
    
    //locationText
    UILabel *locationDetails = [[UILabel alloc]init];
    locationDetails.lineBreakMode = NSLineBreakByWordWrapping;
    locationDetails.numberOfLines = 0;
    NSString *locationText = [h getLocation];
    CGSize c = [locationText sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(290, 500) lineBreakMode:NSLineBreakByWordWrapping];
    [locationDetails setFrame:CGRectMake(startX, 30, 300, c.height)];
    [locationDetails setFont:[UIFont systemFontOfSize:10]];
    [locationDetails setTextColor:[UIColor whiteColor]];
    [locationDetails setText:locationText];
    
    
    //set location View
    int imageViewHeight = 108;
    UIView *locationView = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, c.height + imageViewHeight + 50)];
    [locationView setBackgroundColor:[Utils colorFromHex:@"#f57c00"]];
    [locationView addSubview:locationDetails];
    locationView.tag = 1001;
    
    //set location icon
    UIImageView *locationIconview = [[UIImageView alloc]initWithFrame:CGRectMake(startX, 10, 20, 20)];
    [locationIconview setImage:[UIImage imageNamed:@"address"]];
    [locationIconview setContentMode:UIViewContentModeCenter];
    locationIconview.image = [locationIconview.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];\
    [locationIconview setTintColor:[UIColor whiteColor]];
    [locationView addSubview:locationIconview];
    
    //set description label
    UILabel *locationViewTitle = [[UILabel alloc]initWithFrame:CGRectMake(startX + 25 , 10 , 100, 20)];
    [locationViewTitle setText:@"Location"];
    [locationViewTitle setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    [locationViewTitle setTextColor:[UIColor whiteColor]];
    [locationView addSubview:locationViewTitle];
    
    //set location image
    NSString *urlstr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?center=%f,%f&zoom=11&size=640x216&markers=color:red%%7Clabel:M%%7C%f+%f",mainData.getLatitude,mainData.getLongitude,mainData.getLatitude,mainData.getLongitude];
    UIImageView *locationImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, locationView.frame.size.height-imageViewHeight, 320, imageViewHeight)];
    [locationImage setImageWithURL:[NSURL URLWithString:urlstr] placeholderImage:[UIImage imageNamed:@"placeholder map"]];
    [locationView addSubview:locationImage];
    

    [container addSubview:locationView];
    
    startY += locationView.frame.size.height + margin;
    
    NSString *facilityList = @"";
    for (NSDictionary *d in [h getFacilities])
    {
        facilityList = [facilityList stringByAppendingString:[NSString stringWithFormat:@"+ %@\n",d[@"title"]]];
    }
    
    
    CGSize k = [facilityList sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(290, 600) lineBreakMode:NSLineBreakByWordWrapping];
    
    //facility View
    UIView *facilityView = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, k.height + 40)];
    [facilityView setBackgroundColor:[UIColor whiteColor]];
    facilityView.tag = 1002;
    
    
    //set facility icon
    UIImageView *facilityIconview = [[UIImageView alloc]initWithFrame:CGRectMake(startX, 10, 20, 20)];
    [facilityIconview setImage:[UIImage imageNamed:@"icon_flag"]];
    [facilityIconview setContentMode:UIViewContentModeCenter];
    [facilityView addSubview:facilityIconview];
    
    //set facility label
    UILabel *facilityViewTitle = [[UILabel alloc]initWithFrame:CGRectMake(startX + 25 , 10 , 100, 20)];
    [facilityViewTitle setText:@"Amenities"];
    [facilityViewTitle setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    [facilityViewTitle setTextColor:[Utils colorFromHex:@"#f57c00"]];
    [facilityView addSubview:facilityViewTitle];
    
    
    
    UILabel *list = [[UILabel alloc]initWithFrame:CGRectMake(startX, 40, 290, k.height)];
    list.lineBreakMode = NSLineBreakByWordWrapping;
    list.numberOfLines = 0;
    [list setText:facilityList];
    [list setTextColor:[Utils colorFromHex:@"#999999"]];
    [list setFont:[UIFont systemFontOfSize:10]];
    [facilityView addSubview:list];
    
    [container addSubview:facilityView];
    
    
    CGRect f = container.frame;
    f.size.height = facilityView.frame.origin.y+facilityView.frame.size.height;
    [container setFrame:f];
    containerHeight.constant = facilityView.frame.origin.y+facilityView.frame.size.height;
}

-(void)instantiateCancelBtn:(NSNotification *)not
{
    
    for (UIView *v in container.subviews)
    {
        if (v.tag>=1000)
        {
            [v removeFromSuperview];
        }
    }
    [spinner startAnimating];
    
    
    [[Network shared_instance]getReservedHotel:[not userInfo][@"bookingId"] completionBlock:^(int status, id response)
    {
     
        switch (status) {
            case 0:
            {
                hotel *h = [[hotel alloc]initWithData:response];
                [self setAttributesExtended:h withCancellation:YES];
            }
                break;
                case 1:
            {
            
                NSLog(@"failed");
                [spinner stopAnimating];
            }
                break;
                
            default:
                break;
        }
    }];
    
    
}


#pragma mark bookingView Methods

-(void)setBookingData:(NSDictionary *)dict
{

    
    
    checkin.image=[Utils imageNamed:@"icon_calander" imagetintColor:[UIColor whiteColor]];
    checkout.image=[Utils imageNamed:@"icon_calander" imagetintColor:[UIColor whiteColor]];
    
    [hotelTitle setText:dict[@"hotelName"]];
    [cityName setText:dict[@"city"][@"name"]];
    [price setText:[NSString stringWithFormat:@"%d",[dict[@"amount"] intValue]]];
    ratingView.rating = [dict[@"rating"] intValue];
    ratingView.canEdit = NO;
    
    
    
    [currencySymbol setText:[Utils getCurrencySymBol:dict[@"currency"]]];
    
    [img1 setImageWithURL:[NSURL URLWithString:dict[@"images"][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
    [img2 setImageWithURL:[NSURL URLWithString:dict[@"images"][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
    [img3 setImageWithURL:[NSURL URLWithString:dict[@"images"][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
    
    
    UILabel *chkInDate = (UILabel *)[checkInView viewWithTag:600];
    UILabel *chkInMonth = (UILabel *)[checkInView viewWithTag:601];
    UILabel *chkInDay = (UILabel *)[checkInView viewWithTag:602];
    
    UILabel *chkOutDate = (UILabel *)[checkOutView viewWithTag:700];
    UILabel *chkOutMonth = (UILabel *)[checkOutView viewWithTag:701];
    UILabel *chkOutDay = (UILabel *)[checkOutView viewWithTag:702];
    
    
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *inDate = [f dateFromString:dict[@"checkInDate"]];
    NSDate *outDate = [f dateFromString:dict[@"checkOutDate"]];
    
    
    NSCalendar *calender = [NSCalendar currentCalendar];
    NSDateComponents *c = [calender components:(NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit) fromDate:inDate];
    
    [chkInDate setText:[NSString stringWithFormat:@"%d",c.day]];
    [chkInMonth setText:[Utils getMonthOfYear:c.month]];
    [chkInDay setText:[Utils getDayOfWeek:c.weekday]];
    
    c = [calender components:(NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit) fromDate:outDate];
    
    [chkOutDate setText:[NSString stringWithFormat:@"%d",c.day]];
    [chkOutMonth setText:[Utils getMonthOfYear:c.month]];
    [chkOutDay setText:[Utils getDayOfWeek:c.weekday]];


}


-(IBAction)cancelBooking:(id)sender
{
    NSLog(@"%d",[sender tag]);
    
    [[Network shared_instance]cancelHotel:[NSString stringWithFormat:@"%d",[sender tag]] completionBlock:^(int status, id response)
    {
     
        switch (status) {
            case 0:
            {
                NSLog(@"%@",response);
            }
                break;

            case 1:
            {
                NSLog(@"%@",response);
            
            }
                break;

            default:
                break;
        }
        
    }];

}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    isbooking = NO;
    holderScroll.delaysContentTouches = NO;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBookingBtnPressed:(id)sender
{
    NSLog(@"pressed");
    [[NSNotificationCenter defaultCenter]postNotificationName:ToHotelRooms object:nil];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"tabview did scroll" object:nil];
}


#pragma mark imageSlider

-(void)imageOnClick:(UIGestureRecognizer *)tap
{
    UIView *v = tap.view;
    int tag = v.tag-900;
    [self.delegate didOpenImageSlider:YES index:tag];
    
}

#pragma mark UpdateMinimumPrice

-(void)updatePrice:(NSNotification *)not
{
    NSDictionary *data = [not userInfo];
    [price setText:data[@"price"]];
    [self.view makeToast:@"Congrats you got more affordable price"];
}

@end
