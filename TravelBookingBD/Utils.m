//
//  Utils.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "Utils.h"

@implementation Utils



+(UIColor *)colorFromHex:(NSString *)string
{

    unsigned rgbvalue=0;
    NSScanner *scanner=[NSScanner scannerWithString:string];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbvalue];
    return [UIColor colorWithRed:((rgbvalue & 0xFF0000) >> 16) / 255.0 green:((rgbvalue & 0xFF00) >> 8) / 255.0 blue:(rgbvalue & 0xFF) / 255.0 alpha:1.0];

}

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}



+ (UIImage *)imageNamed:(NSString *)name imagetintColor:(UIColor *)color {
    UIImage *img = nil;
    img = [UIImage imageNamed:name];
    
    UIGraphicsBeginImageContextWithOptions(img.size, NO, 0.0f);
    [color setFill];
    CGRect bounds = CGRectMake(0, 0, img.size.width, img.size.height);
    UIRectFill(bounds);
    [img drawInRect:bounds blendMode:kCGBlendModeDestinationIn alpha:1.0];
    
    UIImage *tintedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
        
    return tintedImage;
    
}

+ (void)roundedLayer:(CALayer *)viewLayer
              radius:(float)r
              shadow:(BOOL)s
              bounds:(CGRect)b
{
    [viewLayer setMasksToBounds:NO];
    [viewLayer setCornerRadius:r];
    if(s)
    {
        [viewLayer setShadowColor:[[self colorFromHex:@"#333333"] CGColor]];
        [viewLayer setShadowOffset:CGSizeMake(0,0)];
        [viewLayer setShadowOpacity:.5];
        [viewLayer setShadowRadius:r];
        viewLayer.shadowPath = [UIBezierPath bezierPathWithRect:b].CGPath;
    }
    return;
}

+(UIBezierPath *)createPathwithframe:(CGRect)frame
{
    
    UIBezierPath *path=[[UIBezierPath alloc]init];
    path.lineJoinStyle=kCGLineJoinRound;
    [path moveToPoint:CGPointMake(0.0, 0.0)];
    [path addLineToPoint:CGPointMake(0.0,frame.size.height)];
    [path addLineToPoint:CGPointMake(frame.size.width, frame.size.height)];
    [path addLineToPoint:CGPointMake(frame.size.width-5.0, 0.0)];
    [path closePath];
    return path;
    
}


+(NSDictionary *)getFormattedDate:(NSString *)date
{


    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-mm-dd"];
    NSDate *d = [dateFormatter dateFromString:date];
    
    
    NSCalendar *calender=[[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned units = NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit;
    NSDateComponents *components =[calender components:units fromDate:d];
    
    NSString *year = [NSString stringWithFormat:@"%d",components.year];
    NSString *month = [NSString stringWithFormat:@"%d",components.month];
    NSString *day = [NSString stringWithFormat:@"%d",components.day];
    
    NSDictionary *dict = @{@"year":year,@"month":month,@"day":day};


    return dict;

}


+(NSString *)getCurrencySymBol:(NSString *)lowestPriceCurrency
{
    
    NSString *symbol =@"$";
    NSArray *arr = [MasterDataModel shared_instance].currencies;
    
    for (NSDictionary *d in arr)
    {
        if ([d[@"currencyCode"] isEqualToString:lowestPriceCurrency])
        {
            symbol = d[@"currencySymbol"];
            break;
        }
    }
    
    return symbol;
}


+(NSString *)getMonthOfYear:(int)month
{
    
    NSArray *months=@[@"January",@"February",@"March",@"April",@"May",@"June",@"July",@"August",@"September",@"October",@"November",@"December"];
    
    return months[month-1];
    
}



+(NSString *)getDayOfWeek:(int )day
{
    
    NSArray *array=@[@"Sunday",@"Monday",@"Tuesday",@"Wednesday",@"Thursday",@"Friday",@"Saturday"];
    
    return array[day-1];
    
    
}


+(NSString *)filterTitle:(NSString *)title
{
	title = [title lowercaseString];
    NSArray *arr = [title componentsSeparatedByString:@" "];
	NSCharacterSet *charset = [NSCharacterSet alphanumericCharacterSet];
    
    NSMutableString *resultStr = [[NSMutableString alloc]init];
    for (NSString *str in arr)
    {
		if ([str isEqualToString:@""])
			continue;
		
		BOOL valid = [[str  stringByTrimmingCharactersInSet:charset] isEqualToString:@""];
		if (valid)
		{
			NSString *s = [str stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:[[str substringToIndex:1] uppercaseString]];
			resultStr = [[resultStr stringByAppendingString:[NSString stringWithFormat:@"%@ ",s]] mutableCopy];
		}
		
    }
    
    return resultStr;
	
}



+(NSString *)hashValue:(NSString *)privateKey publicKey:(NSString *)publicKey
{

	const char *cKey  = [privateKey cStringUsingEncoding:NSUTF8StringEncoding];
	const char *cData = [publicKey cStringUsingEncoding:NSUTF8StringEncoding];
	unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];
	CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
	
	NSString *hash;
	
	NSMutableString* output = [NSMutableString   stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
	
	for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
		[output appendFormat:@"%02x", cHMAC[i]];
	hash = output;
	return [NSString stringWithFormat:@"%@%@%@",[Utils generateRandomString:4],hash,[Utils generateRandomString:6]];


}



+(NSString *)generateRandomString:(int)length
{
	NSMutableString *output = [[NSMutableString alloc]initWithCapacity:length];
	NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	
	for (int i = 0; i<length; i++)
    {
		[output appendFormat:@"%C",[letters characterAtIndex:arc4random()%[letters length]]];
	}

	return output;
}


+(float)rounded2decimal:(float)value
{

	return (value*100 + .5)/100;
}

@end
