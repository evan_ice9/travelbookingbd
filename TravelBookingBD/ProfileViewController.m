//
//  ProfileViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize propic,containerView,SignOut,topController;

- (void)viewDidLoad {
    [super viewDidLoad];
    [Utils roundedLayer:containerView.layer radius:2.0f shadow:YES bounds:containerView.bounds];
    [Utils roundedLayer:SignOut.layer radius:2.0f shadow:YES bounds:SignOut.bounds];
    

    propic.layer.borderColor=[[Utils colorFromHex:@"#dbdbdb"] CGColor];
    propic.layer.borderWidth=2.0f;
    propic.layer.cornerRadius=propic.frame.size.height/4;
    //propic.clipsToBounds=YES;
    propic.layer.masksToBounds=YES;
    // Do any additional setup after loading the view.
    
    [self setupView];
}



-(void)setupView
{

    UILabel *name=(UILabel *)[self.view viewWithTag:50];
    UILabel *birthday=(UILabel *)[self.view viewWithTag:51];
    UILabel *address=(UILabel *)[self.view viewWithTag:52];
    UILabel *zip=(UILabel *)[self.view viewWithTag:53];
    UILabel *city=(UILabel *)[self.view viewWithTag:54];
    UILabel *passport=(UILabel *)[self.view viewWithTag:55];


    UserObject *u=[UserObject sharedInstance];
    
    if ([u IsUserLoggedIn])
    {
        [name setText:[u userObjectForKey:UserName]];
        [birthday setText:[u userObjectForKey:UserDOB]];
        [address setText:[u userObjectForKey:UserLocation]];
        [city setText:[u userObjectForKey:UserCity]];
        [propic setImageWithURL:[NSURL URLWithString:[u userObjectForKey:UserphotoURL]]];
        [zip setText:[u userObjectForKey:UserZip]];
        [passport setText:[u userObjectForKey:UserPassport]];
    }
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)signOut:(id)sender
{
    [[FBSession activeSession]closeAndClearTokenInformation];
    [[UserObject sharedInstance]clearUserPreference];
    [[UserObject sharedInstance]setUserLoggedIn:NO];
    NSLog(@"user Logged out");
    [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];
	[topController.RSdelegate searchDataDidUpdate];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)editBtn:(id)sender
{
    
    
    
}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
