//
//  MasterDataModel.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "MasterDataModel.h"

@implementation MasterDataModel



+(MasterDataModel *)shared_instance
{

    static  MasterDataModel *my_model = nil;
    static  dispatch_once_t once_token;
    
    dispatch_once(&once_token, ^{
        
        my_model =[[MasterDataModel alloc]init];
        
        
    });

    return my_model;

}

-(NSString *)getTourTypeID:(NSString *)value
{
    if ([value isEqualToString:@"All"])
        return @"All";

    int i = 0;
    for (NSString *v in self.types)
    {
        i++;
        if ([value isEqualToString:v])
        {
            break;
        }
    }
    
    return [NSString stringWithFormat:@"%d",i];

}

@end
