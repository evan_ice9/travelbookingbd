//
//  singleImageViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 5/31/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "singleImageViewController.h"

@implementation singleImageViewController



-(void)viewDidLoad
{

    NSURL *url = [NSURL URLWithString:self.imgUrl];
    [self.imageView setContentMode:UIViewContentModeScaleToFill];
    [self.imageView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"placeholder findHotels"]];
}

@end
