//
//  SignInViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "SignInViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>

@interface SignInViewController ()

@end

@implementation SignInViewController
{
    GPPSignIn *GSignIn;
    MBProgressHUD *hud;
}

@synthesize user,password,user_error_icon,username_error_msg,password_error_icon,password_error_msg,topViewController;

- (void)viewDidLoad {
    [super viewDidLoad];
    user.delegate = self;
    password.delegate = self;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (IBAction)signUp:(id)sender
{
    SignUpViewController *signUp = (SignUpViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signUp"];
    [self presentViewController:signUp animated:YES completion:nil];
}

- (IBAction)signIn:(id)sender
{
    
    
    if (![self.user.text isEqualToString:@""] && ![self.password.text isEqualToString:@""])
    {
        hud = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:hud];
        [hud show:YES];
        
        NSDictionary *dict =@{@"email":self.user.text,@"password":self.password.text};
        [[Network shared_instance]loginwithParameters:dict completionBlock:^(int status, id response) {
            
            switch (status) {
                case 0:
                    NSLog(@"%@",response);
                    [self loginSuccess:response];
                    break;
                case 1:
                   NSLog(@"%@",response);
                    [self loginFailed];                     
                    break;
                    
                default:
                    break;
            }
            
        }];
        
    }
    else
    {
    
        if ([self.user.text isEqualToString:@""])
        {
            username_error_msg.hidden = NO;
            user_error_icon.hidden = NO;            
        }
        else
        {
            password_error_msg.hidden = NO;
            password_error_icon.hidden = NO;
        }
    
    }
    
    

    
}

- (IBAction)signInGplus:(id)sender
{
    
    GSignIn =[GPPSignIn sharedInstance];
    
    
    if ([GSignIn authentication])
    {
        [GSignIn signOut];
        NSLog(@"logging out");
    }
    else
    {
        GSignIn.shouldFetchGooglePlusUser = YES;
        GSignIn.shouldFetchGoogleUserEmail=YES;
        GSignIn.clientID=GPlus_ClientID;
        GSignIn.scopes = @[@"profile"];
        GSignIn.delegate = self;
        [GSignIn authenticate];
    }
}

- (IBAction)signInFb:(id)sender
{
    
    NSLog(@"%u",[[FBSession activeSession] state]);
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended)
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    else
    {
        
        NSArray *params=@[@"public_profile",@"email",@"user_birthday",@"user_location"];
        
        hud = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:hud];
        [hud show:YES];
        
        [FBSession openActiveSessionWithReadPermissions:params allowLoginUI:YES completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
         {
             
             [hud show:NO];
             [hud removeFromSuperview];
             
             AppDelegate *appdelegate=[UIApplication sharedApplication].delegate;
             [appdelegate sessionStateChanged:session state:status error:error];
             [self dismissViewControllerAnimated:YES completion:nil];
             
         }];
        
    }

}

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)loginSuccess:(NSDictionary *)info
{

    [hud show:NO];
    [hud removeFromSuperview];
    
    NSLog(@"user Logged in");
    info = [info objectForKey:@"user"];
    [[UserObject sharedInstance]setUserLoggedIn:YES];
    [[UserObject sharedInstance]SetAccountType:emailAccount];
    
    
    NSString *fullName=[NSString stringWithFormat:@"%@.%@ %@",info[@"title"],info[@"firstName"],info[@"lastName"]];
    NSString *userID=[NSString stringWithFormat:@"%@",info[@"id"]];
    NSString *email=[NSString stringWithFormat:@"%@",info[@"email"]];
    NSString *birthday =[NSString stringWithFormat:@"%@",info[@"dob"]];
    NSString *location =[NSString stringWithFormat:@"%@",info[@"address"]];
    NSString *photoURL = info[@"image"];
    NSString *zip = info[@"zip"];
    NSString *nationality = info[@"nationality"];
    NSString *passport_no = info[@"passport"];
    NSString *city_name = info[@"city"][@"name"];
    
    NSLog(@"%@ %@ %@ %@ %@",fullName,userID,email,birthday,location);
    
    UserObject *u=[UserObject sharedInstance];
    [u setEmail:email];
    [u setUserName:fullName];
    [u setBirthday:birthday];
    [u setLocation:location];
    [u setProfileURL:photoURL];
    [u setUserID:userID];
    [u setZip:zip];
    [u setNationality:nationality];
    [u setPassport:passport_no];
    [u setCity:city_name];

    [topViewController.RSdelegate searchDataDidUpdate];
    [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];

    [self dismissViewControllerAnimated:YES completion:^{
        
        NSDictionary *temp_dict=[[NSDictionary alloc]initWithObjectsAndKeys:@YES,@"doAdjust",nil];
        [[NSNotificationCenter defaultCenter]postNotificationName:SideMenu_Action object:nil userInfo:temp_dict];
    }];

}


-(void)loginFailed
{
    [hud show:NO];
    [hud removeFromSuperview];
    [self.view makeToast:@"incorrect username or password"];
    
}


#pragma mark Keyboard Delegates

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;

}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *str = textField.text;
    str = [str stringByReplacingCharactersInRange:range withString:string];
    
    if ([str length]>0)
    {
        if (textField == user)
        {
            user_error_icon.hidden = YES;
            username_error_msg.hidden = YES;
        }
        else if (textField == password)
        {
            password_error_icon.hidden = YES;
            password_error_msg.hidden = YES;
        }
    }

    return YES;
}

#pragma mark Gplus Delegates


-(void)finishedWithAuth:(GTMOAuth2Authentication *)auth error:(NSError *)error
{

    if(!error)
    {
        NSLog(@"%@",GSignIn.authentication.userEmail);
    }

}
@end
