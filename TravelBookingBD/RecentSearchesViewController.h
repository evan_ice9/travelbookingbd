//
//  RecentSearches.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/5/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SignInViewController.h"
#import "SearchViewController.h"
#import <PDTSimpleCalendar.h>
#import <MBProgressHUD.h>

@interface RecentSearchesViewController : UITableViewController<RecentSearchesDelegate,UIPickerViewDelegate,UIPickerViewDataSource,PDTSimpleCalendarViewDelegate,PDTSimpleCalendarViewCellDelegate>


-(void)reloadRecentSearchData;

@end
