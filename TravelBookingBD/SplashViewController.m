//
//  ViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/20/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "SplashViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    
    [self checkFBlogin];
    [self loadEssentialInfo];


    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated
{

    [[UIApplication sharedApplication]setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];

}


-(void)loadEssentialInfo
{
    [[Network shared_instance]getFeaturedHotelsandToursWithCompletion:^(int status, id response)
     {
         
         switch (status)
         {
             case 0:
             {
                 [self setFeatearedHotelandTour:response];
                 
                 [[Network shared_instance]getCurrenciesWithCompletionBlock:^(int status, id response)
                  {
                      switch (status) {
                          case 0:
                          {
                              NSLog(@"%@",response);
                              [self setCurrencies:response];
                              
                              [[Network shared_instance]getTourFilterOptionsWithCompletionBlock:^(int status, id response)
                              {
                              
                                  [self setFilterOptions:response];
                                  
                                  InitSlidingViewController *iv=(InitSlidingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"initSlidingView"];
                                  [self presentViewController:iv animated:YES completion:nil];
                                  
                              }];
                              
                          }
                              break;
                              
                          default:
                              break;
                      }
                  }];
             }
                 break;
             case 1:
             {
                 [self.view makeToast:@"Network Error" duration:1.0 position:CSToastPositionBottom];
                 
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW,3* NSEC_PER_SEC),dispatch_get_main_queue(),^{
                     
                     
                     InitSlidingViewController *iv=(InitSlidingViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"initSlidingView"];
                     [self presentViewController:iv animated:YES completion:nil];
                 });
                 
             }
                 break;
         }
         
     }];




}

-(void)checkFBlogin
{

    
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
    {
        
        NSArray *permission=@[@"public_profile",@"email",@"user_birthday",@"user_location"];
        [FBSession openActiveSessionWithReadPermissions:permission allowLoginUI:NO completionHandler:^(FBSession *session, FBSessionState status, NSError *error)
        {
         
            AppDelegate *appdelegate=[UIApplication sharedApplication].delegate;
            
            [appdelegate sessionStateChanged:session state:status error:error];
            
        }];
        
        
    }


}

-(void)setFeatearedHotelandTour:(id)response
{


    NSArray *hotels =[response objectForKey:ObjHotels];
    NSArray *tours = [response objectForKey:ObjTours];
    
    
    NSMutableArray *FeaturedHotels=[[NSMutableArray alloc]init];
    NSMutableArray *FeaturedTours=[[NSMutableArray alloc]init];
    
    for (NSDictionary *obj in hotels)
    {
        hotel *h=[[hotel alloc]initWithData:obj];
        [FeaturedHotels addObject:h];
    }
    
    for (NSDictionary *obj in tours)
    {
        tour *t=[[tour alloc]initWithData:obj];
        [FeaturedTours addObject:t];
    }

    [MasterDataModel shared_instance].featuredHotels = FeaturedHotels;
    [MasterDataModel shared_instance].featuredTours = FeaturedTours;

}


-(void)setCurrencies:(id)response
{
 
    NSArray *arr = [response objectForKey:@"currencies"];
    
    [MasterDataModel shared_instance].currencies = arr;
    
    
}

-(void)setFilterOptions:(id)response
{
    
    NSLog(@"%@",response);
    
    NSMutableArray *temp_arr = [[NSMutableArray alloc]initWithArray:response[@"categories"]];
    [temp_arr addObject:@"All"];
    [MasterDataModel shared_instance].categories = temp_arr;
    
    temp_arr = [[NSMutableArray alloc]initWithArray:response[@"packages"]];
    [temp_arr addObject:@"All"];
    [MasterDataModel shared_instance].packages = temp_arr;
    
    NSMutableArray *arr = [[NSMutableArray alloc]init];
    
    for (NSDictionary *d in response[@"types"])
    {
        [arr addObject:d[@"name"]];
    }
    [arr addObject:@"All"];
    [MasterDataModel shared_instance].types = arr;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
