//
//  SignInViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import <UIView+Toast.h>
#import <MBProgressHUD.h>
#import "AppDelegate.h"
#import "SignUpViewController.h"
#import <MBProgressHUD.h>
#import "SearchViewController.h"

@interface SignInViewController : UIViewController<GPPSignInDelegate,UITextFieldDelegate>

- (IBAction)signUp:(id)sender;

- (IBAction)signIn:(id)sender;

- (IBAction)signInGplus:(id)sender;

- (IBAction)signInFb:(id)sender;

- (IBAction)back:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *user;

@property (strong, nonatomic) IBOutlet UITextField *password;

@property (strong, nonatomic) IBOutlet UIImageView *user_error_icon;

@property (strong, nonatomic) IBOutlet UIImageView *password_error_icon;

@property (strong, nonatomic) IBOutlet UILabel *username_error_msg;

@property (strong, nonatomic) IBOutlet UILabel *password_error_msg;

@property (weak,nonatomic) SearchViewController *topViewController;

@end
