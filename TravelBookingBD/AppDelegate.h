//
//  AppDelegate.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/20/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import <GooglePlus/GooglePlus.h>
#import <GoogleMaps/GoogleMaps.h>
#import "RecentSearchesViewController.h"
#import "InitSlidingViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error;

@end

