//
//  tour.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "tour.h"

@implementation tour



-(id)initWithData:(NSDictionary *)data
{

    self = [super init];
    
    if (self)
    {
        
        tourID = data[@"id"];
        title = data[@"title"];
        category = data[@"category"];
        startDate = data[@"startDate"];
        expireDate = data[@"expireDate"];
        startPlace = data[@"startPlace"];
        endPlace = data[@"endPlace"];
        destinations = data[@"destinationsCovered"];
        type = data[@"type"];
        stars = [data[@"stars"] intValue];
        nights = [data[@"nights"] intValue];
        days = [data[@"days"] intValue];
        basicPrice = [data[@"basicPrice"] isEqual:[NSNull null]]?0:[data[@"basicPrice"] floatValue];
        basicPriceAdult = [data[@"basicPriceAdult"] isEqual:[NSNull null]]?0:[data[@"basicPriceAdult"] floatValue];
        basicPriceChild = [data[@"basicPriceChild"] isEqual:[NSNull null]]?0:[data[@"basicPriceChild"] floatValue];
        childwithBed = [data[@"childWithBed"] isEqual:[NSNull null]]?0:[data[@"childWithBed"] floatValue];
        childwithoutBed =[data[@"childWithOutBed"] isEqual:[NSNull null]]?0:[data[@"childWithOutBed"] floatValue];
        currency = data[@"currency"];
        priceType = data[@"priceType"];
        vatTax = [data[@"vatTaxAmount"]isEqual:[NSNull null]]?0:[data[@"vatTaxAmount"]intValue];
        vatTaxType =[data[@"vatTaxType"]isEqual:[NSNull null]]?nil:data[@"vatTaxType"];
        cuttoffPeriod = [data[@"cuttoffPeriod"]isEqual:[NSNull null]]?0:[data[@"cuttoffPeriod"] intValue];
        images =data[@"images"];
        itinerary = data[@"itinerary"]?data[@"itinerary"]:nil;
        features = data[@"features"]?data[@"features"]:nil;
        description = data[@"description"]?data[@"description"]:nil;
        policies = data[@"policies"]?data[@"policies"]:nil;
        hotels = data[@"hotels"]?data[@"hotels"]:nil;
    }


    return self;

}

-(NSString *)getID
{
    return tourID;
}


-(NSString *)getTitle
{
    return title;
}

-(NSString *)getCategory
{
    return category;
}

-(NSString *)getStartDate
{
    return startDate;
}

-(NSString *)getExpiretDate
{
    return expireDate;
}

-(NSDictionary *)getStartPlace
{
    return startPlace;
}


-(NSDictionary *)getEndPlace
{
    return endPlace;
}

-(NSString *)getType
{
    return type;
}

-(NSArray *)Destinations
{

    return destinations;
}

-(int)Stars
{
    return stars;
}

-(int)nights
{

    return  nights;

}

-(int)days
{
    
    return  days;
    
}

-(float)getPrice
{

    if ([priceType isEqualToString:@"Group"])
    {
        return basicPrice;
    }
    else
    {
        return basicPriceAdult;
    }
}


-(NSString *)currency
{
    
    return currency;
}


-(NSString *)priceType
{
    
    return priceType;
}

-(int)vatTaxAmount
{
    
    return vatTax;
}

-(NSString *)vatTaxType
{

    return vatTaxType;
}

-(int)cuttoffPeriod
{

    return cuttoffPeriod;
}

-(NSArray *)images
{

    return images;
    
}



-(NSString *)getCurrencySymBol
{
    NSString *symbol =@"$";
    NSArray *arr = [MasterDataModel shared_instance].currencies;
    
    for (NSDictionary *d in arr)
    {
        if ([d[@"currencyCode"] isEqualToString:currency])
        {
            symbol = d[@"currencySymbol"];
            break;
        }
    }
    
    return symbol;

}

-(NSArray *)getItinerary
{

    return itinerary;
}

-(NSMutableArray *)getInclusions
{

    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    
    if (features.count>0)
    {
        for (NSDictionary *d in features)
        {
            if ([d[@"type"] isEqualToString:@"inclusion"])
            {
                [arr addObject:d];
            }
        }
    }
    

    return arr;

}


-(NSMutableArray *)getExclusions
{
    
    NSMutableArray *arr=[[NSMutableArray alloc]init];
    
    
    if (features.count>0)
    {
        for (NSDictionary *d in features)
        {
            if ([d[@"type"] isEqualToString:@"exclusion"])
            {
                [arr addObject:d];
            }
        }
    }
    
    
    return arr;
    
}

-(NSString *)getDescription
{
    return description;
}


-(NSString *)getPrivacyStatement
{
    return [policies count]>0?policies[@"privacy"]:nil;
}

-(NSString *)getRefundStatement
{
    return [policies count]>0?policies[@"refund"]:nil;
}

-(NSString *)getTerms
{
    return [policies count]>0?policies[@"terms"]:nil;
}


-(NSArray *)getPayments
{

    NSMutableArray *m = [[NSMutableArray alloc]init];
    if ([policies count]>0)
    {
        NSArray *temp = [policies objectForKey:@"payment"];
        
        for (NSDictionary *d in temp)
        {
            [m addObject:d[@"name"]];
        }
    }
    
    return m;

}


-(float)getBasicAdultPrice
{
    return basicPriceAdult;
}

-(float)getBasicChildPrice
{
    if ([priceType isEqualToString:@"Individual"])
    {
        return basicPriceChild;
    }
    else
    {
        return childwithBed;
    }
    
}

-(float)getChildAlternatePrice
{
    return childwithoutBed;
}

-(NSArray *)coveredHotels
{
    return hotels;
}


@end
