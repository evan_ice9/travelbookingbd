//
//  TourOverViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "TourOverViewController.h"

@interface TourOverViewController ()
{

    TourDetailViewController        *tc1;
    TourAccommodationViewController *tc2;
    MGSwipeTabBarController *tab;
    imageScrollerViewController *isvc;
    tour *mainData;

}

@end


@implementation TourOverViewController


@synthesize tabView,header;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tc1 = (TourDetailViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"tourDetail"];
    tc1.delegate = self;
    tc2 = (TourAccommodationViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"tourAccommodation"];
    tc2.delegate = self;
    tc2.tourDelegate = self;
    tab =[[MGSwipeTabBarController alloc]initWithViewControllers:@[tc1,tc2]];
    [tabView addSubview:tab.view];
    
    
    for (int i=300; i<302; i++)
    {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        [btn addTarget:self action:@selector(handleButton:) forControlEvents:UIControlEventTouchUpInside];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#8bc34a"]] forState:UIControlStateSelected];
        [btn setBackgroundImage:[Utils imageWithColor:[Utils colorFromHex:@"#8bc34a"]] forState:UIControlStateSelected|UIControlStateHighlighted];
        [btn setTitleColor:[Utils colorFromHex:@"#999999"] forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        
        
        if (i == 300)
        {
            btn.selected =YES;
        }
        
    }
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:CGRectMake(header.frame.origin.x+2, header.frame.origin.y, header.frame.size.width-2, header.frame.size.height)];
    header.layer.masksToBounds = YES;
    header.layer.shadowColor = [UIColor blackColor].CGColor;
    header.layer.shadowOffset = CGSizeMake(0.0f, 3.0f);
    header.layer.shadowOpacity = 0.3f;
    header.layer.shadowPath = shadowPath.CGPath;
    header.clipsToBounds = NO;
    

    
    // Do any additional setup after loading the view.
}


-(void)viewDidAppear:(BOOL)animated
{

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(adjustTabBtn) name:@"tabview did scroll" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(toTourAccomodations) name:ToTourAccomodations object:nil];
    
}



-(void)fetchTourDetails:(tour *)t
{

    NSDictionary *dict = @{@"tourId":[t getID]};
    
    [[Network shared_instance]getTour:dict completionBlock:^(int status, id response)
     {
         switch (status) {
             case 0:
             {
                 NSLog(@"%@",response);
                 tour *t = [[tour alloc]initWithData:response];
                 [tc1 setup:t];
                 [tc2 setup:t];
             }
                 break;
            case 1:
                 NSLog(@"failed");
                 break;
         }
        
    }];

}

-(void)setData:(tour *)t
{
 
    mainData = t;
    [tc1 setAttributes:t];
    [tc2 setAttributes:t];
    
    [self fetchTourDetails:t];
}



-(IBAction)handleButton:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    if (btn.selected)
    {
        return;
    }
    
    
    btn.selected=YES;
    [tab setSelectedIndex:btn.tag%300 animated:YES];
    
    for (int i=300; i<302; i++)
    {
        if (i!=btn.tag)
        {
            UIButton *temp=(UIButton *)[self.view viewWithTag:i];
            temp.selected=NO;
        }
    }

}


-(void)adjustTabBtn
{
    
    
    int index=[tab selectedIndex];
    
    UIButton *btn=(UIButton *)[self.view viewWithTag:index+300];
    btn.selected=YES;
    
    for (int i=300; i<302; i++)
    {
        if (i!=btn.tag)
        {
            UIButton *temp=(UIButton *)[self.view viewWithTag:i];
            temp.selected=NO;
        }
    }
    
    
    
    
}

-(void)toTourAccomodations
{
    [tab setSelectedIndex:2 animated:YES];
    [self adjustTabBtn];
}


-(void)isModalShow:(BOOL)value
{
    [tab enablePanGesture:value];
}

-(void)isShowToast:(NSString *)msg
{
    [self.view makeToast:msg];
}


-(void)didClickImage:(BOOL)value index:(int)index
{

    if (value)
    {
        [tab enablePanGesture:NO];
        NSLog(@"clicked");
        isvc = [self.storyboard instantiateViewControllerWithIdentifier:@"image_scroller"];
        isvc.tourdelegate = self;
        isvc.imageArray = [mainData images];
        isvc.t =[mainData getTitle];
        isvc.selectedIndex = index;
        [self addChildViewController:isvc];
        [tabView addSubview:isvc.view];
        [isvc didMoveToParentViewController:self];
    }
    else
    {
        [tab enablePanGesture:YES];
        [isvc didMoveToParentViewController:nil];
        [isvc removeFromParentViewController];
        [isvc.view removeFromSuperview];
    
    }
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)back:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
