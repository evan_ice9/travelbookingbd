//
//  HotelRoomTypesViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/2/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ASStarRatingView.h>
#import "hotel.h"
#import "SignInViewController.h"
#import "HotelDetailViewController.h"

@interface HotelRoomTypesViewController : UIViewController
{
    int view_count;
    NSMutableArray *expandData;
    NSMutableArray *bookingData;
}

@property (strong, nonatomic) IBOutlet UIView *nameHolder;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *discount_view;


@property (strong, nonatomic) IBOutlet UIImageView *img1;
@property (strong, nonatomic) IBOutlet UIImageView *img2;
@property (strong, nonatomic) IBOutlet UIImageView *img3;

@property (strong, nonatomic) IBOutlet UILabel *Hoteltitle;

@property (strong, nonatomic) IBOutlet UILabel *cityName;

@property (strong, nonatomic) IBOutlet ASStarRatingView *ratingView;


@property (strong, nonatomic) IBOutlet UILabel *price;

@property (strong, nonatomic) IBOutlet UILabel *currency;

@property (strong, nonatomic) IBOutlet UIView *container;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *containerHeight;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property NSDictionary *parameters;

@property id<hotelDetailDelegate> delegate;
-(void)setRoomData:(NSArray *)rooms;
-(void)setAttributes:(hotel *)mainData;

-(void)setBookingData:(NSDictionary *)data;

@end
