//
//  hotel.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "hotel.h"

@implementation hotel



-(id)initWithData:(NSDictionary *)data
{
    self = [super init];
    
    if (self)
    {
        hotelID=data[@"id"];
        code=data[@"code"];
        name =data[@"name"]?data[@"name"]:data[@"hotelName"];
        rating = [data[@"rating"] intValue];
        lat = [data[@"lat"] floatValue];
        lon = [data[@"lon"] floatValue];
        location = data[@"location"];
        cityCode = data[@"city"][@"cityCode"];
        cityName = data[@"city"][@"name"];
        lowestPrice = data[@"lowestPrice"]?[data[@"lowestPrice"] floatValue]:[data[@"amount"] floatValue];
        lowestPriceCurrency = data[@"lowestPriceCurrency"]?data[@"lowestPriceCurrency"]:data[@"currency"];
        discount = data[@"discount"]?[data[@"discount"] intValue]:0;
        discountCurrency = data[@"discountCurrency"]?data[@"discountCurrency"]:nil;
        images = data[@"images"];
        hotelDescription = [data objectForKey:@"description"]?data[@"description"]:nil;
        facilities = [data objectForKey:@"facilities"]?data[@"facilities"]:nil;
        searchID = [data objectForKey:@"searchId"]?data[@"searchId"]:nil;
        source = [data objectForKey:@"source"]?[data objectForKey:@"source"]:nil;
        cancellationPolicy = data[@"cancellationPolicy"]?data[@"cancellationPolicy"]:nil;
        bookingID = data[@"bookingId"]?data[@"bookingId"]:nil;
        featured = [data[@"featured"] boolValue];
    }
    
    return self;
    
}



-(NSString *)getID
{
    return hotelID;
}

-(NSString *)getCode
{
    return code;
}

-(NSString *)getName
{
    
    return name;
    
}

-(int)getRating
{
    return rating;
}

-(float)getLatitude
{
    return lat;
}

-(float)getLongitude
{
    return lon;
}

-(NSString *)getLocation
{

    return location;
}

-(NSString *)getCityCode
{
    return cityCode;
}

-(NSString *)getCityName
{
    return cityName;
}

-(float)getLowestPrice
{
    return lowestPrice;
}

-(NSString *)getLowestPriceCurrency
{
    return lowestPriceCurrency;
}

-(int)getDiscount
{

    return discount;
}

-(NSString *)getDiscountCurrency
{
    return discountCurrency;
}

-(NSArray *)images
{
    return images;

}


-(NSString *)getCurrencySymBol
{
    
    NSString *symbol =@"$";
    NSArray *arr = [MasterDataModel shared_instance].currencies;
    
    for (NSDictionary *d in arr)
    {
        if ([d[@"currencyCode"] isEqualToString:lowestPriceCurrency])
        {
            symbol = d[@"currencySymbol"];
            break;
        }
    }
    
    return symbol;
}


-(NSString *)getDescription
{

    return hotelDescription;
}


-(NSArray *)getFacilities
{
    return facilities;
}

-(NSString *)getSearchID
{
    return searchID;
}

-(NSString *)getSource
{
    return source;
}

-(NSArray *)cancellationPolicyArray
{
    return cancellationPolicy;
}

-(NSString *)getBookingID
{
    return bookingID;
}

-(BOOL)isFeatured
{
    return featured;
}


@end
