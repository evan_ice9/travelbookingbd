//
//  imageScrollerViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 5/31/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <MGSwipeTabBarController.h>
#import "imageScrollerViewController.h"
#import "singleImageViewController.h"

@implementation imageScrollerViewController
{
    MGSwipeTabBarController *tab;
}

@synthesize imageArray,titleText,paginationText;

-(void)viewDidLoad
{

    /*imageArray =@[@{@"src":@"https://scontent-sin1-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/11150495_799093250204247_7324068955397354970_n.jpg?oh=850e890a604e530ea1e5e29928d1f0f0&oe=55EA60E7"},@{@"src":@"https://scontent-sin1-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/11150495_799093250204247_7324068955397354970_n.jpg?oh=850e890a604e530ea1e5e29928d1f0f0&oe=55EA60E7"},@{@"src":@"https://scontent-sin1-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/11150495_799093250204247_7324068955397354970_n.jpg?oh=850e890a604e530ea1e5e29928d1f0f0&oe=55EA60E7"}];*/
    
    [titleText setText:self.t];
    [self.view setBackgroundColor:[UIColor colorWithWhite:0 alpha:.4]];
    
    NSMutableArray *viewControllers = [[NSMutableArray alloc]init];
    for (id imageObject in imageArray)
    {
        singleImageViewController *single = [self.storyboard instantiateViewControllerWithIdentifier:@"singleImage"];
        single.imgUrl = imageObject[@"src"];
        [viewControllers addObject:single];
    }
    
     tab = [[MGSwipeTabBarController alloc]initWithViewControllers:viewControllers];
    [self.holder addSubview:tab.view];
    [tab setSelectedIndex:self.selectedIndex animated:NO];
    
    int index = tab.selectedIndex+1;
    int totalPages = imageArray.count;
    [paginationText setText:[NSString stringWithFormat:@"%d/%d",index,totalPages]];
    
}

-(void)viewDidAppear:(BOOL)animated
{

    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updatePagination) name:@"tabview did scroll" object:nil];

}

-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (IBAction)btnClicked:(id)sender
{
    [self.hoteldelegate didOpenImageSlider:NO index:0];
    [self.tourdelegate didClickImage:NO index:0];
}


-(void)updatePagination
{
    
    int index = tab.selectedIndex+1;
    int totalPages = imageArray.count;
    [paginationText setText:[NSString stringWithFormat:@"%d/%d",index,totalPages]];
}
@end
