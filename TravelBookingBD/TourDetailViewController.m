//
//  TourDetailViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "TourDetailViewController.h"

@interface TourDetailViewController ()

@end

@implementation TourDetailViewController
{
    tour *mainData;
}


@synthesize scrollView,container;
@synthesize tourTitle,tourType,priceTag,currencySymbol,tag1,tag2;
@synthesize img1,img2,img3;
@synthesize duration,startPlace,endPlace,destinations,offerStart,offerExpires,spinner,bookingBtn;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    scrollView.delaysContentTouches = NO;
}



-(void)setAttributes:(tour *)t
{
    mainData = t;
    [tourTitle setText:[t getTitle]];
    [tourType setText:[t getType]];
    [priceTag setText:[NSString stringWithFormat:@"%.2f",[t getPrice]]];
    [currencySymbol setText:[t getCurrencySymBol]];
    
    if ([mainData images].count>0)
    {
        [img1 setImageWithURL:[NSURL URLWithString:[mainData images][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
        
        if ([mainData images].count>1)
        {
            
            [img2 setImageWithURL:[NSURL URLWithString:[mainData images][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            
            if ([mainData images].count>2)
            {
                [img3 setImageWithURL:[NSURL URLWithString:[mainData images][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            }
        }
        
    }
    
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *start = [formatter dateFromString:[t getStartDate]];
    NSDate *end = [formatter dateFromString:[t getExpiretDate]];
    
    
    
    
    [formatter setDateFormat:@"dd MMMM"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *a;
    NSDate *b;
    
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&a
                 interval:NULL forDate:start];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&b
                 interval:NULL forDate:end];
    
    [offerStart setText:[formatter stringFromDate:start]];
    [offerExpires setText:[formatter stringFromDate:end]];
    [duration setText:[NSString stringWithFormat:@"%d days,%d nights",[mainData days],[mainData nights]]];
    [startPlace setText:[t getStartPlace][@"name"]];
    [endPlace setText:[t getEndPlace][@"name"]];
    
    NSString *str = @"";
    
    if ([t Destinations].count>0)
    {
        for (NSDictionary *d in [t Destinations])
        {
            str = [str stringByAppendingString:[NSString stringWithFormat:@"%@,",[d objectForKey:@"name"]]];
        }
        str = [str substringToIndex:str.length-1];
    }
    
    [destinations setText:str];
    
    UILabel *l1 =(UILabel *)[tag1 viewWithTag:55];
    UILabel *l2 =(UILabel *)[tag2 viewWithTag:55];
    
    [l1 setText:[t getCategory]];
    [l2 setText:[t priceType]];
    tag1.layer.cornerRadius = 5.0;
    tag2.layer.cornerRadius = 5.0;
    
}


-(void)setup:(tour *)t
{

    img1.userInteractionEnabled = YES;
    img1.tag = 900;
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [img1 addGestureRecognizer:tap1];
    
    img2.tag = 901;
    img2.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [img2 addGestureRecognizer:tap2];
    
    img3.tag = 902;
    img3.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    [img3 addGestureRecognizer:tap3];
    
    mainData = t;
    scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [spinner removeFromSuperview];

    int marginY = 8;
    int startY = 567;
    int startX = 15;
    startY += marginY;
    
    
    //set description text
    UILabel *descriptionText = [[UILabel alloc]init];
    descriptionText.lineBreakMode = NSLineBreakByWordWrapping;
    descriptionText.numberOfLines = 0;
    NSString *placerText = [t getDescription];
    [descriptionText setFont:[UIFont systemFontOfSize:10]];
    CGSize s = [placerText sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(290, 600) lineBreakMode:NSLineBreakByWordWrapping];
    [descriptionText setFrame:CGRectMake(startX, 25, 290, s.height+20)];
    [descriptionText setText:placerText];
    [descriptionText setTextColor:[Utils colorFromHex:@"#999999"]];
    
    //setup description view
    UIView *descriptionView = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, s.height + 50)];
    [descriptionView addSubview:descriptionText];
    [descriptionView setBackgroundColor:[UIColor whiteColor]];
    
    //set description icon
    UIImageView *iconview = [[UIImageView alloc]initWithFrame:CGRectMake(startX, 10, 20, 20)];
    [iconview setImage:[UIImage imageNamed:@"icon_description"]];
    [iconview setContentMode:UIViewContentModeCenter];
    [descriptionView addSubview:iconview];
    
    //set description label
    UILabel *viewTitle = [[UILabel alloc]initWithFrame:CGRectMake(startX + 25 , 10 , 100, 20)];
    [viewTitle setText:@"Description"];
    [viewTitle setFont:[UIFont fontWithName:@"Helvetica" size:18]];
    [viewTitle setTextColor:[Utils colorFromHex:@"#f57c00"]];
    [descriptionView addSubview:viewTitle];
    
    
    [container addSubview:descriptionView];
    
    //hotels covered view
    
    startY += descriptionView.frame.size.height + marginY;
    
    UIView *v = [[UIView alloc]initWithFrame:CGRectMake(0, startY, 320, 200)];
    [v setBackgroundColor:[UIColor whiteColor]];
    UIImageView *icon =[[UIImageView alloc]initWithFrame:CGRectMake(15, 10, 20, 20)];
    [icon setImage:[UIImage imageNamed:@"icon_hotel"]];
    [icon setContentMode:UIViewContentModeCenter];
    UILabel *label =[[UILabel alloc]initWithFrame:CGRectMake(45, 10, 200, 20)];
    [label setTextColor:[UIColor orangeColor]];
    [label setText:@"Hotels Covered"];
    [label setFont:[UIFont systemFontOfSize:14]];
    [v addSubview:icon];
    [v addSubview:label];
    
    
    int total = [mainData coveredHotels].count;
    int rows = total/3;
    rows = total%3? rows+1:rows;
    
    int X=15;
    int Y=40;
    
    for (int i=0; i<rows; i++)
    {
        for (int j=0; j<3; j++)
        {
            if (i*3 + j == total)
            {
                break;
            }
            
            UIView *demo =[self getHotelImageView:[mainData coveredHotels][i][@"imageSrc"] Nights:[[mainData coveredHotels][i][@"nights"] intValue] Title:[mainData coveredHotels][i][@"name"] startX:X + (89+10)*j startY:Y + i*(85+5)];
            [v addSubview:demo];
            
            
        }
    }
    
    int total_height = Y + rows*(85+5)+20;
    
    [v setFrame:CGRectMake(0, startY, 320, total_height)];
    
    [container addSubview:v];
    
    
    
    startY +=total_height + marginY;
    
    //itinerary view
    
    int iternary_start =startY;
    
    UIView *itinerary =[[UIView alloc]init];
    [itinerary setBackgroundColor:[Utils colorFromHex:@"#6d63b0"]];
    
    int label_height = 20;
    int textview_height = 0;
    
    
    UIImageView *itineraryIconview = [[UIImageView alloc]initWithFrame:CGRectMake(startX, 10, 20, 20)];
    [itineraryIconview setImage:[UIImage imageNamed:@"icon_iternary"]];
    [itineraryIconview setContentMode:UIViewContentModeCenter];
    [itinerary addSubview:itineraryIconview];
    
    UILabel *itineraryViewTitle = [[UILabel alloc]initWithFrame:CGRectMake(startX + 25 , 10 , 100, label_height)];
    [itineraryViewTitle setText:@"Itinerary"];
    [itineraryViewTitle setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [itineraryViewTitle setTextColor:[UIColor whiteColor]];
    [itinerary addSubview:itineraryViewTitle];
    
    
    int count = [t getItinerary].count;
    
    startY = 20+label_height;
    startX = 15;
    
    total_height = 0;
    
    X = startX + 5;
    Y = startY;
    
    for (int i = 0; i<count; i++)
    {
        NSString *descriptionText = [t getItinerary][i][@"description"];
        CGSize s = [descriptionText sizeWithFont:[UIFont fontWithName:@"Helvetica" size:10] constrainedToSize:CGSizeMake(280, 600) lineBreakMode:NSLineBreakByWordWrapping];
        textview_height = s.height;
        
        UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(X,Y, 280, label_height)];
        [l setText:[t getItinerary][i][@"title"]];
        [l setFont:[UIFont fontWithName:@"Helvetica-Bold" size:12]];
        [l setTextColor:[UIColor whiteColor]];
        
        Y +=label_height+5 ;
        
        UILabel *text = [[UILabel alloc]initWithFrame:CGRectMake(X,Y, 280, textview_height)];
        [text setBackgroundColor:[UIColor clearColor]];
        [text setText:[t getItinerary][i][@"description"]];
        [text setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [text setTextColor:[UIColor whiteColor]];
        text.numberOfLines = 0;
        text.lineBreakMode = NSLineBreakByWordWrapping;
        
        Y += textview_height + 5 ;
        
        [itinerary addSubview:l];
        [itinerary addSubview:text];
        
        total_height += label_height + textview_height +10;
    }
    
    total_height +=40;

    
    [itinerary setFrame:CGRectMake(0, iternary_start, 320, total_height)];
    
    
    [container addSubview:itinerary];
    
    
    
    
    
    
    //Inclusion view
    int inclusion_startY = iternary_start + total_height + marginY;
    
    X = 15;
    Y = 10;
    int inclusions_count = [mainData getInclusions].count;
    int temp = inclusions_count%2==0?inclusions_count/2:(inclusions_count/2)+1;
    int inclusion_view_height = temp * 15+40;
    
    UIView *inclusions = [[UIView alloc]init ];
    UIImageView *icon_inclusion = [[UIImageView alloc]initWithFrame:CGRectMake(X, Y, 20, 15)];
    [icon_inclusion setImage:[UIImage imageNamed:@"icon_inclusion"]];
    [icon_inclusion setContentMode:UIViewContentModeCenter];
    [inclusions addSubview:icon_inclusion];
    
    
    UILabel *inclusion_label = [[UILabel alloc]initWithFrame:CGRectMake(X + 25, Y, 200, 15)];
    [inclusion_label setText:@"Inclusions"];
    [inclusion_label setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [inclusion_label setTextColor:[Utils colorFromHex:@"#6d63b0"]];
    [inclusions addSubview:inclusion_label];
    
    [inclusions setBackgroundColor:[UIColor whiteColor]];
    [inclusions setFrame:CGRectMake(0, inclusion_startY, 320, inclusion_view_height)];
                          
    
    //[container setFrame:CGRectMake(0, 0, 320, inclusion_startY+inclusion_view_height + 20)];
    
    //[self.height setConstant:inclusion_startY+inclusion_view_height + 20];
    
    [container addSubview:inclusions];
    
    UIView *inclusion_left_panel = [[UIView alloc]initWithFrame:CGRectMake(X, Y + 15, 130, temp*15+5)];
    UIView *inclusion_right_panel = [[UIView alloc]initWithFrame:CGRectMake(X+150, Y + 15, 130, temp*15+5)];
    
    [inclusions addSubview:inclusion_left_panel];
    [inclusions addSubview:inclusion_right_panel];
    
    
    X = 5;
    Y = 5;
    for (int i = 0; i< inclusions_count; i++)
    {
        UILabel * l =[[UILabel alloc]initWithFrame:CGRectMake(X, Y + (i/2*15),150, 15)];
        [l setText:[NSString stringWithFormat:@"+ %@",[mainData getInclusions][i][@"featureName"]]];
        [l setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [l setTextColor:[UIColor grayColor]];
        if (i%2==1)
        {
            [inclusion_left_panel addSubview:l];
        }
        else
        {
            [inclusion_right_panel addSubview:l];
        
        }
        
    }

    
    //exclusion view
    int exclusion_startY = inclusion_startY + inclusion_view_height + marginY;
    
    
    X = 15;
    Y = 10;
    int exclusions_count = [mainData getExclusions].count;
    int temp2 = exclusions_count%2==0?exclusions_count/2:(exclusions_count/2)+1;
    int exclusion_view_height = temp2*15+40;
    
    UIView *exclusions = [[UIView alloc]init ];
    UIImageView *icon_exclusion = [[UIImageView alloc]initWithFrame:CGRectMake(X, Y, 20, 15)];
    [icon_exclusion setImage:[UIImage imageNamed:@"icon_exclusion"]];
    [icon_exclusion setContentMode:UIViewContentModeCenter];
    [exclusions addSubview:icon_exclusion];
    
    
    UILabel *exclusion_label = [[UILabel alloc]initWithFrame:CGRectMake(X + 25, Y, 200, 15)];
    [exclusion_label setText:@"Exclusions"];
    [exclusion_label setFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [exclusion_label setTextColor:[Utils colorFromHex:@"#6d63b0"]];
    [exclusions addSubview:exclusion_label];
    
    [exclusions setBackgroundColor:[UIColor whiteColor]];
    [exclusions setFrame:CGRectMake(0, exclusion_startY, 320, exclusion_view_height)];
    
    
    UIView *exclusion_left_panel = [[UIView alloc]initWithFrame:CGRectMake(X, Y + 15, 130, temp2*15+5)];
    UIView *exclusion_right_panel = [[UIView alloc]initWithFrame:CGRectMake(X+150, Y + 15, 130, temp2*15+5)];
    
    [exclusions addSubview:exclusion_left_panel];
    [exclusions addSubview:exclusion_right_panel];
    
    X = 5;
    Y = 5;
    for (int i = 0; i< exclusions_count; i++)
    {
        UILabel * l =[[UILabel alloc]initWithFrame:CGRectMake(X, Y + (i/2*15), 150, 15)];
        [l setText:[NSString stringWithFormat:@"+ %@",[mainData getExclusions][i][@"featureName"]]];
        [l setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [l setTextColor:[UIColor grayColor]];
        if (i%2==0)
        {
            [exclusion_left_panel addSubview:l];
        }
        else
        {
            [exclusion_right_panel addSubview:l];
            
        }
        
    }
    
    [container setFrame:CGRectMake(0, 0, 320, exclusion_startY+exclusion_view_height + 20)];
    [container addSubview:exclusions];
    [self.height setConstant:exclusion_startY+exclusion_view_height + 20];
    
    [bookingBtn setUserInteractionEnabled:YES];
    [bookingBtn addTarget:self action:@selector(bookingBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
}


-(UIView *)getHotelImageView:(NSString *)imageName Nights:(int)nightCount Title:(NSString *)title startX:(CGFloat)x startY:(CGFloat)y
{
    UIView *v=[[UIView alloc]initWithFrame:CGRectMake(x, y, 89 , 85)];
    [v setBackgroundColor:[Utils colorFromHex:@"#f57c00"]];
    
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(1,1, 87, 83)];
    [imgView setImageWithURL:[NSURL URLWithString:imageName] placeholderImage:[UIImage imageNamed:@"placeholder findHotels"]];
    [imgView setContentMode:UIViewContentModeScaleToFill];
    imgView.layer.cornerRadius = 2.0f;
    imgView.clipsToBounds = YES;
    [v addSubview:imgView];
    
    
    UIView *overlay = [[UIView alloc]initWithFrame:CGRectMake(1, 55, 87, 29)];
    [overlay setBackgroundColor:[Utils colorFromHex:@"#f57c00"]];
    [v addSubview:overlay];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(1, 57, 87, 25)];
    [label setText:[NSString stringWithFormat:@"%d nights in %@",nightCount,title]];
    [label setFont:[UIFont systemFontOfSize:10]];
    [label setNumberOfLines:2];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setTextColor:[UIColor whiteColor]];

    [v addSubview:label];
    [v bringSubviewToFront:label];
    
    v.layer.cornerRadius = 4.0f;
    
    return v;

}


-(IBAction)bookingBtnPressed:(id)sender
{
    NSLog(@"btn pressed");
    [[NSNotificationCenter defaultCenter]postNotificationName:ToTourAccomodations object:nil];
    
}


-(void)handleTap:(UITapGestureRecognizer *)tap
{

    [self.delegate didClickImage:YES index:tap.view.tag-900];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
