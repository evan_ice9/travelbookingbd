//
//  UserObject.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/11/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "UserObject.h"


@implementation UserObject




+(UserObject *)sharedInstance
{

    static UserObject *myUser = nil;
    
    static dispatch_once_t onceToken;
    
    
    dispatch_once(&onceToken, ^{
    
        
        myUser=[[UserObject alloc]init];
        
    });


    return myUser;

}



-(void)setUserID:(NSString *)User_id
{

    [[NSUserDefaults standardUserDefaults]setObject:User_id forKey:UserID];
    
}

-(NSString *)getUserID
{
    
  return [[NSUserDefaults standardUserDefaults]objectForKey:UserID];
}

-(void)setUserLoggedIn:(BOOL)loggedIn
{
    
    [[NSUserDefaults standardUserDefaults]setBool:loggedIn forKey:UserLoggedIn];
    
}


-(BOOL)IsUserLoggedIn
{

    return [[NSUserDefaults standardUserDefaults] boolForKey:UserLoggedIn];

}


-(void)SetAccountType:(AccountType)type
{

    [[NSUserDefaults standardUserDefaults]setObject:@(type) forKey:UserLoginType];
    

}



-(AccountType)getUserType
{

    return [[[NSUserDefaults standardUserDefaults]objectForKey:UserLoginType] intValue];
}



-(void)setUserName:(NSString *)name
{
    [[NSUserDefaults standardUserDefaults]setObject:name forKey:UserName];
}


-(void)setProfileURL:(NSString *)url
{
    [[NSUserDefaults standardUserDefaults]setObject:url forKey:UserphotoURL];
}


-(void)setEmail:(NSString *)email
{
    [[NSUserDefaults standardUserDefaults]setObject:email forKey:UserEmail];
}


-(void)setFbID:(NSString *)fbID
{

    [[NSUserDefaults standardUserDefaults]setObject:fbID forKey:UserFbID];

}

-(NSString *)userObjectForKey:(NSString *)key
{

    return   [[NSUserDefaults standardUserDefaults]objectForKey:key];

}

-(void)setBirthday:(NSString *)dob
{
    [[NSUserDefaults standardUserDefaults]setObject:dob forKey:UserDOB];
}


-(void)setLocation:(NSString *)location
{

    [[NSUserDefaults standardUserDefaults]setObject:location forKey:UserLocation];

}

-(void)setZip:(NSString *)zip
{

    [[NSUserDefaults standardUserDefaults]setObject:zip forKey:UserZip];

}

-(void)setNationality:(NSString *)nationality
{

    [[NSUserDefaults standardUserDefaults]setObject:nationality forKey:UserNationality];
}


-(void)setPassport:(NSString *)passport
{
    [[NSUserDefaults standardUserDefaults]setObject:passport forKey:UserPassport];
}

-(void)setCity:(NSString *)city
{
    [[NSUserDefaults standardUserDefaults]setObject:city forKey:UserCity];
}


-(void)clearUserPreference
{
    
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
}


-(void)setRecentSearchArray:(NSArray *)recentSearch
{

    [[NSUserDefaults standardUserDefaults]setObject:recentSearch forKey:UserSearchData];

}

-(NSArray *)getRecentSearchArray
{
    return [[NSUserDefaults standardUserDefaults]objectForKey:UserSearchData];
}


@end
