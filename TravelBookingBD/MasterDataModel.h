//
//  MasterDataModel.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MasterDataModel : NSObject


+(MasterDataModel *)shared_instance;


@property(strong,nonatomic) NSArray *featuredHotels;
@property(strong,nonatomic) NSArray *featuredTours;
@property(strong,nonatomic) NSArray *currencies;
@property(strong,nonatomic) NSArray *categories;
@property(strong,nonatomic) NSArray *packages;
@property(strong,nonatomic) NSArray *types;

-(NSString *)getTourTypeID:(NSString *)value;

@end
