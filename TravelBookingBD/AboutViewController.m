//
//  AboutViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

NSDictionary *_dataSource;

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _table.dataSource=self;
    _table.delegate=self;
    
    
    _dataSource=[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Strings" ofType:@"plist"]];
    _dataSource=[_dataSource objectForKey:@"About"];
    
    
    _table.tableHeaderView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, _table.frame.size.width, 20)];
    // Do any additional setup after loading the view.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataSource.allKeys.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    static NSString *cell_Identifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_Identifier];
    

    UILabel *title=(UILabel *)[cell viewWithTag:50];
    
    [title setText:[[_dataSource allKeys] objectAtIndex:indexPath.row]];
    
    
    UITextView *textView=(UITextView *)[cell viewWithTag:60];
    
    [textView setText:[[_dataSource allValues] objectAtIndex:indexPath.row]];
    
    textView.textColor=[Utils colorFromHex:@"#999999"];
    textView.font=[UIFont systemFontOfSize:11];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *text=[[_dataSource allValues] objectAtIndex:indexPath.row];
     UIFont  *cellfont=[UIFont systemFontOfSize:11.0f];
     
     CGSize constraintSize = CGSizeMake(200.0f, MAXFLOAT);  // Make changes in width as per your label requirement.
     
     CGSize textSize = [text sizeWithFont:cellfont constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
    
    return textSize.height+40;


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
