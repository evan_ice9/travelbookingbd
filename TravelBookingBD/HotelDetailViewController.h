//
//  HotelDetailViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/28/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "hotel.h"
#import <ASStarRatingView.h>

@protocol hotelDetailDelegate<NSObject>
-(void)didOpenImageSlider:(BOOL)value index:(int)indexValue;

@end

@interface HotelDetailViewController : UIViewController


@property id<hotelDetailDelegate> delegate;
@property BOOL isbooking;

@property (strong, nonatomic) IBOutlet UIScrollView *holderScroll;
- (IBAction)onBookingBtnPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *checkin;
@property (strong, nonatomic) IBOutlet UIImageView *checkout;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;



@property (strong, nonatomic) IBOutlet UILabel *hotelTitle;

@property (strong, nonatomic) IBOutlet UILabel *cityName;
@property (strong, nonatomic) IBOutlet UILabel *price;


@property (strong, nonatomic) IBOutlet UIImageView *img1;

@property (strong, nonatomic) IBOutlet UIImageView *img2;

@property (strong, nonatomic) IBOutlet UIImageView *img3;


@property (strong, nonatomic) IBOutlet UILabel *currencySymbol;


@property (strong, nonatomic) IBOutlet ASStarRatingView *ratingView;




@property (strong, nonatomic) IBOutlet UIView *container;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *containerHeight;


@property (strong, nonatomic) IBOutlet UIView *checkInView;

@property (strong, nonatomic) IBOutlet UIView *checkOutView;


@property (strong,nonatomic) NSDictionary *parameters;



-(void)setHotel:(hotel *)h;
-(void)setAttributesExtended:(hotel *)h withCancellation:(BOOL)cancel;

-(void)setBookingData:(NSDictionary *)dict;


@end
