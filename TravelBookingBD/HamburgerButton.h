//
//  HamburgerButton.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/22/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HamburgerButton : UIButton
{
    CAShapeLayer *top;
    CAShapeLayer *middle;
    CAShapeLayer *bottom;
}

@end
