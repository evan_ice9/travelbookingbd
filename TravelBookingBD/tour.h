//
//  tour.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/12/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface tour : NSObject
{
    
    NSString *tourID;
    NSString *title;
    NSString *category;
    NSString *startDate;
    NSString *expireDate;
    NSDictionary *startPlace;
    NSDictionary *endPlace;
    NSString *type;
    NSArray *destinations;
    int stars;
    int nights;
    int days;
    float basicPrice;
    float basicPriceAdult;
    float basicPriceChild;
    float childwithBed;
    float childwithoutBed;
    NSString *currency;
    NSString *priceType;
    int vatTax;
    NSString *vatTaxType;
    int cuttoffPeriod;
    NSArray *images;
    NSArray *itinerary;
    NSArray *features;
    NSString *description;
    NSDictionary *policies;
    NSArray *hotels;
}

-(id)initWithData:(NSDictionary *)data;
-(NSString *)getID;
-(NSString *)getTitle;

-(NSString *)getCategory;

-(NSString *)getStartDate;
-(NSString *)getExpiretDate;

-(NSDictionary *)getStartPlace;

-(NSDictionary *)getEndPlace;

-(NSString *)getType;

-(NSArray *)Destinations;

-(int)Stars;

-(int)nights;

-(int)days;

-(float)getPrice;

-(float)getBasicAdultPrice;

-(float)getBasicChildPrice;

-(float)getChildAlternatePrice;

-(NSString *)currency;


-(NSString *)priceType;

-(int)vatTaxAmount;

-(NSString *)vatTaxType;

-(int)cuttoffPeriod;

-(NSArray *)images;

-(NSString *)getCurrencySymBol;

-(NSArray *)getItinerary;
-(NSMutableArray *)getInclusions;
-(NSMutableArray *)getExclusions;
-(NSString *)getDescription;


-(NSString *)getPrivacyStatement;

-(NSString *)getRefundStatement;
-(NSString *)getTerms;
-(NSArray *)getPayments;
-(NSArray *)coveredHotels;

@end
