//
//  UserObject.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/11/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum
{
    
    emailAccount,
    fbAccount,
    gPlusAccount
    
} AccountType;

@interface UserObject : NSObject

+(UserObject *)sharedInstance;

-(void)setUserID:(NSString *)User_id;
-(NSString *)getUserID;
-(void)setUserLoggedIn:(BOOL)loggedIn;
-(BOOL)IsUserLoggedIn;
-(void)SetAccountType:(AccountType)type;
-(AccountType)getUserType;
-(void)setUserName:(NSString *)name;
-(void)setProfileURL:(NSString *)url;
-(void)setEmail:(NSString *)email;
-(void)setFbID:(NSString *)fbID;
-(void)setBirthday:(NSString *)dob;
-(void)setLocation:(NSString *)location;
-(void)setZip:(NSString *)zip;
-(void)setNationality:(NSString *)nationality;
-(void)setPassport:(NSString *)passport;
-(void)setCity:(NSString *)city;
-(void)setRecentSearchArray:(NSArray *)recentSearch;
-(NSArray *)getRecentSearchArray;

-(NSString *)userObjectForKey:(NSString *)key;
-(void)clearUserPreference;

@end
