//
//  HotelOverViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/28/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTabBarController.h>
#import "HotelDetailViewController.h"
#import "HotelRoomTypesViewController.h"
#import "imageScrollerViewController.h"
#import <YIInnerShadowView.h>
#import "hotel.h"

@interface HotelOverViewController : UIViewController<hotelDetailDelegate>
{
    HotelDetailViewController *vc1;
    HotelRoomTypesViewController *vc2;
    MGSwipeTabBarController *tab;
}

@property (strong, nonatomic) IBOutlet UIView *holder;
@property (strong,nonatomic) hotel *mainData;
@property (strong,nonatomic) NSDictionary *bookingData;
@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) NSDictionary *parameters;


- (IBAction)back:(id)sender;
-(void)setData:(hotel *)h;

@end
