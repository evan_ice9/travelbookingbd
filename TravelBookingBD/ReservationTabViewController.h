//
//  ReservationTabViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/26/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MGSwipeTabBarController.h>
#import <YIInnerShadowView.h>
#import "ReservationTableViewController.h"

@interface ReservationTabViewController : UIViewController
{

    ReservationTableViewController *uv1;
    ReservationTableViewController *uv2;

}

-(void)promptLogin;


@property (strong, nonatomic) IBOutlet UIView *holder;
@property MGSwipeTabBarController *tabController;

@end
