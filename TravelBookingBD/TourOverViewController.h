//
//  TourOverViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TourDetailViewController.h"
#import "TourAccommodationViewController.h"
#import <MGSwipeTabBarController.h>
#import "imageScrollerViewController.h"

@interface TourOverViewController : UIViewController<modalMethods,tourDetailDelegate>


@property (strong, nonatomic) IBOutlet UIView *header;
@property (strong, nonatomic) IBOutlet UIView *tabView;
- (IBAction)back:(id)sender;

-(void)setData:(tour *)t;

@end
