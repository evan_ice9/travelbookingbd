//
//  SearchViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 2/24/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PDTSimpleCalendar.h>
#import "hotel.h"
#import "tour.h"
#import "TableViewWithBlock.h"
#import "SelectionCell.h"




#define handle_Tap(view,delegate,selector) do {\
view.userInteractionEnabled = YES;\
[view addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:delegate action:selector]];\
} while(0)

@protocol RecentSearchesDelegate <NSObject>
-(void)searchDataDidUpdate;
@end

@interface SearchViewController : UIViewController<UITextFieldDelegate,PDTSimpleCalendarViewDelegate,PDTSimpleCalendarViewCellDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (retain,nonatomic) id<RecentSearchesDelegate> RSdelegate;
@property (strong,nonatomic) UIPickerView *pickerView;
@property (strong, nonatomic) IBOutlet UIView *formContainer;
@property (strong, nonatomic) IBOutlet UIView *hotelsContainer;
@property (strong, nonatomic) IBOutlet UIView *offersContainer;

@property (strong, nonatomic) IBOutlet UITextField *searchText;

@property (strong, nonatomic) IBOutlet UIView *checkin;

@property (strong, nonatomic) IBOutlet UIView *checkout;

@property (strong, nonatomic) IBOutlet TableViewWithBlock *autoTable;

@property (strong, nonatomic) IBOutlet UIImageView *inputErrorIcon;

@property (strong, nonatomic) IBOutlet UILabel *errorMessage;






- (IBAction)onBtnPress:(id)sender;




@end
