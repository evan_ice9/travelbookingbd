//
//  HotelSearchResultViewController.h
//  TravelBookingBD
//
//  Created by Amit Kumar Saha on 2/18/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ASStarRatingView.h>
#import "hotel.h"
#import "HotelOverViewController.h"
#import "HotelFilterViewController.h"

@interface HotelSearchResultViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,hotelSearchFilterDelegate>


@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) NSDictionary *parameters;
@property (strong, nonatomic) NSArray *dataSource;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property (strong, nonatomic) IBOutlet UIView *dateview;

@property (strong, nonatomic) IBOutlet UIView *noRooms;

@property (strong, nonatomic) IBOutlet UIView *nightsview;

@property (strong, nonatomic) IBOutlet UIView *adultsview;

@property (strong, nonatomic) IBOutlet UIView *childview;

@property (strong, nonatomic) IBOutlet UILabel *paramText;

@property (strong, nonatomic) IBOutlet UIButton *btnFilter;

- (IBAction)filterBtn:(id)sender;

- (IBAction)back:(id)sender;
@end
