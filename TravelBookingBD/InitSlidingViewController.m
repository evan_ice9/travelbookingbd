//
//  InitSlidingViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "InitSlidingViewController.h"

#define slide_amount 270.0f
#define top_offset 70.0f
#define peek_amount 50.0f


@interface InitSlidingViewController ()

@end

@implementation InitSlidingViewController
{
    UIView *modal;
    UIView *filterView;
    UIPickerView *pickerView;
    UIView *containerView;
    NSArray *pickerData;
    int selectedIndex;
    PDTSimpleCalendarViewController *calendarViewController;
    NSDate *selectedDate;
    UIView *calendarContainerView;
    UIView *checkInView;
    int t_type,t_category,t_package,t_price;
    SearchViewController *searches;
}


-(void)viewDidAppear:(BOOL)animated
{
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(sideMenuPressed:) name:SideMenu_Action object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(HotelDetailView:) name:hotelDetailView object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchView:) name:searchResult object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(SideMenuVisibilityCheck:) name:Side_Menu_Visibility object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showSignIn) name:SideMenuSignIn object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showSignUp:) name:SideMenuSignUp object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showHotels) name:showFeatureHotel object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showTours) name:showFeatureTour object:nil];
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


-(void)loadTopView
{

    searches =(SearchViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
    [self addChildViewController:searches];
    
    [searches.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
    [self._holderView addSubview:searches.view];
    self.sideMenuTitle = @"Search Hotels";
    [self.TitleLable setText:self.sideMenuTitle];
    
}


-(void)showHotels
{
    
    [self.TitleLable setText:@"Featured Hotels"];
    self.sideMenuTitle = @"Featured Hotels";

    
    for (UIViewController *v in self.childViewControllers)
    {
        if ([v isKindOfClass:[FeaturedHotelViewController class]])
        {
            [self._holderView bringSubviewToFront:v.view];
            return;
        }
    }
    
    
    FeaturedHotelViewController *featuredHotels =(FeaturedHotelViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"featuredHotel"];
    [self addChildViewController:featuredHotels];
    
    [featuredHotels.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
    [self._holderView addSubview:featuredHotels.view];

}


-(void)showTours
{
    [self.TitleLable setText:@"Featured Tours"];
    self.sideMenuTitle = @"Featured Tours";
    self.filterBtn.hidden = NO;
    
    for (UIViewController *v in self.childViewControllers)
    {
        if ([v isKindOfClass:[FeaturedToursViewController class]])
        {
            [self._holderView bringSubviewToFront:v.view];
            return;
        }
    }
    
    
    FeaturedToursViewController *featuredTours =(FeaturedToursViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"featuredTour"];
    [self addChildViewController:featuredTours];
    
    [featuredTours.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
    [self._holderView addSubview:featuredTours.view];
}




-(void)SideMenuVisibilityCheck:(NSNotification *)not
{

    BOOL visibility = [[not userInfo][@"value"] boolValue];

   [self sideMenuEnable:visibility];
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    transition_animation=NO;
    side_menu=(SideMenuController *)[self.storyboard instantiateViewControllerWithIdentifier:@"SideMenuTable"];
    side_menu.topOffset=top_offset;
    side_menu.slideAmount=slide_amount-self.view.frame.size.width;
    side_menu.peekAmount=peek_amount;
    side_menu.view.layer.zPosition = 70;
    
    
    
    side_menu.view.frame=CGRectMake(-self.view.frame.size.width, top_offset, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:side_menu.view];
    [self.view addGestureRecognizer:side_menu.panGesture];
    side_menu.delegate = self;
    
    
    maskLayer=[[UIView alloc]initWithFrame:CGRectMake(0, top_offset, self.view.frame.size.width,  self.view.frame.size.height)];
    [maskLayer setBackgroundColor:[UIColor clearColor]];
    maskLayer.layer.zPosition = 20;
    
    [Utils roundedLayer:self.top_menu.layer radius:0.0 shadow:YES bounds:self.top_menu.bounds];
    

    selectedDate = [[NSDate alloc]initWithTimeInterval:4*24*60*60 sinceDate:[NSDate date]];
    
    t_type = [MasterDataModel shared_instance].types.count-1,t_category = [MasterDataModel shared_instance].categories.count-1,t_package = [MasterDataModel shared_instance].packages.count-1;
    
    [self loadTopView];
    
    // Do any additional setup after loading the view.
}



-(void)showSlider
{
    
    if (transition_animation)
        return;
    
    [self.view addSubview:maskLayer];
    
    transition_animation=YES;
    
    [UIView animateWithDuration:.5f animations:^{
        
        side_menu.view.frame=CGRectMake(slide_amount-self.view.frame.size.width, top_offset, self.view.frame.size.width, self.view.frame.size.height);
        [maskLayer setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:.4]];
        
    } completion:^(BOOL finished) {
        
        
        side_menu.sideMenuOpen=YES;
        transition_animation=NO;
        [self.view bringSubviewToFront:side_menu.view];
        [self.TitleLable setText:@"Travel Booking Bangladesh"];
        self.TitleLable.minimumScaleFactor = .9;
        self.TitleLable.adjustsFontSizeToFitWidth = YES;
        self.TitleLable.adjustsLetterSpacingToFitWidth = YES;
        
    }];
    
    
}


-(void)hideSlider
{
    if (transition_animation)
        return;
    
    
    transition_animation=YES;
    [UIView animateWithDuration:.5f animations:^{
        
        side_menu.view.frame=CGRectMake(-self.view.frame.size.width, top_offset, self.view.frame.size.width, self.view.frame.size.height);
        [maskLayer setBackgroundColor:[UIColor clearColor]];
        
    } completion:^(BOOL finished) {
        
        side_menu.sideMenuOpen=NO;
        transition_animation=NO;
        [maskLayer removeFromSuperview];
        if (self.sideMenuTitle.length>0)
        {
            [self.TitleLable setText:self.sideMenuTitle];
        }
    }];
    
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark sideMenu Methods

-(void)scrollPosition:(float)x
{
    x *= -1;
    NSLog(@"%f",x);
    
    if (x>=50 && x<320)
    {
        [self.view bringSubviewToFront:side_menu.view];
        
        if ([[self.view subviews] containsObject:maskLayer])
        {
            float percentage = x/270;
            float alphaValue = .4*(1-percentage);
            [maskLayer setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:alphaValue]];
            NSLog(@"%f",alphaValue);
        }
        else
        {
            [self.view addSubview:maskLayer];
            float percentage = x/270;
            float alphaValue = .4*(1-percentage);
            [maskLayer setBackgroundColor:[[UIColor clearColor] colorWithAlphaComponent:alphaValue]];
            NSLog(@"%f",alphaValue);
        }
    }
    else
    {
        [maskLayer removeFromSuperview];
    }
    
}

-(void)showSignIn
{
    SignInViewController *signIn = (SignInViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signIn"];
    [self presentViewController:signIn animated:YES completion:^(void)
    {
    
        NSLog(@"Search hotels");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[SearchViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                return;
            }
        }
        
        
        SearchViewController *recentSearches =(SearchViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
        [self addChildViewController:recentSearches];
        
        [recentSearches.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:recentSearches.view];
    
    
    }];
}


-(void)showSignUp:(NSNotification *)not
{
    NSDictionary *userData = [not userInfo];
    
    SignUpViewController *signUp = (SignUpViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signUp"];
    signUp.fbUserInfo = userData;
    [self presentViewController:signUp animated:YES completion:nil];
}

-(void)sideMenuEnable:(BOOL)value
{
    side_menu.panGesture.enabled = value;
}


-(void)sideMenuAdjust
{

    if (side_menu.sideMenuOpen)
    {
        [self hideSlider];
    }
    else
    {
        
        [self showSlider];
    }

}


- (IBAction)sideBtnClick:(id)sender
{

    NSLog(@"pressed");
    
    [self sideMenuAdjust];
}

-(void)sideMenuPressed:(NSNotification *)notification
{
    
    NSDictionary *user_info=[notification userInfo];
    
    NSString *value=[user_info objectForKey:@"value"];
    BOOL adjust = (BOOL)[user_info objectForKey:@"doAdjust"];
    
    if (adjust)
    {
        [self sideMenuAdjust];
    }
    
    if (value && ![value isEqualToString:@"Sign In"])
    {
        [self.TitleLable setText:value];
        self.sideMenuTitle = value;
        self.filterBtn.hidden = [value isEqualToString:@"Featured Tours"]?NO:YES;
        if (![value isEqualToString:@"Featured Tours"])
        {
            [filterView removeFromSuperview];
            [modal removeFromSuperview];
        }
    }

    
    if ([value isEqualToString:@"About"])
    {
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[AboutViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                [self sideMenuAdjust];
                return;
            }
        }
        
        AboutViewController *aboutController=(AboutViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"About_view_controller"];
        [self addChildViewController:aboutController];
        [aboutController.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:aboutController.view];
        [self sideMenuAdjust];
    }
    else if ([value isEqualToString:@"Sign In"])
    {
        [self sideMenuAdjust];
        if ([[UserObject sharedInstance]IsUserLoggedIn])
        {        
            ProfileViewController *profileView = (ProfileViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"profileView"];
			profileView.topController = searches;
            [self presentViewController:profileView animated:YES completion:nil];
        }
        else
        {
        
            SignInViewController *signIn = (SignInViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"signIn"];
			signIn.topViewController = searches;
            [self presentViewController:signIn animated:YES completion:nil];
        }
        
        
    }
    else if ([value isEqualToString:@"Featured Hotels"])
    {
        
        NSLog(@"featured hotels");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[FeaturedHotelViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                [v viewDidLoad];
                [self sideMenuAdjust];
                return;
            }
        }
        
        
        FeaturedHotelViewController *featuredHotels =(FeaturedHotelViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"featuredHotel"];
        [self addChildViewController:featuredHotels];
        
        [featuredHotels.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:featuredHotels.view];
        [self sideMenuAdjust];
        
    }
    else if ([value isEqualToString:@"Featured Tours"])
    {
        
        NSLog(@"featured tours");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[FeaturedToursViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                [self sideMenuAdjust];
                return;
            }
        }
        
        
        FeaturedToursViewController *featuredTours =(FeaturedToursViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"featuredTour"];
        [self addChildViewController:featuredTours];
        
        [featuredTours.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:featuredTours.view];
        [self sideMenuAdjust];
        
    }
    else if ([value isEqualToString:@"Recent Searches"])
    {
        
        NSLog(@"Recent searches");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[RecentSearchesViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                //[v viewDidLoad];
                [self sideMenuAdjust];
                return;
            }
        }
        RecentSearchesViewController *recentSearches =(RecentSearchesViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"recent_searches"];
        [self addChildViewController:recentSearches];
        
        searches.RSdelegate = recentSearches;
        
        [recentSearches.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:recentSearches.view];
        [self sideMenuAdjust];
        
    }
    else if ([value isEqualToString:@"Search Hotels"])
    {
        
        
        NSLog(@"Search hotels");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[SearchViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                [self sideMenuAdjust];
                return;
            }
        }
        
        
        SearchViewController *recentSearches =(SearchViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"searchView"];
        [self addChildViewController:recentSearches];
        
        [recentSearches.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:recentSearches.view];
        [self sideMenuAdjust];
        
        
        
        
        
    }
    else if ([value isEqualToString:@"My Reservations"])
    {
        
        NSLog(@"My Reservation");
        
        for (UIViewController *v in self.childViewControllers)
        {
            if ([v isKindOfClass:[ReservationTabViewController class]])
            {
                [self._holderView bringSubviewToFront:v.view];
                ReservationTabViewController *tab = (ReservationTabViewController *)v;
                [tab promptLogin];
                
                [self sideMenuAdjust];
                return;
            }
        }
        
        
        ReservationTabViewController *reserve_tab =(ReservationTabViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"reserveation_tab"];
        [self addChildViewController:reserve_tab];
        
        [reserve_tab.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
        [self._holderView addSubview:reserve_tab.view];
        [self sideMenuAdjust];
        
    }
    
    
    
    
    
}


#pragma mark FilterViewMethods


- (IBAction)filterBtnClick:(id)sender
{
    if (!filterView)
    {
        filterView = [[[NSBundle mainBundle]loadNibNamed:@"tourFilter" owner:self options:0] firstObject];
        
        modal = [[UIView alloc]init];
        [modal setBackgroundColor:[UIColor blackColor]];
        modal.alpha = .4;
        [modal setFrame:CGRectMake(self._holderView.frame.origin.x, self._holderView.frame.origin.y, self._holderView.frame.size.width, self._holderView.frame.size.height)];
		UITapGestureRecognizer *cancelFilter = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(filterCancelBtn:)];
		[modal addGestureRecognizer:cancelFilter];
        modal.layer.zPosition = 60;
		
        [self.view addSubview:modal];
        
        [filterView setFrame:CGRectMake(20, 100, 280, 401)];
        filterView.layer.zPosition = 70;
        [self.view addSubview:filterView];
        
        checkInView = [filterView viewWithTag:350];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(popCalenderView:)];
        [checkInView addGestureRecognizer:tap];
        
        side_menu.panGesture.enabled = NO;
        
        UIButton *doneBtn = (UIButton *)[filterView viewWithTag:450];
        [doneBtn addTarget:self action:@selector(filterDone:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *resetBtn = (UIButton *)[filterView viewWithTag:452];
        [resetBtn addTarget:self action:@selector(filterReset:) forControlEvents:UIControlEventTouchUpInside];
        
        UIScrollView *scroll = (UIScrollView *)[filterView viewWithTag:500];
        scroll.delaysContentTouches = NO;
        scroll.bounces = NO;
        
        M13Checkbox *checkBox = (M13Checkbox *)[filterView viewWithTag:480];
        checkBox.checkColor = [Utils colorFromHex:@"#6D63B0"];
        checkBox.strokeColor = [Utils colorFromHex:@"#6D63B0"];
		checkBox.checkState = t_price == 1?M13CheckboxStateChecked:M13CheckboxStateUnchecked;
		
        [self setTextDate:checkInView date:selectedDate];
        
        UIButton *tourType = (UIButton *)[filterView viewWithTag:220];
        UIButton *tourCategory = (UIButton *)[filterView viewWithTag:230];
        UIButton *packageType = (UIButton *)[filterView viewWithTag:240];
        
        
        UILabel *typeLabel = (UILabel *)[filterView viewWithTag:245];
        [typeLabel setText:[MasterDataModel shared_instance].types[t_type]];
        
        UILabel *categoryLabel = (UILabel *)[filterView viewWithTag:246];
        [categoryLabel setText:[MasterDataModel shared_instance].categories[t_category]];
        
        UILabel *packageLabel = (UILabel *)[filterView viewWithTag:247];
        [packageLabel setText:[MasterDataModel shared_instance].packages[t_package]];
        
        [tourType addTarget:self action:@selector(tourTypeSelect:) forControlEvents:UIControlEventTouchUpInside];
        [tourCategory addTarget:self action:@selector(tourTypeSelect:) forControlEvents:UIControlEventTouchUpInside];
        [packageType addTarget:self action:@selector(tourTypeSelect:) forControlEvents:UIControlEventTouchUpInside];
    }
    
}


-(IBAction)tourTypeSelect:(UIButton *)sender
{
    int tag = sender.tag;
    
    
    switch (tag)
    {
        case 220:
        {
           NSLog(@"tour type");
            pickerData = [MasterDataModel shared_instance].types;
        }
            break;
        case 230:
        {
           NSLog(@"tour category");
           pickerData = [MasterDataModel shared_instance].categories;
        }
            break;
        case 240:
        {
           NSLog(@"package packages");
           pickerData = [MasterDataModel shared_instance].packages;
        }
            break;
            
    }

    [self showPickerView:tag];

}


-(IBAction)filterDone:(id)sender
{
    
    UILabel *searchText = (UILabel *)[filterView viewWithTag:120];
    UILabel *type = (UILabel *)[filterView viewWithTag:245];
    NSString *typeString = [[MasterDataModel shared_instance]getTourTypeID:type.text];
    
    UILabel *category = (UILabel *)[filterView viewWithTag:246];
    UILabel *package = (UILabel *)[filterView viewWithTag:247];
    
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    
    
    M13Checkbox *priceSortCheck = (M13Checkbox *)[filterView viewWithTag:480];
    
    NSDictionary *param;
    
    if ( priceSortCheck.checkState == M13CheckboxStateChecked)
    {
		t_price = 1;
		
        if ([searchText.text isEqualToString:@""])
        {
            param = @{@"tourType":typeString,@"tourCategory":category.text,@"packageType":package.text,@"sort":@"price",@"startDate":[f stringFromDate:selectedDate],@"filter":@"true",@"offset":@"0",@"limit":@"10"};
        }
        else
        {
            param = @{@"destination":searchText.text,@"tourType":typeString,@"tourCategory":category.text,@"packageType":package.text,@"sort":@"price",@"startDate":[f stringFromDate:selectedDate],@"filter":@"true",@"offset":@"0",@"limit":@"10"};
        }
        
    }
    else
    {
		t_price = 0;
		
        if ([searchText.text isEqualToString:@""])
        {
            param = @{@"tourType":typeString,@"tourCategory":category.text,@"packageType":package.text,@"startDate":[f stringFromDate:selectedDate],@"filter":@"true",@"offset":@"0",@"limit":@"10"};
        }
        else
        {
        
            param = @{@"destination":searchText.text,@"tourType":typeString,@"tourCategory":category.text,@"packageType":package.text,@"startDate":[f stringFromDate:selectedDate],@"filter":@"true",@"offset":@"0",@"limit":@"10"};
        }
    
    }

    
    for (UIViewController *v in self.childViewControllers)
    {
        if ([v isKindOfClass:[FeaturedToursViewController class]])
        {
            FeaturedToursViewController *ft = (FeaturedToursViewController *)v;
            [ft updateFilterOptions:param];
            break;
        }
    }
    
    [modal removeFromSuperview];
    [filterView removeFromSuperview];
    filterView = nil;
    side_menu.panGesture.enabled = YES;

}

-(IBAction)filterReset:(id)sender
{
    
    for (UIViewController *v in self.childViewControllers)
    {
        if ([v isKindOfClass:[FeaturedToursViewController class]])
        {
            FeaturedToursViewController *ft = (FeaturedToursViewController *)v;
            [ft loadTours];
            break;
        }
    }
    
    
    [modal removeFromSuperview];
    [filterView removeFromSuperview];
    filterView = nil;
    side_menu.panGesture.enabled = YES;

}

-(void)filterCancelBtn:(id)sender
{
	[modal removeFromSuperview];
	[filterView removeFromSuperview];
	filterView = nil;
	modal = nil;
	
	if (containerView)
	{
    	[containerView removeFromSuperview];
		containerView = nil;
	}
	side_menu.panGesture.enabled = YES;
}


#pragma mark FilterPickerView delegates

-(void)showPickerView:(int)tag
{
    
    pickerView=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 240, 270)];
    pickerView.dataSource=self;
    pickerView.delegate=self;
    
    
    switch (tag)
    {
        case 220:
        {
            [pickerView selectRow:t_type inComponent:0 animated:NO];
			selectedIndex = t_type;
        }
            break;
        case 230:
        {
            [pickerView selectRow:t_category inComponent:0 animated:NO];
     		selectedIndex = t_category;
        }
            break;
        case 240:
        {
           [pickerView selectRow:t_package inComponent:0 animated:NO];
     		selectedIndex = t_package;
        }
            break;
    }
    
    containerView=[[UIView alloc]initWithFrame:CGRectMake(40, 150, 240, 300)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 270, 240, 30)];
    containerView.backgroundColor=[UIColor whiteColor];
    containerView.layer.cornerRadius=5.0;
    modal.layer.zPosition = 80;
    containerView.layer.zPosition = 90;
    
    [containerView addSubview:pickerView];
    doneBtn.tag=tag;
    [doneBtn setBackgroundImage:[Utils imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(pickerSelectDone:) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    [self.view addSubview:containerView];
    [self.view bringSubviewToFront:containerView];
    containerView.clipsToBounds = YES;
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{

    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return  pickerData.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return pickerData[row];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    selectedIndex = row;
}

-(IBAction)pickerSelectDone:(UIButton *)sender
{
    
    [containerView removeFromSuperview];
    modal.layer.zPosition = 60;
    
    
    switch (sender.tag)
    {
        case 220:
        {
        
            UILabel *typeLabel = (UILabel *)[filterView viewWithTag:245];
            [typeLabel setText:pickerData[selectedIndex]];
            t_type = selectedIndex;
        }
            break;
        case 230:
        {
            
            UILabel *categoryLabel = (UILabel *)[filterView viewWithTag:246];
            [categoryLabel setText:pickerData[selectedIndex]];
            t_category = selectedIndex;
        }
            break;
        case 240:
        {
            
            UILabel *packageLabel = (UILabel *)[filterView viewWithTag:247];
            [packageLabel setText:pickerData[selectedIndex]];
            t_package = selectedIndex;
        }
            break;
            
        default:
            break;
    }
}
#pragma mark CalendarView delegates

-(IBAction)popCalenderView:(UIButton *)sender
{
    
    calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
    NSDate *firstDate=[[NSDate date] dateByAddingTimeInterval:4*24*60*60];
    NSDateComponents *offset =[[NSDateComponents alloc]init];
    offset.month = 12;
    calendarViewController.firstDate = firstDate;
    calendarViewController.lastDate=[calendarViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0];
    
    [calendarViewController setSelectedDate:selectedDate];
    [calendarViewController scrollToSelectedDate:NO];
    [calendarViewController.collectionView reloadData];
    [calendarViewController setDelegate:self];
    
    
    
    calendarContainerView=[[UIView alloc]initWithFrame:CGRectMake(20, 80, 280, 400)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];
            
    [self addChildViewController:calendarViewController];
    [calendarContainerView addSubview:calendarViewController.view];
    doneBtn.backgroundColor=[UIColor grayColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
    [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
    [calendarContainerView addSubview:doneBtn];
    [self.view addSubview:calendarContainerView];
    [self.view bringSubviewToFront:calendarContainerView];
    
    calendarContainerView.layer.zPosition = 90;
    modal.layer.zPosition = 80;
            
}

-(void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    selectedDate = date;
}


-(void)setTextDate:(UIView *)view date:(NSDate *)d
{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *c = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:d];
    
    UILabel *month = (UILabel *)[view viewWithTag:601];
    UILabel *weekDay = (UILabel *)[view viewWithTag:602];
    UILabel *day = (UILabel *)[view viewWithTag:600];
    
    [month setText:[NSString stringWithFormat:@"%@",[Utils getMonthOfYear:c.month]]];
    [weekDay setText:[NSString stringWithFormat:@"%@",[Utils getDayOfWeek:c.weekday]]];
    [day setText:[NSString stringWithFormat:@"%d",c.day]];
    
    
}

-(IBAction)dateConfirmBtn:(UIButton *)sender
{
      checkInView = [filterView viewWithTag:350];
      [self setTextDate:checkInView date:selectedDate];
    
      [calendarContainerView removeFromSuperview];
      modal.layer.zPosition = 60;
}





#pragma mark ModalViews

-(void)HotelDetailView:(NSNotification *)not
{

    NSLog(@"%@",[not userInfo]);
    int index =[[not userInfo][@"index"] intValue];
    
    
    for (UIViewController *v in self.childViewControllers)
    {
        if ([v isKindOfClass:[HotelOverViewController class]])
        {
            HotelOverViewController *overview = (HotelOverViewController *)v;
            [overview setData:[MasterDataModel shared_instance].featuredHotels[index]];
            [self._holderView bringSubviewToFront:overview.view];
            overview = nil;
            return;
        }
    }
    
    HotelOverViewController *overview=(HotelOverViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"hotelOverView"];
    overview.mainData = [MasterDataModel shared_instance].featuredHotels[index];
    [self addChildViewController:overview];
    [overview.view setFrame:CGRectMake(0, 0, self._holderView.frame.size.width, self._holderView.frame.size.height)];
    [self._holderView addSubview:overview.view];

}



-(void)searchView:(NSNotification *)not
{
    NSDictionary *dict = [not userInfo];
    HotelSearchResultViewController *hs = [self.storyboard instantiateViewControllerWithIdentifier:@"searcheResults"];
    hs.parameters = dict;
    [self presentViewController:hs animated:YES completion:nil];
}

@end
