//
//  HamburgerButton.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/22/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HamburgerButton.h"

@implementation HamburgerButton


CGFloat width =18.0f;
CGFloat height =16.0f;
CGFloat topYPosition =2.0f;
CGFloat middleYPosition = 7.0f;
CGFloat bottomYPosition =12.0f;




-(id)initWithFrame:(CGRect)frame
{
    if((self=[super initWithFrame:frame]))
    {
        
        UIBezierPath *path=[[UIBezierPath alloc]init];
        [path moveToPoint:CGPointMake(0, 0)];
        [path addLineToPoint:CGPointMake(width, 0)];
        
        top.path=path.CGPath;
        
        [path moveToPoint:CGPointMake(0, 0)];
        [path addLineToPoint:CGPointMake(width, 0)];
        
        
    }
    return self;

}

@end


