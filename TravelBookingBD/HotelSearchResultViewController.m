//
//  HotelSearchResultViewController.m
//  TravelBookingBD
//
//  Created by Amit Kumar Saha on 2/18/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "HotelSearchResultViewController.h"

@interface HotelSearchResultViewController ()

@end


@implementation HotelSearchResultViewController
{
    RadioButton *r1,*r2;
    UIView *filterHotelView,*modal;
    NMRangeSlider *nm1,*nm2;
    int offset,limit;
    NSString *searchId;
    BOOL ready,scrollEnd;
    NSString *sortType;
    NSString *priceLowerValue,*priceUpperValue,*ratingLowerValue,*ratingUpperValue;
    NSArray *areaData;
    TableViewWithBlock *areaTable;
    HotelFilterViewController *filterView;
	NSString *minPrice,*maxPrice;

}

@synthesize table,parameters,dataSource,spinner,btnFilter;

@synthesize dateview,noRooms,nightsview,adultsview,childview,paramText;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    ready = YES;
    scrollEnd = NO;
    offset = 0;
    limit = 10;
    
    table.delegate=self;
    table.dataSource=self;
    [self setupViews];
    [self loadData:offset reset:NO];
    
    // Do any additional setup after loading the view.
}



-(void)setupViews
{

    [Utils roundedLayer:dateview.layer radius:2.0 shadow:YES bounds:dateview.bounds];
    [Utils roundedLayer:noRooms.layer radius:2.0 shadow:YES bounds:noRooms.bounds];
    [Utils roundedLayer:nightsview.layer radius:2.0 shadow:YES bounds:nightsview.bounds];
    [Utils roundedLayer:adultsview.layer radius:2.0 shadow:YES bounds:adultsview.bounds];
    [Utils roundedLayer:childview.layer radius:2.0 shadow:YES bounds:childview.bounds];
    
    NSDictionary *filter = [parameters objectForKey:@"rooms"][0];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *checkinDate = [formatter dateFromString:parameters[@"checkInDate"]];
    NSDate *checkOutDate = [formatter dateFromString:parameters[@"checkOutDate"]];
    
    
    
    UILabel *date = (UILabel *)[dateview viewWithTag:150];
    UILabel *noOfRooms = (UILabel *)[noRooms viewWithTag:150];
    UILabel *nights = (UILabel *)[nightsview viewWithTag:150];
    UILabel *adults = (UILabel *)[adultsview viewWithTag:150];
    UILabel *children = (UILabel *)[childview viewWithTag:150];
    
    [formatter setDateFormat:@"dd MMM"];
    [date setText:[NSString stringWithFormat:@"%@-%@",[formatter stringFromDate:checkinDate],[formatter stringFromDate:checkOutDate]]];
    [noOfRooms setText:filter[@"rooms"]];
    [nights setText:filter[@"nights"]];
    [adults setText:filter[@"adults"]];
    [children setText:filter[@"children"]];
    
    sortType = @"price";
}

-(void)loadData:(int)off reset:(BOOL)r
{

    NSMutableDictionary *temp = [parameters mutableCopy];
    [temp setObject:[NSString stringWithFormat:@"%d",off] forKey:@"offset"];
    [temp setObject:[NSString stringWithFormat:@"%d",limit] forKey:@"limit"];
    if (searchId)
    {
        [temp setObject:searchId forKey:@"searchId"];
    }
    
    parameters = temp;
    
    
    [[Network shared_instance]getHotelswithParameters:parameters completionBlock:^(int status, id response)
    {
        switch (status) {
            case 0:
            {
                [spinner stopAnimating];
                NSLog(@"%@",response);
                
                if (r)
                {
                    searchId = [response objectForKey:@"searchId"];
                    dataSource = response[@"hotels"];
                }
                else
                {
                    searchId = [response objectForKey:@"searchId"];
                    NSMutableArray *temp = [NSMutableArray arrayWithArray:dataSource];
                    [temp addObjectsFromArray:response[@"hotels"]];
                    dataSource = temp;
                }
                
                [table reloadData];
                [paramText setText:[NSString stringWithFormat:@"%@ search results found for '%@' city",response[@"totalRecords"],parameters[@"cityName"]]];
                ready = YES;
                [self hideActivityIndicator];
                [self hideFilterActivityIndicator];
                if ([response[@"hotels"] count]==0)
                {
                    scrollEnd = YES;
                }
				else
				{
					btnFilter.hidden = NO;
					minPrice = response[@"minPrice"];
					maxPrice = response[@"maxPrice"];
				}

            }
                break;
            case 1:
            {
                NSLog(@"failed");
            
            }
                
                break;
                
            default:
                break;
        }
    }];



}
#pragma mark TableView Delegates



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    NSString *cellIdentifier=@"cell";
    UITableViewCell *cell=[table dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    hotel *h=[[hotel alloc]initWithData:dataSource[indexPath.row]];
    
    UIImageView *imgView = (UIImageView *)[cell viewWithTag:75];
    UILabel *hotelTitle = (UILabel *)[cell viewWithTag:70];
    UILabel *priceView = (UILabel *)[cell viewWithTag:71];
    UILabel *currencySymbol = (UILabel *)[cell viewWithTag:72];
    UILabel *cityName = (UILabel *)[cell viewWithTag:73];
    ASStarRatingView *ratingView = (ASStarRatingView *)[cell viewWithTag:74];
    UIView *container = (UIView *)[cell viewWithTag:80];
    UIView *featuredHolder = (UIView *)[cell viewWithTag:77];
    UILabel *featuredLabel = (UILabel *)[cell viewWithTag:78];
    
    
    if ([h images].count>0)
    {
    [imgView setImageWithURL:[NSURL URLWithString:[h images][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder findHotels"]];
    }
    
    [hotelTitle setText:[Utils filterTitle:[h getName]]];
    
    if (roundf([h getLowestPrice])==[h getLowestPrice])
    {
        [priceView setText:[NSString stringWithFormat:@"%.f",[h getLowestPrice]]];
    }
    else
    {
        [priceView setText:[NSString stringWithFormat:@"%.2f",[h getLowestPrice]]];
    }
    
    
    if (![h isFeatured])
    {
        featuredHolder.hidden = YES;
        featuredLabel.hidden = YES;
    }
    else
    {
        featuredHolder.hidden = NO;
        featuredLabel.hidden = NO;
    }
    
    [currencySymbol setText:[h getCurrencySymBol]];
    [cityName setText:[h getCityName]];
    
    ratingView.rating = h.getRating;
    ratingView.canEdit = NO;
    [Utils roundedLayer:container.layer radius:2.0 shadow:YES bounds:container.bounds];
    
    return cell;


}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{


    return dataSource.count;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    hotel *h=[[hotel alloc]initWithData:dataSource[indexPath.row]];
    
    HotelOverViewController *hc=(HotelOverViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"hotelOverView"];
    hc.mainData = h;
    hc.parameters = parameters;
    [self presentViewController:hc animated:YES completion:nil];

}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{

    int row = indexPath.row;
    
    
    if (row == [dataSource count]-1 && !scrollEnd)
    {
        
        if (ready)
        {
          offset +=limit;
          ready = NO;
            
          [self loadData:offset reset:NO];
          [self showActivityIndicator];
          NSLog(@"lazy load");
            
        }
        

    }


}


-(void)showActivityIndicator
{
    UIView *activity = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    UIActivityIndicatorView *aI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [aI startAnimating];
    [aI setColor:[Utils colorFromHex:@"#8BC34A"]];
    [activity addSubview:aI];
    aI.center = CGPointMake(160, 30);
    
    [activity setBackgroundColor:[UIColor clearColor]];
    
    table.tableFooterView = activity;
    
}

-(void)hideActivityIndicator
{
    table.tableFooterView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark FilterBtn-Methods

- (IBAction)filterBtn:(id)sender
{
    
    modal = [[UIView alloc]init];
    [modal setBackgroundColor:[UIColor blackColor]];
    modal.alpha = .4;
    [modal setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
    modal.layer.zPosition = 60;
    UITapGestureRecognizer *modalTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(removeModalView)];
    [self.view addSubview:modal];
    [modal addGestureRecognizer:modalTap];
    
    
    filterView = [self.storyboard instantiateViewControllerWithIdentifier:@"filterView"];
    filterView.parameters = parameters;
	filterView.priceUpperValue = maxPrice;
	filterView.priceLowerValue = minPrice;
    [filterView.view setFrame:CGRectMake(20, 50, 280, 478)];
    [self addChildViewController:filterView];
    filterView.view.layer.zPosition = 70;
    filterView.delegate = self;
    [filterView didMoveToParentViewController:self];
    [self.view addSubview:filterView.view];
    
}


-(void)applyFilter:(NSDictionary *)filter
{

    NSMutableDictionary *temp = [[NSMutableDictionary alloc]init];
    temp = [parameters mutableCopy];
    [temp setObject:filter forKey:@"filter"];
    parameters = temp;
    offset = 0;
    
    [filterView removeFromParentViewController];
    [filterView.view removeFromSuperview];
    [filterView didMoveToParentViewController:nil];
    [modal removeFromSuperview];
    
    [self.table setContentOffset:CGPointZero animated:YES];
    scrollEnd = NO;
    [self loadData:offset reset:YES];
    [self showFilterActivityIndicator];

}


-(void)removeModalView
{

    [filterView removeFromParentViewController];
    [filterView.view removeFromSuperview];
    [filterView didMoveToParentViewController:nil];
    [modal removeFromSuperview];

}



-(void)showFilterActivityIndicator
{

    UIView *activity = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    UIActivityIndicatorView *aI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [aI startAnimating];
    [aI setColor:[Utils colorFromHex:@"#8BC34A"]];
    [activity addSubview:aI];
    aI.center = CGPointMake(160, 30);
    
    [activity setBackgroundColor:[UIColor clearColor]];
    
    table.tableHeaderView = activity;
    
}

-(void)hideFilterActivityIndicator
{
    table.tableHeaderView = nil;
}


- (IBAction)back:(id)sender
{

    [self dismissViewControllerAnimated:YES completion:^{
        
        [[Network shared_instance]cancelAllConnections];
        
    }];
}

@end
