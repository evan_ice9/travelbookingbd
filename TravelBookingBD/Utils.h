//
//  Utils.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MasterDataModel.h"
#import "InitSlidingViewController.h"
#import "SignInViewController.h"
#import <Security/Security.h>
#import <CommonCrypto/CommonHMAC.h>

@interface Utils : NSObject

+(UIColor *)colorFromHex:(NSString *)string;
+ (UIImage *)imageNamed:(NSString *)name imagetintColor:(UIColor *)color ;
+ (void)roundedLayer:(CALayer *)viewLayer radius:(float)r shadow:(BOOL)s bounds:(CGRect)b;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(UIBezierPath *)createPathwithframe:(CGRect)frame;
+(NSDictionary *)getFormattedDate:(NSString *)date;
+(NSString *)getCurrencySymBol:(NSString *)currency;
+(NSString *)getMonthOfYear:(int)month;
+(NSString *)getDayOfWeek:(int )day;
+(NSString *)filterTitle:(NSString *)title;
+(NSString *)hashValue:(NSString *)privateKey publicKey:(NSString *)publicKey;
+(NSString *)generateRandomString:(int)length;
+(float)rounded2decimal:(float)value;
@end

