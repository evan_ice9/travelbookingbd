//
//  singleImageViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 5/31/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface singleImageViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property NSString *imgUrl;

@end
