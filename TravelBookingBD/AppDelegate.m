//
//  AppDelegate.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/20/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
    [GMSServices provideAPIKey:@"AIzaSyAtGlgdtv3FZVk8OuZLUyEdkaSJ8ZDuxEQ"];
    // Override point for customization after application launch.
    
    NSURLCache *UrlCache = [[NSURLCache alloc]initWithMemoryCapacity:1024*1024*4 diskCapacity:1024*1024*100 diskPath:@"app_cache"];
    [NSURLCache setSharedURLCache:UrlCache];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    // Handle the user leaving the app while the Facebook login dialog is being shown
    // For example: when the user presses the iOS "home" button while the login dialog is active
    [FBAppCall handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation
{
    
    if ([[url scheme] rangeOfString:@"fb"].location==NSNotFound)
    {
        return [GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation];
    }
        return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}





-(void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error
{
    // If the session was opened successfully
    if (!error && state == FBSessionStateOpen){
        NSLog(@"Session opened");
        // Show the user the logged-in UI
        [self userLoggedIn];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
        // If the session is closed
        NSLog(@"Session closed");
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
    
    // Handle errors
    if (error){
        NSLog(@"Error");
        NSString *alertText;
        NSString *alertTitle;
        // If the error requires people using an app to make an action outside of the app in order to recover
        if ([FBErrorUtility shouldNotifyUserForError:error] == YES){
            alertTitle = @"Something went wrong";
            alertText = [FBErrorUtility userMessageForError:error];
            [self showMessage:alertText withTitle:alertTitle];
        } else {
            
            // If the user cancelled login, do nothing
            if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryUserCancelled) {
                NSLog(@"User cancelled login");
                
                // Handle session closures that happen outside of the app
            } else if ([FBErrorUtility errorCategoryForError:error] == FBErrorCategoryAuthenticationReopenSession){
                alertTitle = @"Session Error";
                alertText = @"Your current session is no longer valid. Please log in again.";
                [self showMessage:alertText withTitle:alertTitle];
                
                // Here we will handle all other errors with a generic error message.
                // We recommend you check our Handling Errors guide for more information
                // https://developers.facebook.com/docs/ios/errors/
            } else {
                //Get more error information from the error
                NSDictionary *errorInformation = [[[error.userInfo objectForKey:@"com.facebook.sdk:ParsedJSONResponseKey"] objectForKey:@"body"] objectForKey:@"error"];
                
                // Show the user an error message
                alertTitle = @"Something went wrong";
                alertText = [NSString stringWithFormat:@"Please retry. \n\n If the problem persists contact us and mention this error code: %@", [errorInformation objectForKey:@"message"]];
                [self showMessage:alertText withTitle:alertTitle];
            }
        }
        // Clear this token
        [FBSession.activeSession closeAndClearTokenInformation];
        // Show the user the logged-out UI
        [self userLoggedOut];
    }
}


-(void)setUpProfile
{

    FBRequest *request=[FBRequest requestForMe];
    
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
    {
        
        NSDictionary *data=result;
        //NSString *fullName=[NSString stringWithFormat:@"%@",data[@"name"]];
        NSString *userID=[NSString stringWithFormat:@"%@",data[@"id"]];
        NSString *email=[NSString stringWithFormat:@"%@",data[@"email"]];
        
        [[Network shared_instance]fbLoginWithParameters:@{@"email":email,@"fbId":userID} completionBlock:^(int status, id response)
        {
            switch (status) {
                case 0:
                {
                
                    NSLog(@"%@",response);
                    [[UserObject sharedInstance]setUserLoggedIn:YES];
                    [[UserObject sharedInstance]SetAccountType:fbAccount];
                    [self loginSuccess:response];
                    
                }
                    break;
                case 1:
                {
                    NSLog(@"%@",response);
                    [[NSNotificationCenter defaultCenter]postNotificationName:SideMenuSignUp object:nil userInfo:result];
                }
                    break;                
            }
            
            
            
        }];
        /*NSString *birthday =[NSString stringWithFormat:@"%@",data[@"birthday"]];
        NSString *location =[NSString stringWithFormat:@"%@",data[@"location"][@"name"]];
        
        NSLog(@"%@ %@ %@ %@ %@",fullName,userID,email,birthday,location);
        
        UserObject *u=[UserObject sharedInstance];
        [u setEmail:email];
        [u setUserName:fullName];
        [u setFbID:userID];
        [u setBirthday:birthday];
        [u setLocation:location];
        
        
        [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];*/
        
    }];

}


-(void)loginSuccess:(NSDictionary *)info
{
    
    NSLog(@"user Logged in");
    info = [info objectForKey:@"user"];
    [[UserObject sharedInstance]setUserLoggedIn:YES];
    [[UserObject sharedInstance]SetAccountType:emailAccount];
    
    
    NSString *fullName=[NSString stringWithFormat:@"%@.%@ %@",info[@"title"],info[@"firstName"],info[@"lastName"]];
    NSString *userID=[NSString stringWithFormat:@"%@",info[@"id"]];
    NSString *email=[NSString stringWithFormat:@"%@",info[@"email"]];
    NSString *birthday =[NSString stringWithFormat:@"%@",info[@"dob"]];
    NSString *location =[NSString stringWithFormat:@"%@",info[@"address"]];
    NSString *photoURL = info[@"image"];
    NSString *zip = info[@"zip"];
    NSString *nationality = info[@"nationality"];
    NSString *passport_no = info[@"passport"];
    NSString *city_name = info[@"city"][@"name"];
    
    NSLog(@"%@ %@ %@ %@ %@",fullName,userID,email,birthday,location);
    
    UserObject *u=[UserObject sharedInstance];
    [u setEmail:email];
    [u setUserName:fullName];
    [u setBirthday:birthday];
    [u setLocation:location];
    [u setProfileURL:photoURL];
    [u setUserID:userID];
    [u setZip:zip];
    [u setNationality:nationality];
    [u setPassport:passport_no];
    [u setCity:city_name];

	UIViewController *rootController = self.window.rootViewController;
	while ([rootController.presentedViewController isKindOfClass:[InitSlidingViewController class]])
	{
		rootController = rootController.presentedViewController;
	}
	
	if ([rootController isKindOfClass:[InitSlidingViewController class]])
    {
		for (UIViewController *uv in rootController.childViewControllers)
		{
			if ([uv isKindOfClass:[RecentSearchesViewController class]])
			{
				RecentSearchesViewController *rs = (RecentSearchesViewController *)uv;
				[rs reloadRecentSearchData];
			}
			
		}
	}
	
	
    [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];
}


-(void)userLoggedIn
{

    NSLog(@"user Logged in");
    [self setUpProfile];
    
}

-(void)userLoggedOut
{
    
    [[UserObject sharedInstance]clearUserPreference];
    [[UserObject sharedInstance]setUserLoggedIn:NO];
    NSLog(@"user Logged out");
    [[NSNotificationCenter defaultCenter]postNotificationName:Reload_Table object:nil];
}

-(void)showMessage:(NSString *)text withTitle:(NSString *)title
{


}



@end
