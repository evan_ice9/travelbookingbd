//
//  InitSlidingViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/21/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

@protocol tourFilter<NSObject>
-(void)updateFilterOptions:(NSDictionary *)params;
@end

#import <UIKit/UIKit.h>
#import "AboutViewController.h"
#import "SideMenuController.h"
#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "FeaturedHotelViewController.h"
#import "HotelSearchResultViewController.h"
#import "SearchViewController.h"
#import "ProfileViewController.h"
#import "ReservationTabViewController.h"
#import "RecentSearchesViewController.h"
#import "HotelOverViewController.h"
#import "HotelSearchResultViewController.h"
#import <M13Checkbox.h>
#import "FeaturedToursViewController.h"

@interface InitSlidingViewController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,scrolingDelegate,PDTSimpleCalendarViewDelegate,PDTSimpleCalendarViewCellDelegate>
{

    SideMenuController *side_menu;
    UIView             *maskLayer;
    BOOL transition_animation;

}

@property (strong, nonatomic) IBOutlet UIView *top_menu;
@property (strong, nonatomic) IBOutlet UIView *_holderView;
@property (strong, nonatomic) IBOutlet UIButton *filterBtn;
@property (strong, nonatomic) IBOutlet UILabel *TitleLable;
@property (strong, nonatomic) NSString *sideMenuTitle;
@property (weak,nonatomic) id<tourFilter> delegate;

- (IBAction)sideBtnClick:(id)sender;

- (IBAction)filterBtnClick:(id)sender;

@end
