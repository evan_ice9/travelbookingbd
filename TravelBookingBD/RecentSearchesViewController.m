//
//  RecentSearches.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/5/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "RecentSearchesViewController.h"

@implementation RecentSearchesViewController
{

    NSArray *Maindata;
    BOOL ready;
    UIView *v,*maskView,*containerView,*calendarContainerView;
    UILabel *rooms,*nights,*adults,*children,*currency;
    UITapGestureRecognizer *tap;
    UIPickerView *pickerView;
    NSArray *pickerData;
    int pickerViewSelectedIndex;
    PDTSimpleCalendarViewController *calanderViewController;
    NSDate *defaultCheckInDate,*selectedDate,*checkOutDate;
    MBProgressHUD *hud;
}


-(void)viewDidAppear:(BOOL)animated
{
    /*if ([[UserObject sharedInstance] IsUserLoggedIn])
    {
        [self showActivityIndicator];
        [self loadData];
    }
    else
    {
        [self processOfflineData:[[NSUserDefaults standardUserDefaults] objectForKey:offlineSearchArray]];
    }*/
}

-(void)viewDidLoad
{
    NSLog(@"layout appear");
    
    self.tableView.tableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    [self.tableView.tableFooterView setBackgroundColor:[UIColor clearColor]];
    defaultCheckInDate = [NSDate dateWithTimeInterval:4*60*60*24 sinceDate:[NSDate date]];
    checkOutDate = [NSDate dateWithTimeInterval:60*60*24 sinceDate:defaultCheckInDate];
    ready = YES;
    
    if ([[UserObject sharedInstance] IsUserLoggedIn])
    {
        [self showActivityIndicator];
        [self loadData];
    }
    else
    {
        [self processOfflineData:[[NSUserDefaults standardUserDefaults] objectForKey:offlineSearchArray]];
    }
}

-(void)loadData
{

    [[Network shared_instance]recentSearcheswithUserID:[[UserObject sharedInstance]getUserID] completionBlock:^(int status, id response)
    {
     
        switch (status) {
            case 0:
            {
            
                NSLog(@"%@",response);
                Maindata = response;
                [[UserObject sharedInstance] setRecentSearchArray:response];
                [self hideActivityIndicator];
                [self.tableView reloadData];
                ready = YES;
            
            }
                break;
            case 1:
            {
            
                Maindata = [[UserObject sharedInstance]getRecentSearchArray];
                [self hideActivityIndicator];
                [self.tableView reloadData];
                NSLog(@"failure");
            
            }
                break;
                
            default:
                break;
        }
        
        
        
        
        
    }];
    
}

-(void)processOfflineData:(NSArray *)arr
{

    NSMutableArray *offlineData = [[NSMutableArray alloc]init];
    
    for (NSDictionary *dict in arr)
    {
        
        NSDictionary *data = @{@"Childrens":dict[@"rooms"][0][@"children"],
                               @"adults":dict[@"rooms"][0][@"adults"],
                               @"checkInDate":dict[@"checkInDate"],
                               @"checkOutDate":dict[@"checkOutDate"],                              @"city":@{@"cityCode":dict[@"city"],@"name":dict[@"cityName"]},
                               @"currency":dict[@"currency"],
                               @"rooms":dict[@"rooms"][0][@"rooms"]
                               };
        [offlineData addObject:data];
        
    }
    
    
    Maindata = offlineData;
    [self.tableView reloadData];

}

-(void)showActivityIndicator
{
    UIView *activity = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    UIActivityIndicatorView *aI = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [aI startAnimating];
    [aI setColor:[Utils colorFromHex:@"#8BC34A"]];
    [activity addSubview:aI];
    aI.center = CGPointMake(160, 30);
    
    [activity setBackgroundColor:[UIColor clearColor]];
    
    self.tableView.tableHeaderView = activity;

}

-(void)hideActivityIndicator
{
    self.tableView.tableHeaderView = nil;
}


#pragma mark ModalView delegates

-(void)showModalView:(NSInteger)index
{
    
    [self dimIn];
    
    [[NSNotificationCenter defaultCenter]postNotificationName:Side_Menu_Visibility object:nil userInfo:@{@"value":@(NO)}];
    
    self.tableView.scrollEnabled = NO;
    v =[[[NSBundle mainBundle]loadNibNamed:@"modalView" owner:self options:nil] firstObject];
    v.layer.cornerRadius = 4.0;
    v.clipsToBounds = YES;
    
    tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelModalView)];
    [maskView addGestureRecognizer:tap];
    
    [self.view addSubview:v];
    [v setFrame:CGRectMake(30,self.tableView.contentOffset.y + 40, 260, 262)];
    UIButton *dnBtn =(UIButton *)[v viewWithTag:95];
    dnBtn.tag = index;
    [dnBtn addTarget:self action:@selector(doneBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    
    for (int k=90; k<95; k++)
    {
        UIButton *selectBtn= (UIButton *)[v viewWithTag:k];
        [selectBtn addTarget:self action:@selector(showPickerView:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    rooms = (UILabel *)[v viewWithTag:190];
    nights = (UILabel *)[v viewWithTag:191];
    adults = (UILabel *)[v viewWithTag:192];
    children = (UILabel *)[v viewWithTag:193];
    currency = (UILabel *)[v viewWithTag:194];
    
    UIButton *checkInBtn = (UIButton*)[v viewWithTag:53];
    UIButton *checkOutBtn = (UIButton *)[v viewWithTag:54];
    
    UIView *checkInView = [v viewWithTag:50];
    UIView *checkOutView = [v viewWithTag:51];
    
    [self setTextDate:checkInView date:[[NSDate date] dateByAddingTimeInterval:4*24*60*60]];
    [self setTextDate:checkOutView date:[[NSDate date] dateByAddingTimeInterval:5*24*60*60]];
    
    [checkInBtn addTarget:self action:@selector(popCalenderView:) forControlEvents:UIControlEventTouchUpInside];
    [checkOutBtn addTarget:self action:@selector(popCalenderView:) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)dimIn
{
    
    maskView=[[UIView alloc]initWithFrame:self.view.bounds];
    maskView.backgroundColor=[UIColor blackColor];
    maskView.alpha=0;
    [self.view addSubview:maskView];
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=.8;
    }];
    
    
    
}

-(void)dimOut
{
    
    [UIView animateWithDuration:.5 animations:^{
        
        
        maskView.alpha=0;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [maskView removeFromSuperview];
            maskView=nil;
            
        }
    }];
    
}

-(void)cancelModalView
{
    self.tableView.scrollEnabled = YES;
    [v removeFromSuperview];
    [self dimOut];

}

-(IBAction)doneBtn:(id)sender
{
    
    UIButton *btn = (UIButton *)sender;
    int index = btn.tag;
    NSDictionary *data = [Maindata objectAtIndex:index];
    NSDateFormatter * f =[[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    
    NSString *startDate = [f stringFromDate:defaultCheckInDate];
    NSString *endDate = [f stringFromDate:checkOutDate];
    
    NSDictionary *dict = @{@"currency":currency.text,@"city":data[@"city"][@"cityCode"],@"checkInDate":startDate,@"checkOutDate":endDate,@"nationality":@"BD",@"rooms":@[@{@"adults":adults.text,@"children":children.text,@"nights":nights.text,@"rooms":rooms.text}],@"offset":@"0",@"limit":@"10"};
    
    [[NSNotificationCenter defaultCenter]postNotificationName:searchResult object:nil userInfo:dict];
    [self cancelModalView];

}

#pragma mark PickerView delegates

-(IBAction)showPickerView:(UIButton *)sender
{
    int tag = (int)sender.tag;
    if (tag == 94)
    {
        pickerData = @[@"USD",@"INR",@"GBP",@"EUR",@"YEN",@"BDT"];
    }
    else
    {
        NSMutableArray *arr = [[NSMutableArray alloc]init];
        for (int i = 0; i<30; i++)
        {
            [arr addObject:[NSString stringWithFormat:@"%d",i]];
        }
        
        pickerData = arr;
    }
    
    pickerView=[[UIPickerView alloc]initWithFrame:CGRectMake(0, 0, 240, 270)];
    pickerView.dataSource=self;
    pickerView.delegate=self;
    
    
    containerView=[[UIView alloc]initWithFrame:CGRectMake(40, 10, 240, 300)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 270, 240, 30)];
    containerView.backgroundColor=[UIColor whiteColor];
    containerView.layer.cornerRadius=5.0;
    
    [containerView addSubview:pickerView];
    doneBtn.tag=tag;
    doneBtn.backgroundColor=[UIColor grayColor];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(pickerSelectDone:) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:doneBtn];
    [self.view addSubview:containerView];
    [self.view bringSubviewToFront:containerView];
    containerView.clipsToBounds = YES;
    
}


-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    return 1;
    
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return pickerData.count;
    
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    return   pickerData[row];
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    pickerViewSelectedIndex = (int)row;
    
}
-(IBAction)pickerSelectDone:(id)sender
{
    
    UIButton *btn=(UIButton *)sender;
    
    switch (btn.tag) {
        case 90:
            NSLog(@"rooms %@",pickerData[pickerViewSelectedIndex]);
            [rooms setText:pickerData[pickerViewSelectedIndex]];
            break;
            
        case 91:
            NSLog(@"nights %@",pickerData[pickerViewSelectedIndex]);
            [nights setText:pickerData[pickerViewSelectedIndex]];
            break;
            
        case 92:
            NSLog(@"adults %@",pickerData[pickerViewSelectedIndex]);
            [adults setText:pickerData[pickerViewSelectedIndex]];
            break;
            
        case 93:
            NSLog(@"children %@",pickerData[pickerViewSelectedIndex]);
            [children setText:pickerData[pickerViewSelectedIndex]];
            break;
            
        case 94:
            NSLog(@"currency %@",pickerData[pickerViewSelectedIndex]);
            [currency setText:pickerData[pickerViewSelectedIndex]];
            break;
        default:
            break;
            
    }
    
    [containerView removeFromSuperview];
    
}



#pragma mark CalendarView delegates


-(IBAction)popCalenderView:(UIButton *)sender
{
    NSLog(@"tapped %d",(int)sender.tag);
    
    int tag = (int)sender.tag;
    
    calanderViewController = [[PDTSimpleCalendarViewController alloc] init];
    NSDateComponents *offset=[[NSDateComponents alloc]init];
    offset.month = -1;
    NSDate *firstDate=(tag==53)?[[NSDate date] dateByAddingTimeInterval:4*24*60*60]:[NSDate dateWithTimeInterval:60*60*24 sinceDate:defaultCheckInDate];
    calanderViewController.firstDate = firstDate;
    offset.month=(tag == 53)?12:1;
    calanderViewController.lastDate = (tag == 53)?[calanderViewController.calendar dateByAddingComponents:offset toDate:[NSDate date] options:0]:[calanderViewController.calendar dateByAddingComponents:offset toDate:defaultCheckInDate options:0];
    
    
    if (tag==53)
    {
        
        selectedDate = defaultCheckInDate;
        
    }
    else if(tag == 54)
    {
        
        selectedDate = checkOutDate;
        
    }
    
    [calanderViewController setSelectedDate:selectedDate];
    [calanderViewController scrollToSelectedDate:NO];
    [calanderViewController.collectionView reloadData];
    [calanderViewController setDelegate:self];
    
    
    
    calendarContainerView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 280, 400)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];
    
    switch (tag)
    {
            
            
        case 53:
            
            [self addChildViewController:calanderViewController];
            [calendarContainerView addSubview:calanderViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            doneBtn.tag = tag;
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calanderViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [calendarContainerView addSubview:doneBtn];
            [self.view addSubview:calendarContainerView];
            [self.view bringSubviewToFront:calendarContainerView];
            break;
            
            
            
        case 54:
            
            [self addChildViewController:calanderViewController];
            [calendarContainerView addSubview:calanderViewController.view];
            doneBtn.backgroundColor=[UIColor grayColor];
            [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
            doneBtn.titleLabel.textColor=[UIColor whiteColor];
            doneBtn.tag = tag;
            [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
            [calanderViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
            [calendarContainerView addSubview:doneBtn];
            [self.view addSubview:calendarContainerView];
            [self.view bringSubviewToFront:calendarContainerView];
            
            break;
            
        default:
            break;
    }
    
}

-(void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    selectedDate = date;
}



-(void)setTextDate:(UIView *)view date:(NSDate *)d
{
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *c = [calendar components:(NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday) fromDate:d];
    
    UILabel *month = (UILabel *)[view viewWithTag:60];
    UILabel *weekDay = (UILabel *)[view viewWithTag:61];
    UILabel *day = (UILabel *)[view viewWithTag:62];
    
    [month setText:[NSString stringWithFormat:@"%@",[Utils getMonthOfYear:c.month]]];
    [weekDay setText:[NSString stringWithFormat:@"%@",[Utils getDayOfWeek:c.weekday]]];
    [day setText:[NSString stringWithFormat:@"%d",c.day]];
    
    
}


-(IBAction)dateConfirmBtn:(UIButton *)sender
{
    NSLog(@"%d",(int)sender.tag);
    
    
    switch ((int)sender.tag)
    {
        case 53:
        {
            
            UIView *checkInView = [v viewWithTag:50];
            UIView *checkOutView = [v viewWithTag:51];
            [self setTextDate:checkInView date:selectedDate];
            [self setTextDate:checkOutView date:[NSDate dateWithTimeInterval:60*60*24 sinceDate:selectedDate]];
            defaultCheckInDate = selectedDate;
            checkOutDate = [NSDate dateWithTimeInterval:60*60*24 sinceDate:selectedDate];
            
            
        }
            break;
            
        case 54:
        {
            UIView *checkOutView = [v viewWithTag:51];
            [self setTextDate:checkOutView date:selectedDate];
            checkOutDate = selectedDate;
            
        }
            break;
        default:
            break;
    }
    [calendarContainerView removeFromSuperview];
}


#pragma mark tableview delegates

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;

}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return Maindata.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier=@"cell";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    UIView *containerMaskView=(UIView *)[cell viewWithTag:10];
    [Utils roundedLayer:containerMaskView.layer radius:2.0 shadow:YES bounds:containerView.bounds];
    
    NSDictionary *data = [Maindata objectAtIndex:indexPath.row];
    
    
    
    UILabel *destinationText = (UILabel *)[cell viewWithTag:130];
    UILabel *checkInText = (UILabel *)[cell viewWithTag:131];
    UILabel *checkOutText = (UILabel *)[cell viewWithTag:132];
    UILabel *lblrooms = (UILabel *)[cell viewWithTag:133];
    UILabel *lblnights = (UILabel *)[cell viewWithTag:134];
    UILabel *lbladults = (UILabel *)[cell viewWithTag:135];
    UILabel *lblchildren = (UILabel *)[cell viewWithTag:136];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *d1 = [formatter dateFromString:data[@"checkInDate"]];
    NSDate *d2 = [formatter dateFromString:data[@"checkOutDate"]];
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *c = [calendar components:(NSCalendarUnitDay) fromDate:d1 toDate:d2 options:0];
    
    
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setDateFormat:@"dd MMM"];
    
    
    
    NSString *startDate = [formatter stringFromDate:d1];
    NSString *endDate = [formatter stringFromDate:d2];
    
    [checkInText setText:startDate];
    [checkOutText setText:endDate];
    [destinationText setText:data[@"city"][@"name"]];
    [lblrooms setText:[NSString stringWithFormat:@"%@",data[@"rooms"]]];
    
    NSString *adultCount = [NSString stringWithFormat:@"%@",data[@"adults"]];
    NSString *childCount = [NSString stringWithFormat:@"%@",data[@"Childrens"]];
    
    [lblchildren setText:childCount];
    [lbladults setText:adultCount];
    [lblnights setText:[NSString stringWithFormat:@"%d",c.day]];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{


    NSDictionary *data = [Maindata objectAtIndex:indexPath.row];
    NSString *adultCount = [NSString stringWithFormat:@"%@",data[@"adults"]];
    NSString *childCount = [NSString stringWithFormat:@"%@",data[@"Childrens"]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *d1 = [formatter dateFromString:data[@"checkInDate"]];
    NSDate *d2 = [formatter dateFromString:data[@"checkOutDate"]];
    
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSDateComponents *c = [calendar components:(NSCalendarUnitDay) fromDate:d1 toDate:d2 options:0];
    
    NSString *nightsCount = [NSString stringWithFormat:@"%d",c.day];
    
    NSString *roomsCount = [NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",data[@"rooms"]]];
    
    NSDateComponents *com = [calendar components:(NSDayCalendarUnit) fromDate:[NSDate date] toDate:d1 options:0];
    
    
    if (com.day<0)
    {
    
        [self showModalView:indexPath.row];
    }
    else
    {
    
		NSDictionary *dict = @{@"currency":@"GBP",@"city":data[@"city"][@"cityCode"],@"cityName":data[@"city"][@"name"],@"checkInDate":data[@"checkInDate"],@"checkOutDate":data[@"checkOutDate"],@"nationality":@"BD",@"rooms":@[@{@"adults":adultCount,@"children":childCount,@"nights":nightsCount,@"rooms":roomsCount}],@"offset":@"0",@"limit":@"10"};
        
        [[NSNotificationCenter defaultCenter]postNotificationName:searchResult object:nil userInfo:dict];

    }
    
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{

    if (!self.tableView.isDecelerating)
    {
        if (self.tableView.contentOffset.y < -40)
        {
            if (ready)
            {
                if ([[UserObject sharedInstance] IsUserLoggedIn])
                {
                    [self loadData];
                    NSLog(@"load");
                    ready = NO;
                    [self showActivityIndicator];
                    
                }
                else
                {
                    [self processOfflineData:[[NSUserDefaults standardUserDefaults] objectForKey:offlineSearchArray]];
                    NSLog(@"Offline load");
                    //ready = NO;
                    //[self showActivityIndicator];
                
                }
            }
            
        }
    }

}

#pragma mark SearchDelegate

-(void)searchDataDidUpdate
{
	[self reloadRecentSearchData];
}



-(void)reloadRecentSearchData
{

	if ([[UserObject sharedInstance] IsUserLoggedIn])
	{
		[self loadData];
		NSLog(@"load");
		ready = NO;
		[self showActivityIndicator];
		
	}
	else
	{
		[self processOfflineData:[[NSUserDefaults standardUserDefaults] objectForKey:offlineSearchArray]];
		NSLog(@"Offline load");
		//ready = NO;
		//[self showActivityIndicator];
		
	}

}


@end
