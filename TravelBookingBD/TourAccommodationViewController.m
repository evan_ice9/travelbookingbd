//
//  TourAccommodationViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/19/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "TourAccommodationViewController.h"

@interface TourAccommodationViewController ()

@end

@implementation TourAccommodationViewController
{
    tour *mainData;
    UIPickerView *pickerView;
    UIView *containerView,*background,*bookingModalView;
    NSMutableArray *pickerData;
    int pickerSelectedIndex;
    PDTSimpleCalendarViewController *calendarViewController;
    NSDate *selectedDate;
    MBProgressHUD *hud;
    
}

@synthesize container,scrollview,heightConstraint;
@synthesize tourTitle,price,tourType;
@synthesize img1,img2,img3,currencySymbol,tag1,tag2,spinner;
@synthesize checkInView,calendarIcon,bookingCard;

- (void)viewDidLoad
{
    [super viewDidLoad];
}



-(void)setAttributes:(tour *)t
{
    mainData = t;
    [tourTitle setText:[t getTitle]];
    [price setText:[NSString stringWithFormat:@"%.2f",[t getPrice]]];
    [tourType setText:[t getType]];
    
    
    if ([mainData images].count>0)
    {
        [img1 setImageWithURL:[NSURL URLWithString:[mainData images][0][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-1"]];
        
        if ([mainData images].count>1)
        {
            
            [img2 setImageWithURL:[NSURL URLWithString:[mainData images][1][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            
            if ([mainData images].count>2)
            {
                [img3 setImageWithURL:[NSURL URLWithString:[mainData images][2][@"src"]] placeholderImage:[UIImage imageNamed:@"placeholder Details-2"]];
            }
        }
        
    }
    
    
    UILabel *l1 =(UILabel *)[tag1 viewWithTag:55];
    UILabel *l2 =(UILabel *)[tag2 viewWithTag:55];
    
    [l1 setText:[t getCategory]];
    [l2 setText:[t priceType]];
    tag1.layer.cornerRadius = 5.0;
    tag2.layer.cornerRadius = 5.0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(checkInBtnPressed:)];
    [checkInView addGestureRecognizer:tap];
    
    calendarIcon.image = [calendarIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    calendarIcon.tintColor = [UIColor whiteColor];
    
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate =[f dateFromString:[mainData getStartDate]];
    selectedDate = startDate;
    [self setDate:startDate];
}

-(void)setup:(tour *)t
{
    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    UITapGestureRecognizer *tap2 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    UITapGestureRecognizer *tap3 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    
    img1.userInteractionEnabled = YES;
    img2.userInteractionEnabled = YES;
    img3.userInteractionEnabled = YES;
    
    img1.tag = 900;
    img2.tag = 901;
    img3.tag = 902;
    
    [img1 addGestureRecognizer:tap1];
    [img2 addGestureRecognizer:tap2];
    [img3 addGestureRecognizer:tap3];
    
    [spinner removeFromSuperview];

    mainData = t;
    
   /* int price_view_startY_alternative = 0;
    if (![[t priceType] isEqualToString:@"Individual"])
    {
        bookingCard.translatesAutoresizingMaskIntoConstraints = YES;
        price_view_startY_alternative = 30;
        CGRect frame = bookingCard.frame;
        frame.size.height +=price_view_startY_alternative;
        bookingCard.frame = frame;
        bookingCardHeightConstant.constant = frame.size.height;
        ChildAlternativeDiv.hidden = false;
        alternativeDivider.hidden = false;
        childDivLabel.text = @"Child with bed";
    }*/
    
    
    
    //price view
	int price_view_startY = 391 + 8;// + price_view_startY_alternative;
    int pricing_height = 92;
    
    UIView *pricing_options = [[UIView alloc]initWithFrame:CGRectMake(0, price_view_startY, 320, pricing_height)];
    [pricing_options setBackgroundColor:[UIColor whiteColor]];
    [container addSubview:pricing_options];
    
    int startX = 20;
    int startY = 10;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(startX+25, startY, 200, 20)];
    [label setText:@"Payment options"];
    [label setFont:[UIFont fontWithName:@"Helvetica" size:12]];
    [label setTextColor:[Utils colorFromHex:@"#4C457B"]];
    [pricing_options addSubview:label];
    
   
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(startX, startY, 20, 20)];
    [imgView setImage:[UIImage imageNamed:@"icon_inclusion"]];
    [imgView setContentMode:UIViewContentModeCenter];
    [pricing_options addSubview:imgView];
    
    
    UIView *left_panel = [[UIView alloc]initWithFrame:CGRectMake(startX, startY + 20, 130, 40)];
    UIView *right_panel = [[UIView alloc]initWithFrame:CGRectMake(startX+150, startY + 20, 130, 40)];
    
    
    [pricing_options addSubview:left_panel];
    [pricing_options addSubview:right_panel];

    startY = 5;
    for (int i = 0; i < [t getPayments].count; i++)
    {
        UILabel *l =[[UILabel alloc]initWithFrame:CGRectMake(0, startY + 10 * (i/2), 130, 10)];
        [l setText:[NSString stringWithFormat:@"+ %@",[t getPayments][i]]];
        [l setFont:[UIFont fontWithName:@"Helvetica" size:10]];
        [l setTextColor:[Utils colorFromHex:@"#999999"]];
        
        if (i%2 == 0)
        {
            [left_panel addSubview:l];
        }
        else
        {
            [right_panel addSubview:l];
        }
        
        
    }

    
    //terms
    int policy_start = price_view_startY +pricing_height + 8;
    
    CGPoint p = CGPointMake(0, policy_start);
    UIView *policyView = [self generatePolicyView:p title:@"Terms & Conditions" Icon:[UIImage imageNamed:@"icon_terms&condition"] Text:[t getTerms]];
    [container addSubview:policyView];
    
    //privacy policy
    
   policy_start += policyView.frame.size.height + 8;
    
    p = CGPointMake(0, policy_start);
    UIView *policyView1 = [self generatePolicyView:p title:@"Privacy Policy" Icon:[UIImage imageNamed:@"icon_privacy"] Text:[t getPrivacyStatement]];
    [container addSubview:policyView1];
    
    policy_start += policyView1.frame.size.height + 8;
    
    //refunds
    p = CGPointMake(0, policy_start);
    UIView *policyView2 = [self generatePolicyView:p title:@"Cancellation Policy" Icon:[UIImage imageNamed:@"icon_cancel"] Text:[t getRefundStatement]];
    [container addSubview:policyView2];
    
    [container setFrame:CGRectMake(0, 0, 320, policy_start + policyView2.frame.size.height)];
    
    
    
    
    
    
    
    
    
    [heightConstraint setConstant:policy_start + policyView2.frame.size.height];
    
    
}

- (IBAction)bookTour:(id)sender
{
	
	[self showPickerModalView];
	
    /*UILabel *adultCount = (UILabel *)[self.view viewWithTag:190];
    UILabel *childrenCount = (UILabel *)[self.view viewWithTag:191];
    NSDateFormatter *f = [[NSDateFormatter alloc]init];
    [f setDateFormat:@"yyyy-MM-dd"];
    
    
    if ([[UserObject sharedInstance]IsUserLoggedIn])
    {
        hud = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:hud];
        [hud show:YES];
        
        NSDictionary *params = @{@"userId":[[UserObject sharedInstance]getUserID],@"tourId":[mainData getID],@"totalAmount":[NSString stringWithFormat:@"%.2f",[mainData getPrice]],@"adultsCount":adultCount.text,@"childrenCount":childrenCount.text,@"currency":[mainData currency],@"bookingDate":[f stringFromDate:selectedDate],@"tourStartDate":[mainData getStartDate],@"tourEndDate":[mainData getExpiretDate]};
        
        [[Network shared_instance]bookTour:params completionBlock:^(int status, id response) {

            [hud show:NO];
            [hud removeFromSuperview];

            
            switch (status)
            {
                case 0:
                {
                    NSLog(@"%@",response);
                   [self.delegate isShowToast:@"tour booking successful"];
                }
                    break;
                case 1:
                {
                    NSLog(@"%@",response);
                }
                    break;
            }
            
            
        }];

    }
    else
    {
        [self.delegate isShowToast:@"user not logged in"];
    }
    
    
    */
    
    
}









-(UIView *) generatePolicyView:(CGPoint )start title:(NSString *)title Icon:(UIImage *)img Text:(NSString *)text
{
    UIView *v = [[UIView alloc]init];
    [v setBackgroundColor:[Utils colorFromHex:@"#8bc34a"]];
    
    UIView *headingView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    [headingView setBackgroundColor:[Utils colorFromHex:@"#689f38"]];
    [v addSubview:headingView];
    
    UIImageView *icon = [[UIImageView alloc]initWithFrame:CGRectMake(20, 5, 20, 20)];
    [icon setImage:img];
    [icon setContentMode:UIViewContentModeCenter];
    [headingView addSubview:icon];
    
    UILabel *l = [[UILabel alloc]initWithFrame:CGRectMake(45, 5, 150, 20)];
    [l setText:title];
    [l setTextColor:[UIColor whiteColor]];
    [l setFont:[UIFont fontWithName:@"Helvetica-Bold" size:15]];
    [headingView addSubview:l];
    
    CGSize s = [text sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(280, 800) lineBreakMode:NSLineBreakByWordWrapping];

    UILabel *textView = [[UILabel alloc]initWithFrame:CGRectMake(20, 40, 280, s.height)];
    [textView setText:text];
    textView.numberOfLines = 0;
    textView.lineBreakMode = NSLineBreakByWordWrapping;
    [textView setFont:[UIFont systemFontOfSize:10]];
    [textView setTextColor:[UIColor whiteColor]];
    
    [v addSubview:textView];
    
    [v setFrame:CGRectMake(start.x, start.y, 320, 50 + s.height)];
    

    return v;
}

#pragma mark calendarView delegates
-(IBAction)checkInBtnPressed:(id)sender
{


    calendarViewController = [[PDTSimpleCalendarViewController alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *firstDate=[formatter dateFromString:[mainData getStartDate]];
    NSDate *lastDate = [formatter dateFromString:[mainData getExpiretDate]];
    calendarViewController.firstDate = firstDate;
    calendarViewController.lastDate=lastDate;
    
    [calendarViewController setSelectedDate:selectedDate];
    [calendarViewController scrollToSelectedDate:NO];
    [calendarViewController.collectionView reloadData];
    [calendarViewController setDelegate:self];
    
    background = [[UIView alloc]initWithFrame:self.view.bounds];
    [background setBackgroundColor:[UIColor blackColor]];
    background.alpha = .7;
    [self.view addSubview:background];
    [self.view bringSubviewToFront:background];

    
    containerView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 280, 400)];
    UIButton *doneBtn=[[UIButton alloc]initWithFrame:CGRectMake(0, 370, 280, 30)];
            
    [self addChildViewController:calendarViewController];
    [containerView addSubview:calendarViewController.view];
    [doneBtn setBackgroundImage:[Utils imageWithColor:[UIColor grayColor]] forState:UIControlStateNormal];
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    doneBtn.titleLabel.textColor=[UIColor whiteColor];
    [doneBtn addTarget:self action:@selector(dateConfirmBtn:) forControlEvents:UIControlEventTouchUpInside];
    [calendarViewController.view setFrame:CGRectMake(0, 0, 280, 370)];
    [containerView addSubview:doneBtn];
    [self.view addSubview:containerView];
    [self.view bringSubviewToFront:containerView];

}

-(void)simpleCalendarViewController:(PDTSimpleCalendarViewController *)controller didSelectDate:(NSDate *)date
{
    selectedDate = date;
}

-(IBAction)dateConfirmBtn:(id)sender
{
    [containerView removeFromSuperview];
    [background removeFromSuperview];
    [self setDate:selectedDate];
}


-(void)setDate:(NSDate *)d
{

    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit|NSMonthCalendarUnit|NSWeekdayCalendarUnit) fromDate:d];
    NSString *month = [Utils getMonthOfYear:components.month];
    NSString *day = [NSString stringWithFormat:@"%d",components.day];
    NSString *weekDay =[Utils getDayOfWeek:components.weekday];
    
    UILabel *date = (UILabel *)[checkInView viewWithTag:600];
    [date setText:day];
    
    UILabel *monthLabel = (UILabel *)[checkInView viewWithTag:601];
    [monthLabel setText:month];
    
    UILabel *dayLabel = (UILabel *)[checkInView viewWithTag:602];
    [dayLabel setText:weekDay];


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark pickerModalView-delegates

-(void)showPickerModalView
{

	bookingModalView = [[[NSBundle mainBundle]loadNibNamed:@"bookingView" owner:self options:nil] firstObject];
	[bookingModalView setFrame:CGRectMake(40, 20, 240, 400)];
	
	background = [[UIView alloc]initWithFrame:self.view.bounds];
	[background setBackgroundColor:[UIColor blackColor]];
	background.alpha = .7;
	
	[self.view addSubview:background];
	[self.view bringSubviewToFront:background];
	
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cancelPickerTap:)];
	[background addGestureRecognizer:tap];
	
	[self.view addSubview:bookingModalView];
	[self.view bringSubviewToFront:bookingModalView];
}

-(void)updatePrice
{
    NSLog(@"%@",[mainData priceType]);
    
    UILabel *adultCount = (UILabel *)[self.view viewWithTag:190];
    UILabel *childCount = (UILabel *)[self.view viewWithTag:191];
    
    if ([[[mainData priceType] lowercaseString]isEqualToString:@"group"])
    {
        float totalPrice = ([adultCount.text intValue] + [childCount.text intValue])*[mainData getPrice];
        [price setText:[NSString stringWithFormat:@"%.2f",totalPrice]];
    }
    else
    {
        UILabel *alternateChildCount = (UILabel *)[self.view viewWithTag:192];
        
        float totalPrice = [adultCount.text intValue]*[mainData getBasicAdultPrice] + [childCount.text intValue]*[mainData getBasicChildPrice] + [alternateChildCount.text intValue]*[mainData getChildAlternatePrice];
        [price setText:[NSString stringWithFormat:@"%.2f",totalPrice]];
    }
}

-(void)cancelPickerTap:(UIGestureRecognizer *)tap
{
	[bookingModalView removeFromSuperview];
	[containerView removeFromSuperview];
	[background removeFromSuperview];
	background = nil;
	containerView = nil;
	bookingModalView = nil;
}


-(void)handleTap:(UITapGestureRecognizer *)tap
{

    [self.tourDelegate didClickImage:YES index:tap.view.tag-900];

}


@end
