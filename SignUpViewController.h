//
//  SignUpViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 1/25/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <PDTSimpleCalendar.h>
#import <MBProgressHUD.h>
#import "TableViewWithBlock.h"
#import <FacebookSDK.h>
#import "SelectionCell.h"


@class InitSlidingViewController;
@class SignInViewController;

@interface SignUpViewController : UIViewController<PDTSimpleCalendarViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;


@property (strong, nonatomic) IBOutlet UITextField *nameTitle;

@property (strong, nonatomic) IBOutlet UITextField *firstname;

@property (strong, nonatomic) IBOutlet UITextField *lastname;

@property (strong, nonatomic) IBOutlet UITextField *address;


@property (strong, nonatomic) IBOutlet UITextField *zip;

@property (strong, nonatomic) IBOutlet UITextField *city;

@property (strong, nonatomic) IBOutlet UITextField *passport;

@property (strong, nonatomic) IBOutlet UILabel *DOB;

@property (strong, nonatomic) IBOutlet UIImageView *pictureView;

@property (strong, nonatomic) IBOutlet UITextField *email;

@property (strong, nonatomic) IBOutlet UITextField *pass;

@property (strong, nonatomic) IBOutlet UITextField *phone;

@property (strong, nonatomic) IBOutlet TableViewWithBlock *cityTable;

@property (weak,nonatomic) NSDictionary *fbUserInfo;

- (IBAction)signUp:(id)sender;

- (IBAction)backToSignIn:(id)sender;

- (IBAction)back:(id)sender;




@end
