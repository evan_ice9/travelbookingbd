//
//  FeaturedToursViewController.m
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/15/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import "FeaturedToursViewController.h"

@interface FeaturedToursViewController()

@end

@implementation FeaturedToursViewController
{

    NSArray *mainData;
    UIPickerView *pickerView;
    UIView *containerView;
    MBProgressHUD *hud;
}

@synthesize table;

-(void)viewDidLoad
{
    [super viewDidLoad];
    table.delegate = self;
    table.dataSource = self;
    table.tableFooterView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 70)];
    [self loadTours];
}



-(void)loadTours
{

    hud = [[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:hud];
    [hud show:YES];
    
    [[Network shared_instance]filterTours:@{} completionBlock:^(int status, id response)
    {
        
        
        switch (status) {
            case 0:
            {
                mainData = response[@"tours"];
                [table reloadData];
            }
                break;
            case 1:
            {
                NSLog(@"failed");
                
            }
                break;
                
                
            default:
                break;
        }
        
        [hud show:NO];
        [hud removeFromSuperview];

    }];




}

-(NSString *)getShortMonth:(int)index
{

    NSArray *months = @[@"JAN",@"FEB",@"MAR",@"APR",@"JUN",@"JUL",@"AUG",@"SEP",@"OCT",@"NOV",@"DEC"];


    return months[index -1];
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{

    return 1;
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{


    return mainData.count;

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{


    
    NSString *cellIdentifier = @"cell";
    
    UITableViewCell *cell=(UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    tour *t=[[tour alloc]initWithData:mainData[indexPath.row]];
    
    UIImageView *tourImage = (UIImageView *)[cell viewWithTag:50];
    UILabel *name = (UILabel *)[cell viewWithTag:51];
    UILabel *type = (UILabel *)[cell viewWithTag:52];
    UILabel *price = (UILabel *)[cell viewWithTag:53];
    UIView *categoryView=[cell viewWithTag:54];
    UILabel *categoryLabel = (UILabel *)[categoryView viewWithTag:55];
    UILabel *startingDate = (UILabel *)[cell viewWithTag:56];
    UILabel *expireDate = (UILabel *)[cell viewWithTag:57];
    UILabel *currencySymbol = (UILabel *)[cell viewWithTag:80];
    UIView *container = [cell viewWithTag:60];
    
    
    
    [name setText:[t getTitle]];
    [type setText:[t getType]];
    [price setText:[NSString stringWithFormat:@"%.02f",[t getPrice]]];
    
    currencySymbol.text= [t getCurrencySymBol];
    [categoryLabel setText:[t getCategory]];
    
    if ([[t getCategory] isEqual:@"Domestic"])
    {
        [categoryView setBackgroundColor:[Utils colorFromHex:@"#f57c00"]];
    }
    else if ([[t getCategory]isEqualToString:@"International"])
    {
        [categoryView setBackgroundColor:[Utils colorFromHex:@"#1AB7FF"]];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *s = [formatter dateFromString:[t getStartDate]];
    NSDate *e = [formatter dateFromString:[t getExpiretDate]];
    
    
    [formatter setDateFormat:@"dd MMM"];
    
    [startingDate setText:[formatter stringFromDate:s]];
    [expireDate setText:[formatter stringFromDate:e]];
    [tourImage setImageWithURL:[NSURL URLWithString:[t images][0][@"src"]]];
    
    [Utils roundedLayer:container.layer radius:2.0 shadow:YES bounds:container.bounds];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];


    return cell;


}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TourOverViewController *tvc = (TourOverViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"tourOverview"];
    [self presentViewController:tvc animated:YES completion:nil];
    tour *t = [[tour alloc]initWithData:mainData[indexPath.row]];
    [tvc setData:t];
}

-(void)updateFilterOptions:(NSDictionary *)params
{
    hud = [[MBProgressHUD alloc]initWithView:self.view];
    [self.view addSubview:hud];
    [hud show:YES];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    

    for (NSString *key in [params allKeys])
    {
        if (![[params valueForKey:key] isEqualToString:@"All"])
        {
            [dict setObject:[params valueForKey:key] forKey:key];
        }
    }
    params = dict;
    
    [[Network shared_instance]filterTours:params completionBlock:^(int status, id response)
     {
         
         [hud show:NO];
         [hud removeFromSuperview];
         
         
         switch (status)
         {
             case 0:
             {
                 NSLog(@"%@",response);
                 NSArray *arr  = response[@"tours"];
                 
                 NSMutableArray *tempArr = [[NSMutableArray alloc]init];
                 for (NSDictionary *d in arr)
                 {
                     //tour *t = [[tour alloc]initWithData:d];
                     [tempArr addObject:d];
                 }
                 mainData = tempArr;
                 [table reloadData];
                 
             }
                 break;
             case 1:
             {
                 NSLog(@"%@",response);
             }
                 break;
         }
         
         
     }];
    
    
}

@end
