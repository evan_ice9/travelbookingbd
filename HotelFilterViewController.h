//
//  HotelFilterViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 6/23/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RadioButton.h>
#import <NMRangeSlider.h>

@protocol hotelSearchFilterDelegate<NSObject>
-(void)applyFilter:(NSDictionary *)filter;
@end

@interface HotelFilterViewController : UIViewController<UITextFieldDelegate>

@property (weak,nonatomic) id<hotelSearchFilterDelegate>delegate;
@property NSDictionary *parameters;
@property NSString *priceUpperValue;
@property NSString *priceLowerValue;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *slider1LeadingSpace;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *slider1TraiingSpace;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *slider2LeadingSpace;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *slider2TrailingSpace;

@end
