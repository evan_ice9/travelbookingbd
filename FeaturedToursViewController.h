//
//  FeaturedToursViewController.h
//  TravelBookingBD
//
//  Created by Fahim Ahmed on 3/15/15.
//  Copyright (c) 2015 Ice9 Interactive Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InitSlidingViewController.h"
#import "tour.h"
#import "TourOverViewController.h"
#import <PDTSimpleCalendar.h>
#import <MBProgressHUD.h>


@interface FeaturedToursViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *table;
-(void)updateFilterOptions:(NSDictionary *)params;
-(void)loadTours;
@end
